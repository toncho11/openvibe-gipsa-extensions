Notes:

1) Project web-site: https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/Home

2) This source can be compiled with the git source tree of OpenVibe.

3) To compile openvibe-gipsa extensions dll you need to copy the *contents* of 

\src\openvibe-plugins-gipsa\trunc

to the source folder of OpenVibe: \openvibe-src\extras\externals\gipsa-extensions

and recompile OpenVibe.

4) Documentation and images for Bain Invaders are available at:

\src\all-brain-invaders\brain-invaders\trunc\doc

Images for Brain INvaders are in: gipsa-extensions\src\all-brain-invaders\brain-invaders\trunc\doc