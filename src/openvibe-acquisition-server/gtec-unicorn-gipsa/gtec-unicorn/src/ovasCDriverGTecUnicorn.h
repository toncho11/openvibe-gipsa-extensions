#ifndef __OpenViBE_AcquisitionServer_CDriverGTecUnicorn_H__
#define __OpenViBE_AcquisitionServer_CDriverGTecUnicorn_H__

#if defined TARGET_HAS_ThirdPartyGUSBampCAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <Windows.h>

#include "ringbuffer.h"

//#include <gtk/gtk.h>
#include <vector>

//threading
#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory> // unique_ptr

#include <deque>

#include "unicorn.h"

using namespace std;

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGTecUnicorn
	 * \author Anton Andreev, Gipsa-lab, VIBS team
	 * \date 21/08/2020
	 * \brief GTEC Unicorn Black Driver
	 *
	 */

    #ifndef __GDEVICE_H__
    #define __GDEVICE_H__
    struct GDevice
	{
		UNICORN_HANDLE handle;
		std::string   serial;
	};
	#endif
	
	class CDriverGTecUnicorn : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverGTecUnicorn(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);

		virtual void release(void);
		virtual const char* getName(void);

		virtual OpenViBE::boolean initialize(const OpenViBE::uint32 ui32SampleCountPerSentBlock, OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual OpenViBE::boolean uninitialize(void);

		virtual OpenViBE::boolean start(void);
		virtual OpenViBE::boolean stop(void);
		virtual OpenViBE::boolean loop(void);

		virtual OpenViBE::boolean isConfigurable(void);
		virtual OpenViBE::boolean configure(void);
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader(void) { return &m_oHeader; }

		OpenViBE::boolean CDriverGTecUnicorn::acquire(void);

		void ConfigFiltering(HANDLE o_pDevice);

	protected:

		static const int BUFFER_SIZE_SECONDS = 2;         //the size of the GTEC ring buffer in seconds
		static const int GTEC_NUM_CHANNELS = 17;          //the number of channels
		static const int QUEUE_SIZE = 8;//4 default		  //the number of GT_GetData calls that will be queued during acquisition to avoid loss of data
		
		OpenViBE::uint32 NumDevices();

		OpenViBE::uint32 GetChannelIndex(UNICORN_HANDLE hDevice, const char *name);

		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		OpenViBE::uint32 m_ui32SampleCountPerSentBlock;

		//START declaration buffers
		OpenViBE::float32* m_pBufferRawUnicornDevice; //buffer 1 : data from device to ring buffer

		OpenViBE::float32* m_bufferReceivedDataFromRing; //buffer 2 : data from ring buffer

		OpenViBE::float32* m_pBufferForOpenVibe; // buffer 3 : data converted to OpenVibe format
		//END declaration buffers

		OpenViBE::uint32 m_ui32AcquiredChannelCount;      //number of channels specified by the user, never counts the event channels

		OpenViBE::uint32 m_ui32TotalHardwareStimulations; //since start button clicked
		OpenViBE::uint32 m_ui32TotalRingBufferOverruns;
		OpenViBE::uint32 m_ui32TotalCounterErrors; 

		OpenViBE::boolean m_flagIsFirstLoop;
		OpenViBE::boolean m_bufferOverrun;

		//ring buffer provided by Guger
		CRingBuffer<OpenViBE::float32> m_RingBuffer;	

		std::unique_ptr<std::thread> m_ThreadPtr;
		OpenViBE::boolean m_bIsThreadRunning;
		std::mutex m_io_mutex;
		std::condition_variable  m_itemAvailable;

		OpenViBE::boolean ConfigureDevice(OpenViBE::uint32 deviceNumber);

		void detectDevices();

		vector<GDevice> m_vGDevices;                        // List of amplifiers

		std::string CDriverGTecUnicorn::getSerialByHandler(HANDLE o_pDevice);

		OpenViBE::int32 m_i32LengthBufferRawUnicornDevice;

		OpenViBE::int32 m_i32ChannelCounterIndex;
	};
};

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI

#endif // __OpenViBE_AcquisitionServer_CDriverGTecUnicorn_H__
