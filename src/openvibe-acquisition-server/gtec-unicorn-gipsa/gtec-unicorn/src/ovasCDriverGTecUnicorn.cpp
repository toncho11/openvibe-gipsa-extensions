#if defined TARGET_HAS_ThirdPartyGUSBampCAPI

#include "ovasCDriverGTecUnicorn.h"

#include <toolkit/ovtk_all.h>
#include <system/ovCTime.h>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <limits>

#include <mutex>
#include <thread>

#include <unicorn.h>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace std;

#if defined(TARGET_OS_Windows)
#pragma warning(disable: 4800) // disable "forcing value to bool 'true' or 'false' (performance warning)" nag coming from BOOL->bool cast on e.g. VS2010
#endif

#define FRAME_LENGTH			8				// The number of samples acquired per Get_Data() call and the number of samples supplied to OpenVibe (per loop)
#define TESTSIGNAL_ENABLED		FALSE			// Flag to enable or disable testsignal.
#define SELECTED_DEVICE			0				// If several Unicorn devices are in range, the first one will be automatically selected


/*
    17 channels from Unicorn Black on every sample: | EEG1| EEG2| EEG3| EEG4| EEG5| EEG6| EEG7| EEG8| ACCX|ACCY| ACCZ| GYRX|GYRY| GYRZ|CNT|BATLVL|VALID|
	Order might be different. This is why we need to use: UNICORN_GetChannelIndex(channel name);
*/

CDriverGTecUnicorn::CDriverGTecUnicorn(IDriverContext& rDriverContext)
	:IDriver(rDriverContext)
	,m_oSettings("AcquisitionServer_Driver_GTecGUSBamp", m_rDriverContext.getConfigurationManager())
	,m_pCallback(NULL)
	,m_ui32SampleCountPerSentBlock(0)
	,m_pBufferForOpenVibe(NULL)
	,m_ui32AcquiredChannelCount(GTEC_NUM_CHANNELS)
	,m_flagIsFirstLoop(true)
{
	m_oHeader.setSamplingFrequency(UNICORN_SAMPLING_RATE);
	m_oHeader.setChannelCount(0);	

	//m_oSettings.load();
}

void CDriverGTecUnicorn::release(void)
{
	delete this;
}

const char* CDriverGTecUnicorn::getName(void)
{
	return "g.tec Unicorn Gipsa-lab";
}

//___________________________________________________________________//
//                                                                   //

OpenViBE::boolean CDriverGTecUnicorn::initialize(
	const uint32 ui32SampleCountPerSentBlock,
	IDriverCallback& rCallback)
{
	if (m_rDriverContext.isConnected()) return false;

	detectDevices();

	if (NumDevices() == 0) return false;

	m_ui32SampleCountPerSentBlock = ui32SampleCountPerSentBlock;
	m_pCallback = &rCallback;

	//Set number of channels
	unsigned int numberOfChannelsToAcquire = 0;
	int errorCode = UNICORN_GetNumberOfAcquiredChannels(m_vGDevices[SELECTED_DEVICE].handle, &numberOfChannelsToAcquire);
	if (errorCode != UNICORN_ERROR_SUCCESS)
	{
		m_rDriverContext.getLogManager() << LogLevel_Error << "Unable to get channel count.\n";
		return false;
	}
	else
	{
		m_rDriverContext.getLogManager() << LogLevel_Info << "Number of available channels is: " << numberOfChannelsToAcquire << "\n";
	}

	m_oHeader.setChannelCount(numberOfChannelsToAcquire); //only 8 channels are EEG, but currently we provide everything
	m_ui32AcquiredChannelCount = numberOfChannelsToAcquire;
	m_i32LengthBufferRawUnicornDevice = numberOfChannelsToAcquire * FRAME_LENGTH;

	//Set channel names
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 1"), "EEG1");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 2"), "EEG2");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 3"), "EEG3");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 4"), "EEG4");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 5"), "EEG5");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 6"), "EEG6");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 7"), "EEG7");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "EEG 8"), "EEG8");

	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Accelerometer X"), "ACCX");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Accelerometer Y"), "ACCY");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Accelerometer Z"), "ACCZ");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Gyroscope X"), "GYRX");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Gyroscope Y"), "GYRY");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Gyroscope Z"), "GYRZ");
	m_i32ChannelCounterIndex = GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Counter");
	m_oHeader.setChannelName(m_i32ChannelCounterIndex, "Counter");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Battery Level"), "Battery");
	m_oHeader.setChannelName(GetChannelIndex(m_vGDevices[SELECTED_DEVICE].handle, "Validation Indicator"), "VI");

	//Initialize buffers
	m_bufferReceivedDataFromRing = new OpenViBE::float32[m_i32LengthBufferRawUnicornDevice];
	m_pBufferForOpenVibe		 = new OpenViBE::float32[m_i32LengthBufferRawUnicornDevice];
	
	m_RingBuffer.Initialize(BUFFER_SIZE_SECONDS * m_oHeader.getSamplingFrequency() * m_i32LengthBufferRawUnicornDevice);

	//Configure each device
	for (uint32 i = 0; i< NumDevices(); i++)
	{
		ConfigureDevice(i);
	}

	return true; 
}

void CDriverGTecUnicorn::detectDevices()
{
	int errorCode = UNICORN_ERROR_SUCCESS;

	//Get number of available devices
	unsigned int availableDevicesCount = 0;
	errorCode = UNICORN_GetAvailableDevices(NULL, &availableDevicesCount, TRUE);

	//Get serials of available devices
	UNICORN_DEVICE_SERIAL *availableDevices = new UNICORN_DEVICE_SERIAL[availableDevicesCount];
	errorCode = UNICORN_GetAvailableDevices(availableDevices, &availableDevicesCount, TRUE);

	if (errorCode != UNICORN_ERROR_SUCCESS || availableDevicesCount < 1)
	{
		m_rDriverContext.getLogManager() << LogLevel_Error << "No device available. Please pair with a Unicorn device first.\n";
	}
	else
	{
		//Create a GDevice list for all devices and print available device serials
		m_rDriverContext.getLogManager() << LogLevel_Info << "Available Unicorn devices:\n";

		for (unsigned int i = 0; i < availableDevicesCount; i++)
		{
			m_rDriverContext.getLogManager() << LogLevel_Info << "#" << i << ": " << availableDevices[i] << "\n";

			GDevice device;

			UNICORN_HANDLE deviceHandle;

			int errorCode = UNICORN_OpenDevice(availableDevices[i], &deviceHandle);
			if (errorCode != UNICORN_ERROR_SUCCESS)
			{
				m_rDriverContext.getLogManager() << LogLevel_Error << "Unable to connect to device: '" << availableDevices[i] << "'\n";
			}

			device.handle = deviceHandle;

			device.serial = availableDevices[i];

			m_vGDevices.push_back(device);
		}
	}
}

OpenViBE::boolean CDriverGTecUnicorn::ConfigureDevice(OpenViBE::uint32 deviceNumber)
{
	int errorCode = UNICORN_ERROR_SUCCESS;

	UNICORN_HANDLE o_pDevice = m_vGDevices[deviceNumber].handle;
	string current_serial = m_vGDevices[deviceNumber].serial;

	UNICORN_AMPLIFIER_CONFIGURATION configuration;
	errorCode = UNICORN_GetConfiguration(o_pDevice, &configuration);

	if (errorCode != UNICORN_ERROR_SUCCESS)
	{
		m_rDriverContext.getLogManager() << LogLevel_Error << "Unable to get configuration for: '" << current_serial.c_str() << "'\n";
		return false;
	}

	return true;
}

OpenViBE::boolean CDriverGTecUnicorn::start(void)
{
	if(!m_rDriverContext.isConnected()) return false;
	if(m_rDriverContext.isStarted()) return false;

	m_ui32TotalHardwareStimulations = 0;
	m_ui32TotalRingBufferOverruns = 0;
	m_ui32TotalCounterErrors = 0;

	{
		std::lock_guard<std::mutex> lock(m_io_mutex);
		m_RingBuffer.Reset();
	}

	for (uint32 i = 0; i< NumDevices(); i++)
	{
		UNICORN_HANDLE o_pDevice = m_vGDevices[i].handle;
		UNICORN_StartAcquisition(o_pDevice, TESTSIGNAL_ENABLED);
	}
	
	m_bIsThreadRunning = true;
	m_flagIsFirstLoop = true;
	m_bufferOverrun = false;

	m_ThreadPtr.reset(new std::thread( std::bind(&CDriverGTecUnicorn::acquire , this )));

	return true;
}

//This method is called by the AS and it supplies the acquired data to the AS
OpenViBE::boolean CDriverGTecUnicorn::loop(void)
{
	CStimulationSet   l_oStimulationSet;

	int samples_required = 1;

	if (m_rDriverContext.isStarted())
	{
		bool l_bDataAvailable = false;

		{
			std::unique_lock<std::mutex> lock(m_io_mutex);

			while (m_RingBuffer.GetSize() < m_i32LengthBufferRawUnicornDevice) 
			{
				m_itemAvailable.wait(lock);
			}

			try
			{
				if (m_bufferOverrun)
				{
					m_RingBuffer.Reset();
					m_bufferOverrun = false;
					m_ui32TotalRingBufferOverruns++;
					return true;
				}

				m_RingBuffer.Read(m_bufferReceivedDataFromRing, m_i32LengthBufferRawUnicornDevice);
			}
			catch (std::exception e)
			{
				m_rDriverContext.getLogManager() << LogLevel_Error << "Error reading GTEC ring buffer! Error is:" << e.what() << "\n";
			}

			m_itemAvailable.notify_one();

		}

		//covert to openvibe format ch1,ch1,ch1,ch2,ch2,ch2 ...
		if (FRAME_LENGTH == 1)
		{
			memcpy(m_pBufferForOpenVibe, m_bufferReceivedDataFromRing, m_i32LengthBufferRawUnicornDevice * sizeof(OpenViBE::float32));
		}
		else 
		{
			for (uint32 i = 0; i < m_ui32AcquiredChannelCount; i++)
			{
				for (uint32 j = 0; j < FRAME_LENGTH; j++)
				{
					m_pBufferForOpenVibe[FRAME_LENGTH * i + j] = m_bufferReceivedDataFromRing[j * m_ui32AcquiredChannelCount + i];
				}
			}
		}

		//verify counter
		int counter_pos = FRAME_LENGTH * m_i32ChannelCounterIndex;
		for (uint32 i = counter_pos; i < FRAME_LENGTH; i++)
		{
			if (!(m_pBufferForOpenVibe[i] < m_pBufferForOpenVibe[i + 1]))
			{
				m_ui32TotalCounterErrors++;
			}
		}

		//forward data
		m_pCallback->setSamples(m_pBufferForOpenVibe, FRAME_LENGTH);
        m_pCallback->setStimulationSet(l_oStimulationSet);
		m_rDriverContext.correctDriftSampleCount(m_rDriverContext.getSuggestedDriftCorrectionSampleCount());
	}
    else
	{
	    System::Time::sleep(20);
	}

	return true;
}

//This function used by the thread
OpenViBE::boolean CDriverGTecUnicorn::acquire(void)
{	
	if (m_flagIsFirstLoop) //First time do some memory initialization, etc
	{
		m_pBufferRawUnicornDevice = new OpenViBE::float32[m_i32LengthBufferRawUnicornDevice];

		m_flagIsFirstLoop=false;
	}

	while(m_bIsThreadRunning == true)
	{
		try
		{
			DWORD numBytesReceived = 0;
			OpenViBE::boolean flagChunkLostDetected = false;
			OpenViBE::boolean flagChunkTimeOutDetected = false;

			UNICORN_HANDLE hDevice = m_vGDevices[SELECTED_DEVICE].handle;

			//Get FRAME_LENGTH number of samples
			if (UNICORN_GetData(hDevice, FRAME_LENGTH, m_pBufferRawUnicornDevice, m_i32LengthBufferRawUnicornDevice * sizeof(float)) != UNICORN_ERROR_SUCCESS)
			{
				m_rDriverContext.getLogManager() << LogLevel_Error << "Error on GT_GetData\n";
				return false;
			}

			//store to ring buffer, lock it during insertion
			{
				std::lock_guard<std::mutex> lock(m_io_mutex);
				try
				{
					//if we are going to overrun on writing the received data into the buffer, set the appropriate flag; the reading thread will handle the overrun
					m_bufferOverrun = (m_RingBuffer.GetFreeSize() < m_i32LengthBufferRawUnicornDevice);

					m_RingBuffer.Write(m_pBufferRawUnicornDevice, m_i32LengthBufferRawUnicornDevice);
				}
				catch(std::exception e)
				{
					//buffer should be unclocked automatically once the scope is left
					m_rDriverContext.getLogManager() << LogLevel_Error << "Error writing to GTEC ring buffer! Error is: " << e.what() <<"\n";
				}
				//buffer should be unclocked automatically once the scope is left

				m_itemAvailable.notify_one();
			}
				
		}
		catch(std::exception e)
		{
			m_rDriverContext.getLogManager() << LogLevel_Error << "General error in the thread function acquiring data from GTEC! Acquisition interrupted. Error is: " << e.what() << "\n";
			m_bIsThreadRunning = false;
			return false;
		}
	}

	//This code stops the amplifiers in the same thread:
	{
		m_rDriverContext.getLogManager() << LogLevel_Info << "Stopping devices and cleaning up...\n";

		//clean up allocated resources for each device
		for (uint32 i = 0; i< NumDevices(); i++)
		{
			UNICORN_HANDLE hDevice = m_vGDevices[i].handle;

			//stop device
			m_rDriverContext.getLogManager() << LogLevel_Debug << "Sending stop command ...\n";
			if (UNICORN_StopAcquisition(hDevice) != UNICORN_ERROR_SUCCESS)
			{
				m_rDriverContext.getLogManager() << LogLevel_Error << "Stopping device failed! Serial = " << m_vGDevices[NumDevices() - 1].serial.c_str() << "\n";
			}

			//clear memory
			delete[] m_pBufferRawUnicornDevice;
		}

		m_flagIsFirstLoop = true;
		m_bIsThreadRunning = false;
	}
	return true;
}

OpenViBE::boolean CDriverGTecUnicorn::stop(void)
{
	if(!m_rDriverContext.isConnected()) return false;
	if(!m_rDriverContext.isStarted()) return false;

	//stop thread
	m_bIsThreadRunning = false;
	m_ThreadPtr->join(); //wait until the thread has stopped data acquisition

	m_rDriverContext.getLogManager() << LogLevel_Info << "Data acquisition completed.\n";

	//m_rDriverContext.getLogManager() << LogLevel_Debug << "Total number of hardware stimulations acquired: " << m_ui32TotalHardwareStimulations << "\n";
	if (m_ui32TotalRingBufferOverruns > 0 )
		m_rDriverContext.getLogManager() << LogLevel_Error << "Total internal ring buffer overruns: " << m_ui32TotalRingBufferOverruns << "\n";
	if (m_ui32TotalCounterErrors > 0)
		m_rDriverContext.getLogManager() << LogLevel_Error << "Total Unicorn counter errors: " << m_ui32TotalCounterErrors << "\n";
	else m_rDriverContext.getLogManager() << LogLevel_Info << "Total Unicorn counter errors: " << m_ui32TotalCounterErrors << "\n";

	return true;
}

OpenViBE::boolean CDriverGTecUnicorn::uninitialize(void)
{
	if(!m_rDriverContext.isConnected()) return false;
	if(m_rDriverContext.isStarted()) return false;
	
	int totalDevices = NumDevices();
	int deviceClosed = 0;
	
	for (vector<GDevice>::iterator it=m_vGDevices.begin();
		it != m_vGDevices.end();)
	{
		
		if (UNICORN_CloseDevice(&it->handle) != UNICORN_ERROR_SUCCESS)
		{
			m_rDriverContext.getLogManager() << LogLevel_Error << "Unable to close device: " << it->serial.c_str() << "\n";
		}
		else
		{
			deviceClosed++;
		}

		it = m_vGDevices.erase(it);
	}

	m_rDriverContext.getLogManager() << LogLevel_Info << "Total devices closed: " << deviceClosed << " / " << totalDevices << "\n";

	if (!m_vGDevices.empty() || deviceClosed != totalDevices)
		m_rDriverContext.getLogManager() << LogLevel_Error << "Some devices were not closed properly!\n";

	m_vGDevices.clear();

	//clear memory
	if (m_bufferReceivedDataFromRing != NULL)
	{
		delete[] m_bufferReceivedDataFromRing;
		m_bufferReceivedDataFromRing = nullptr;
	}

	if (m_pBufferForOpenVibe != NULL)
	{
		delete[] m_pBufferForOpenVibe;
		m_pBufferForOpenVibe = nullptr;
	}
	m_pCallback= nullptr;

	return true;
}

//___________________________________________________________________//
//                                                                   //
OpenViBE::boolean CDriverGTecUnicorn::isConfigurable(void)
{
	return false;
}

OpenViBE::boolean CDriverGTecUnicorn::configure(void)
{
	return true;
}

inline OpenViBE::uint32 CDriverGTecUnicorn::NumDevices()
{
	return m_vGDevices.size();
}

inline OpenViBE::uint32 CDriverGTecUnicorn::GetChannelIndex(UNICORN_HANDLE hDevice, const char *name)
{
	OpenViBE::uint32* result = new OpenViBE::uint32[1];
	if (UNICORN_GetChannelIndex(hDevice, name, result) != UNICORN_ERROR_SUCCESS)
	{
		m_rDriverContext.getLogManager() << LogLevel_Error << "Error getting channel index for channel: '" << name << "'\n";
	}
	return *result;
}

namespace OpenViBEAcquisitionServer {

inline std::ostream& operator<< (std::ostream& out, const vector<string>& var)
{
	for(size_t i=0;i<var.size();i++) {
		out << var[i] << " ";
	}

	return out;
}

inline std::istream& operator>> (std::istream& in, vector<string>& var)
{
	var.clear();
	string tmp;
	while( in >> tmp ) {
		var.push_back(tmp);
	}

	return in;
}

inline std::ostream& operator<< (std::ostream& out, const vector<GDevice>& var)
{
	for (size_t i = 0; i< var.size(); i++) {
		out << var[i].serial << " ";
	}

	return out;
}

inline std::istream& operator>> (std::istream& in, vector<GDevice>& var)
{
	//cout << "Error not implemented operator >>!";

	return in;
}


}

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI
