clear all;

% Read the file
if isunix
    FILE = './data/data1.gdf';
else
    FILE = '.\data\data1.gdf';
end
[s h] = sload(FILE);
Fs = h.SampleRate;

%%%%% Parse Hardware trigger
% Chanel event
HardTrigger = s(:,end);

% Parse channel event
% each flash has a value between 20 
HardFlash = (HardTrigger >= 20);

% Raising edge
HardFlash = [0; diff(HardFlash)==1];

% indices of each flash
ixHardFlash = find(HardFlash==1);

% Number of stimulations 
Nstim = size(ixHardFlash,1);
disp(['Number of stimulations : ' num2str(Nstim)]);

% Interval between stimulations : 
StimInt = diff(ixHardFlash)/Fs;
plot(StimInt);
xlabel('index of the flash');
ylabel('time (s)');
title('interval between two flash');



