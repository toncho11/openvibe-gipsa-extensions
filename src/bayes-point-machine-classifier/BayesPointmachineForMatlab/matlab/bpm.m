asm = NET.addAssembly('C:\work\openvibe-gipsa-extensions\src\bayes-point-machine-classifier\BayesPointmachineForMatlab\bin\Debug\BayesPointMachineClassifier.dll');

t = asm.AssemblyHandle.GetType('Gipsa.PlatformInvokeTest');
p = System.Activator.CreateInstance(t);

%set training data
d1 = NET.createArray('System.Double',6);

d1.Set(0, 1.2);%feature 1
d1.Set(1, 3.4);%feature 2
d1.Set(2, 1);%class 1

d1.Set(3, 2.4');
d1.Set(4, 2.1);
d1.Set(5, 2);%class 2

%methods(p, '-full')

%double[] values, int maxSize, int actualSize, int vectorsCount
ret = p.Train(d1,6,7,2);

%test Classification
d2 = NET.createArray('System.Double',2);
d2.Set(0, 2.4);%feature 1
d2.Set(1, 2.1);%feature 2

%public double Classify(double[] values, int maxSize, int featuresCount)
class = p.Classify(d2,3,2);

if class > 0.5
    disp(1);
else
    disp(2);
end