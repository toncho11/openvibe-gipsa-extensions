﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace DummyLibraryUsageManaged
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DummyLibrary.DummyClass.Hello(".NET application"));
            Console.WriteLine(SayHello(".NET application"));
        }

        [DllImport("DummyLibrary.dll", CharSet = CharSet.Unicode)]
        public static extern string SayHello(string name);
    }
}
