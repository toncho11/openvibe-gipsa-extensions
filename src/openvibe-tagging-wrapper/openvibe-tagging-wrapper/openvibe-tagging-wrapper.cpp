// This is the main DLL file.

//#include <boost/version.hpp> 
//#if (BOOST_VERSION / 100 % 1000) < 50 
//#error Older versions does not work with software tagging
//#endif

#include "openvibe-tagging-wrapper.h"
#include "..\Communication\OVComm.h"

#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h> //for marshal from string^ to std::string

using namespace System;
using namespace System::Collections;
using namespace System::Collections::Generic;

using namespace System::Runtime::InteropServices;
using namespace msclr::interop; //for marshal_as

public ref class OpenVibeTaggingWrapper {

public:

	enum class eProtocol
	{
		EightBitParallelPort =2,
		Software = 3,
		SerialPort = 4,
	};

    OpenVibeTaggingWrapper(eProtocol protocol) 
	{ 
		if (protocol == OpenVibeTaggingWrapper::eProtocol::Software)
		{
		   pUnmanaged = new OVComm(SoftwareTagging,"openvibeExternalStimulations");
		}

		else if (protocol == OpenVibeTaggingWrapper::eProtocol::EightBitParallelPort)
		{
		   pUnmanaged = new OVComm(EightbitPP,""); //default address is set internally
		   std::cout << "Wrapper: Parallel port mode selected!!!" << std::endl;
		}

		else if (protocol == OpenVibeTaggingWrapper::eProtocol::SerialPort)
		{
		   pUnmanaged = new OVComm(SerialPort,"COM5");
		}

		else throw gcnew ObjectDisposedException("Tagging-wrapper: invalid protocol!");
	}

	OpenVibeTaggingWrapper(eProtocol protocol,System::String^ input) 
	{ 
		std::string input_str = marshal_as<std::string>(input);
		char *cstr = new char[input_str.length() + 1];
		strcpy(cstr, input_str.c_str());

		if (protocol == OpenVibeTaggingWrapper::eProtocol::Software)
		{
		   pUnmanaged = new OVComm(SoftwareTagging,cstr); //the string parameter is used for name of the boost queue
		}

		else if (protocol == OpenVibeTaggingWrapper::eProtocol::EightBitParallelPort)
		{
		   pUnmanaged = new OVComm(EightbitPP,cstr); //the string parameter is used for the address of the parallel port
		}

		else if (protocol == OpenVibeTaggingWrapper::eProtocol::SerialPort)
		{
		   pUnmanaged = new OVComm(SerialPort,cstr); //the string parameter is used for the name of the COM port
		}

		else throw gcnew ObjectDisposedException("Tagging-wrapper: invalid protocol!");

		delete [] cstr;
	}
    
	~OpenVibeTaggingWrapper() 
	{ 
		delete pUnmanaged; 
	}

    !OpenVibeTaggingWrapper() //finalizer
	{
		delete pUnmanaged; 
	}

	OpenVibeTaggingWrapper::eProtocol GetProtocol()
	{
       return m_protocol;
	}

	void SetTargetRow(int row)
	{	
	    pUnmanaged->SetTargetRow(row);
	}

	void SetTargetColumn(int column)
	{		
	    pUnmanaged->SetTargetColumn(column);
	}

	void SetFlashRow(int row)
	{		
	    pUnmanaged->SetFlashRow(row);
	}

	void SetFlashColumn(int column)
	{		
	    pUnmanaged->SetFlashColumn(column);
	}

	void TestWriteValue(int value)
	{		
	    pUnmanaged->TestWriteValue(value);
	}

	void SetFinalized()
	{
		pUnmanaged->SetFinalized();
	}

	/*
	 Send "Train" command for the P300 
	*/
	void SetBeginProcessing()
	{
		pUnmanaged->SetBeginProcessing();
	}

	/*
	 Used in the beginning to initialize OpenVibe
	*/
	void SetStartExperiment()
	{
		pUnmanaged->SetStartExperiment();
	}

	/*
	 Used to separate one repetion from another
	*/
	void SetRepetionCompeted()
	{
		pUnmanaged->SetRepetionCompeted();
	}

	/*
	 Currently used for reseting/disabling the Riemann Potato
	*/
	void SetResetFlag()
	{
		pUnmanaged->SetResetFlag();
	}

	void ConfigureTargetBaseRowCode(int newcode)
	{
		pUnmanaged->ConfigureTargetBaseRowCode(newcode);
	}

	void ConfigureTargetBaseColumnCode(int newcode)
	{
		pUnmanaged->ConfigureTargetBaseColumnCode(newcode);
	}

	void ConfigureFlashBaseRowCode(int newcode)
	{
		pUnmanaged->ConfigureFlashBaseRowCode(newcode);
	}

	void ConfigureFlashBaseColumnCode(int newcode)
	{
		pUnmanaged->ConfigureFlashBaseColumnCode(newcode);
	}

	void ConfigureRepetionCompletedCode(int newcode)
	{
		pUnmanaged->ConfigureRepetionCompletedCode(newcode);
	}

	void ConfigureExperimentStartCode(int newcode)
	{
		pUnmanaged->ConfigureExperimentStartCode(newcode);
	}

	void ConfigureTrainingStartCode(int newcode)
	{
		pUnmanaged->ConfigureTrainingStartCode(newcode);
	}

	void ConfigureResetCode(int newcode)
	{
		pUnmanaged->ConfigureResetCode(newcode);
	}

	/// <summary>
    /// Calculates the final target row and target column.
    /// </summary>
    /// <param name="numberOfRows"></param>
	/// <param name="numberOfColumns"></param>
	/// <param name="VRPNData">Data your client received from OpenVibe through VRPN for the most probable target</param>
    /// <returns>The target's row and then the target's column</returns>
	array< Int32 >^ ProcessVRPNResult(int numberOfRows,int numberOfColumns,array< Double >^ VRPNData)
	{
		//if ((numberOfRows + numberOfColumns)!=VRPNData->Length) throw gcnew Exception("Sum of rows and columns does not correspond VRPNData length!"); 
		
		//the actual length of VRPNData can be fixed to a number bigger than (numberOfRows + numberOfColumns) e.x. 32
		
		int length = numberOfRows + numberOfColumns;
		//we ignore the values that after the numberOfRows + numberOfColumns in VRPNData, they are supposed to 0

		//convert to pure unmanaged
		double* unmanagedVRPNData = new double [length];//VRPNData->Length

		for (int i=0;i<length;i++)//VRPNData->Length
		{
			unmanagedVRPNData [i] = VRPNData[i];
		}

		//call processing function
		int* unmanagedResult = pUnmanaged->ProcessVRPNResult(numberOfRows, numberOfColumns, unmanagedVRPNData);

		//convert result to managed
		array< Int32 >^ managedResult = gcnew array< Int32 >(2);

		managedResult[0] = unmanagedResult[0];
		managedResult[1] = unmanagedResult[1];

		delete[] unmanagedResult;
		delete[] unmanagedVRPNData;

		return managedResult;
	}

	//These two methods combine the functionality of the other 4
	void SetRow(int row, bool isTarget)//zero based
	{
		pUnmanaged->SetRow(row,isTarget);
	}

    void SetColumn(int column, bool isTarget)//zero based
	{
		pUnmanaged->SetColumn(column,isTarget);
	}

	System::String^ GetInitErrorMessage()
	{
		String^ result = gcnew String(pUnmanaged->InitErrorMessage().c_str());
		
		return result;
	}

	private:

    OVComm* pUnmanaged;	
	eProtocol m_protocol;

};
