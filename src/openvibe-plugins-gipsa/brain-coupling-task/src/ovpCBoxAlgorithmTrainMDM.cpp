/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCBoxAlgorithmTrainMDM.h"

#include <iostream>
#include <sstream>
#include <system/Memory.h>

#include "ovpRiemannHelper.h"
#include "ovpCBoxAlgorithmProcessMDM.h"

#include <boost/date_time/posix_time/posix_time.hpp>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessing;

boolean CBoxAlgorithmTrainMDM::initialize(void)
{
	//> init INPUT stimulation
	m_pStimulationDecoderTrigger=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
    m_pStimulationDecoderTrigger->initialize();
    ip_pMemoryBufferToDecodeTrigger.initialize(m_pStimulationDecoderTrigger->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
    op_pStimulationSetTrigger.initialize(m_pStimulationDecoderTrigger->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));
	
	IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	//all signal INPUT channels
	for(uint32 i=1; i<l_rStaticBoxContext.getInputCount(); i++) //first one is stimulation
	{
		IAlgorithmProxy* l_pStreamDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalStreamDecoder));
		l_pStreamDecoder->initialize();
		m_vStreamDecoder.push_back(l_pStreamDecoder);
	}

	//output stimulation 1 
	m_oStimulationEncoder.initialize(*this);

	//UI parameters:
	CString l_sSettingValue;

	//Training file location
	l_rStaticBoxContext.getSettingValue(0, l_sSettingValue);
	std::string l_sFileNamePath=(std::string)this->getConfigurationManager().expand(l_sSettingValue);

	//Train Stimulation
	m_ui64STrainStimulationIdentifier=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	//IsP300
	l_rStaticBoxContext.getSettingValue(2, l_sSettingValue);
	m_IsP300=(OpenViBE::boolean)this->getConfigurationManager().expandAsBoolean(l_sSettingValue);

	//Start processing file section
	if (l_sFileNamePath == "") 
	{ 
	    this->getLogManager() << LogLevel_Error << "Parameter filename is empty!\n";
		//return false;
	}

	this->getLogManager() << LogLevel_Info << "Parameter file:" << l_sFileNamePath.c_str() << "\n";

	m_ParamFile.open(l_sFileNamePath.c_str());

	if (m_ParamFile.bad())
	{
		this->getLogManager() << LogLevel_Error << "Could not create parameter file:" << l_sFileNamePath.c_str() << "\n";
		//return false;
	}
    //End processing file section

	m_bStartTrain = false;
	m_vBufferedSignalPerChannel.clear();
	m_vCovarianceMatricesPerChannel.clear();
	m_vResultMean.clear();

	for(int c=1; c<l_rStaticBoxContext.getInputCount(); c++) //first c=0 is stimulations
	{
		m_vCovarianceMatricesPerChannel.push_back(new std::vector<itpp::mat>());
		m_vBufferedSignalPerChannel.push_back(new std::vector<itpp::mat>());
	}
	
	if (m_IsP300 && l_rStaticBoxContext.getInputCount() > 3) 
    {
		this->getLogManager() << LogLevel_Error << "For P300 you need exactly 1 stimulation and 2 signal input channels." << "\n";
	}
	
	return true;
}

boolean CBoxAlgorithmTrainMDM::uninitialize(void)
{
	IBox& l_rStaticBoxContext=this->getStaticBoxContext();

	//uninit all input channels
	for(uint32 i=0; i<m_vStreamDecoder.size(); i++) //first one is stimulation
	{
		IAlgorithmProxy* l_pStreamDecoder=m_vStreamDecoder[i];
		l_pStreamDecoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*l_pStreamDecoder);
	}
	m_vStreamDecoder.clear();

	//unint output stimulation - Train completed
	m_oStimulationEncoder.uninitialize();

	//uninit input stimulation
	m_pStimulationDecoderTrigger->uninitialize();
    ip_pMemoryBufferToDecodeTrigger.uninitialize();
	op_pStimulationSetTrigger.uninitialize();
    this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoderTrigger);

	//start clear matrices
	for (vector_type<std::vector<itpp::mat>*>::iterator it=m_vBufferedSignalPerChannel.begin(); it!=m_vBufferedSignalPerChannel.end(); it++)
	{
		delete *it;
	}
	for (vector_type<std::vector<itpp::mat>*>::iterator it=m_vCovarianceMatricesPerChannel.begin(); it!=m_vCovarianceMatricesPerChannel.end(); it++)
	{
		delete *it;
	}

	m_vBufferedSignalPerChannel.clear();
	m_vCovarianceMatricesPerChannel.clear();
	m_vResultMean.clear();
	//end clear matrices

	//if (pFile != NULL) {fclose (pFile);}
	
	return true;
}

boolean CBoxAlgorithmTrainMDM::processInput(uint32 ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

boolean CBoxAlgorithmTrainMDM::process(void)
{
	uint64 l_ui64TrainDate, l_ui64TrainChunkStartTime, l_ui64TrainChunkEndTime;
	IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO* l_rDynamicBoxContext=getBoxAlgorithmContext()->getDynamicBoxContext();

	//check stimulation channel
	for(uint32 j=0; j<l_rDynamicBoxContext->getInputChunkCount(0); j++)
	{
		ip_pMemoryBufferToDecodeTrigger=l_rDynamicBoxContext->getInputChunk(0, j);
		m_pStimulationDecoderTrigger->process();

		if(m_pStimulationDecoderTrigger->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedHeader))
		{
			m_oStimulationEncoder.encodeHeader(0);
			l_rDynamicBoxContext->markOutputAsReadyToSend(0,l_rDynamicBoxContext->getInputChunkStartTime(0, j),l_rDynamicBoxContext->getInputChunkEndTime(0, j));
		}

		//BUFFER
		if(m_pStimulationDecoderTrigger->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
            for(uint32 k=0; k<op_pStimulationSetTrigger->getStimulationCount(); k++)
			{
                 // Check for Train Stimulation
				m_bStartTrain |= (op_pStimulationSetTrigger->getStimulationIdentifier(k)==m_ui64STrainStimulationIdentifier);
				if (op_pStimulationSetTrigger->getStimulationIdentifier(k)==m_ui64STrainStimulationIdentifier) this->getLogManager() << LogLevel_Warning << "Start train stimulation detected.\n";
			}

			if(m_bStartTrain)
			{
				l_ui64TrainDate = l_rDynamicBoxContext->getInputChunkEndTime(0, j);
				l_ui64TrainChunkStartTime = l_rDynamicBoxContext->getInputChunkStartTime(0, j);
				l_ui64TrainChunkEndTime = l_rDynamicBoxContext->getInputChunkEndTime(0, j);
			}

		  }

		l_rDynamicBoxContext->markInputAsDeprecated(0,j);
	}


	if (m_bStartTrain) //the user has finished the training phase, now we start calculating on the buffered data (which is also called training) 
	{
		#if defined(HAS_CONCURRENCY)
		this->getLogManager() << LogLevel_Info << "Concurrent execution is enabled: "
		#if defined(HAS_TBB)
		   << "TBB\n";
		#else 
		   << "PPT\n";
		#endif
		;
		#endif
		this->getLogManager() << LogLevel_Info << "Starting actual calculations.\n";
		boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();

		itpp::mat P1;//used only for P300, contains the average of the P300 for target

		//1. We cache the data in the standard case and we sum the input in the P300 case (for a mean)
		//In the standard processing we calculate the covariance on the input and in the P300 case on the concatenated matrix
		for(int c=1; c<l_rStaticBoxContext.getInputCount(); c++) //first c=0 is stimulations and here we want only signal data
	    {
			bool firstRun = true;
			for(int i=0; i<l_rDynamicBoxContext->getInputChunkCount(c); i++) //iterate over the inputs of the box where each input must be a separated class
			{
				TParameterHandler<const IMemoryBuffer*> ip_pMemoryBuffer(m_vStreamDecoder[c-1]->getInputParameter(OVP_GD_Algorithm_SignalStreamDecoder_InputParameterId_MemoryBufferToDecode));
				ip_pMemoryBuffer=l_rDynamicBoxContext->getInputChunk(c, i);
				m_vStreamDecoder[c-1]->process();

				//HEADER
				if(m_vStreamDecoder[c-1]->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedHeader))
				{
				}

				//BUFFER
				if(m_vStreamDecoder[c-1]->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedBuffer))
				{
					TParameterHandler<IMatrix*> ip_pMatrix(m_vStreamDecoder[c-1]->getOutputParameter(OVP_GD_Algorithm_SignalStreamDecoder_OutputParameterId_Matrix));
					itpp::mat X = convert(*ip_pMatrix);

					if (m_IsP300 && c == 2) // third channel is second input signal channel which is "target" for P300
					{
						if (firstRun)
						{
							P1 = itpp::zeros(X.rows(),X.cols());
							firstRun = false;
						}

						P1 = P1 + X;
					}

					m_vBufferedSignalPerChannel[c-1]->push_back(X);
				}

				l_rDynamicBoxContext->markInputAsDeprecated(c-1, i);
			}
		}

		//quick data check
		for (vector_type<std::vector<itpp::mat>*>::iterator signalChannelsIt=m_vBufferedSignalPerChannel.begin(); signalChannelsIt!=m_vBufferedSignalPerChannel.end(); signalChannelsIt++)
		{
			if ((*signalChannelsIt)->size() == 0)
			{
				this->getLogManager() << LogLevel_Error << "Input channel contains no data!\n";
				return false;
			}
		}
			
		if (m_IsP300)
		{
			if (P1.rows()==0 || P1.cols()==0)
			{
				this->getLogManager() << LogLevel_Error << "Training failed. P1 is bad!\n";
				return false;
			}

			P1 = P1 / (int)m_vBufferedSignalPerChannel[1]->size(); //we calculate a mean for the second channel (second chnannel is "target" for P300)
		}

		//2. Calculate covariance matrices for each channel
		#if defined(HAS_CONCURRENCY)
        parallel_for(0, int(m_vBufferedSignalPerChannel.size()), [&](size_t i)
		#else
		for(int i=0;i<m_vBufferedSignalPerChannel.size();i++)
		#endif
		{
			std::vector<itpp::mat>* signalChannelsIt = m_vBufferedSignalPerChannel[i];

			for (std::vector<itpp::mat>::iterator it=signalChannelsIt->begin(); it!=signalChannelsIt->end(); it++)
			{
				  itpp::mat P;
				  itpp::mat X1 = *it;

				  if (m_IsP300) //second channel is target for P300
				  {
					  //concatenate 
					  //[P1]
					  //[X2]
					  itpp::mat XC = itpp::concat_vertical(P1,X1);

					  P = itpp::cov(XC.transpose(),false);
				  }
				  else
				  {
					  P = itpp::cov(X1.transpose(),false); //non target channel for P300 - calculate covariance
				  }
			  
				   m_vCovarianceMatricesPerChannel[i]->push_back(P); //0 based c
			}
	    }
		#if defined(HAS_CONCURRENCY) 
		); 
        #endif


		//4. Final calculation. Calculate a mean(Riemann mean) from all covariance matrices per channel(class) to produce a single covariance matrice - a barycenter in Riemann space that will characterize every class (saved as C0 ... Cn)
		
		//make quick check
		for(int i=0;i<m_vCovarianceMatricesPerChannel.size();i++)
		{
			if (m_vCovarianceMatricesPerChannel[i]->size()==0)
			{
				this->getLogManager() << LogLevel_Error << "Training failed! Matrices are bad!\n";
				return false;
			}
		}
		
		m_vResultMean.resize(m_vCovarianceMatricesPerChannel.size());

		//calculate and store result matrices
		bool badMatrixDetected = false;
		#if defined(HAS_CONCURRENCY)
        parallel_for(0, int(m_vCovarianceMatricesPerChannel.size()), [&](size_t i)
		#else
		for(int i=0;i<m_vCovarianceMatricesPerChannel.size();i++)
		#endif
		{
			std::vector<itpp::mat>* it = m_vCovarianceMatricesPerChannel[i];
			
			m_vResultMean[i] = Riemann::mean(*it);
			
		}
		#if defined(HAS_CONCURRENCY) 
		); 
        #endif

		//add the P1 also for the file output
		if (m_IsP300) 
		{
			m_vResultMean.push_back(P1);
		}

		//Print time
		boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration diff = t2 - t1;
		this->getLogManager() << LogLevel_Info << "Training completed! Calculation time: " << (double)diff.total_milliseconds() / double(1000) << " seconds. All OK! Saving file ...\n";
		
		//5. Save output C0 ... CN, P1 to a single parameter file
		saveFile();

		//6. Start auto-validation
		this->getLogManager() << LogLevel_Info << "Accuracy in auto-validation:\n";

		//We need to remove the last mean which is for P300 and not needed for testing accuracy
		if (m_IsP300)
			#if defined(HAS_CONCURRENCY)
			  m_vResultMean.resize(m_vResultMean.size()-1);
			#else
			  m_vResultMean.pop_back(); 
			#endif

		vector_type<int> l_vAccuracy=vector_type<int>(m_vResultMean.size());

		//the signal m_vCovarianceMatricesPerChannel per channel corresponds per class
		#if defined(HAS_CONCURRENCY)
        parallel_for(0, int(m_vCovarianceMatricesPerChannel.size()), [&](size_t i)
		#else
		for(int i=0;i<m_vCovarianceMatricesPerChannel.size();i++)
		#endif
		{
			std::vector<itpp::mat>* CovarianceMatriceIt = m_vCovarianceMatricesPerChannel[i];
			//test per class 
			for (std::vector<itpp::mat>::iterator it=CovarianceMatriceIt->begin(); it!=CovarianceMatriceIt->end(); it++)
			{
				std::vector<OpenViBE::float64> l_vDistances;

				//classify
				OpenViBE::uint32 index = SignalProcessing::CBoxAlgorithmProcessMDM::ApplyMDM(*it, m_vResultMean, l_vDistances);
				
				if (index == i) //if the predicted class match the expected
					l_vAccuracy[i]++;
			}
		}
		#if defined(HAS_CONCURRENCY) 
		); 
        #endif
		//print result auto-validation
		for(int i=0;i<m_vCovarianceMatricesPerChannel.size();i++)
		{
			this->getLogManager() << LogLevel_Info << "Class: C" << intToString(i).c_str() << " "<< l_vAccuracy[i] << " / " << m_vCovarianceMatricesPerChannel[i]->size() << "\n";
		}
		//end auto-validation

		//Send Train completed to the next box
		m_oStimulationEncoder.getInputStimulationSet()->appendStimulation(OVTK_StimulationId_TrainCompleted, l_ui64TrainDate, 0);
		m_oStimulationEncoder.encodeBuffer(0);
		l_rDynamicBoxContext->markOutputAsReadyToSend(0, l_ui64TrainChunkStartTime, l_ui64TrainChunkEndTime);
		
		m_bStartTrain = false;
	}

	return true;
}

itpp::mat CBoxAlgorithmTrainMDM::convert(const OpenViBE::IMatrix& rMatrix)
{
		itpp::mat l_oResult(
			rMatrix.getDimensionSize(1),
			rMatrix.getDimensionSize(0));

		System::Memory::copy(l_oResult._data(), rMatrix.getBuffer(), rMatrix.getBufferElementCount()*sizeof(float64));
		return l_oResult.transpose();
}

void CBoxAlgorithmTrainMDM::saveFile()
{
	if (!m_ParamFile.bad())
	{
		uint32 count = (m_IsP300) ? m_vResultMean.size()+1 : m_IsP300;

		m_ParamFile << "count: " << m_vResultMean.size() << std::endl;

		int i=0;
		for (int i=0;i<m_vResultMean.size();i++)
		{
			if (m_IsP300 && i == (m_vResultMean.size() - 1)) //the last one is supposed to be P1 for P300 mode
			{
				m_ParamFile << "P1" << std::endl << m_vResultMean[i] << std::endl;
				this->getLogManager() << LogLevel_Info << " Mean covariance matrix size of " << "P1: " << m_vResultMean[i].rows() << " " << m_vResultMean[i].cols()<< "\n";
			}
			else 
			{
				m_ParamFile << "C" << i << std::endl << m_vResultMean[i] << std::endl;
				this->getLogManager() << LogLevel_Info << " Mean covariance matrix size of " <<  "C" << i << ": " << m_vResultMean[i].rows() << " " << m_vResultMean[i].cols()<< "\n";
			}

			//checks: P1 must be equal to number of input data channels. C0 ... Cn must be double the size of P1 or double the size of the number of input data channels
		}

		m_ParamFile.flush();
		m_ParamFile.close();

		this->getLogManager() << LogLevel_Info << "Parameters file saved!\n";
	}
	else
	{
		this->getLogManager() << LogLevel_Error << "Result calculated successfully, but could not be saved to a file!\n";
		//potentially print to console
	}
}

std::string CBoxAlgorithmTrainMDM::intToString(int number)
{
	std::stringstream ss;
    ss << number;
    
	return ss.str();
}
