/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu,

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#if defined TARGET_OS_Windows

#ifndef __OpenViBEPlugins_Algorithm_ClassifierBayesPointMachine_H__
#define __OpenViBEPlugins_Algorithm_ClassifierBayesPointMachine_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <xml/IWriter.h>
#include <xml/IReader.h>

#include <stack>

//#if defined TARGET_HAS_ThirdPartyITPP

#include <itpp/itbase.h>

#define OVP_ClassId_Algorithm_BayesPointMachineClassifier                                       OpenViBE::CIdentifier(0x4776422A, 0x72B819FE)
#define OVP_ClassId_Algorithm_BayesPointMachineClassifierDesc                                   OpenViBE::CIdentifier(0x68862252, 0x6D752863)

namespace OpenViBEPlugins
{
	namespace Local
	{
		class CAlgorithmClassifierBayesPointMachine : public OpenViBEToolkit::CAlgorithmClassifier//, public XML::IWriterCallback, public XML::IReaderCallback
		{
			
		  public:

			CAlgorithmClassifierBayesPointMachine()
			{
				m_initialized = true;

				//trys to find the .NET dll where the algorithm is implemented
				//dll should be in OpenVibe's bin folder
				hDll = LoadLibrary("BayesPointMachineClassifier.dll");

				if (!hDll)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "BayesPointMachineClassifier.dll could not be loaded!!\n";
					m_initialized = false;
				}

				trainFunc = (TrainFunc)GetProcAddress(hDll, "Train");
				if (!trainFunc)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Exact function match not found in BayesPointMachineBridge.dll!\n";
					m_initialized = false;
				}

				classifyFunc = (ClassifyFunc)GetProcAddress(hDll, "Classify");
				if (!classifyFunc)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Exact function match not found in BayesPointMachineBridge.dll!\n";
					m_initialized = false;
				}

				getConfig = (GetConfig)GetProcAddress(hDll, "GetConfig");
				if (!getConfig)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Exact function match not found in BayesPointMachineBridge.dll!\n";
					m_initialized = false;
				}

				setConfig = (SetConfig)GetProcAddress(hDll, "SetConfig");
				if (!getConfig)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Exact function match not found in BayesPointMachineBridge.dll!\n";
					m_initialized = false;
				}
			}

		private:

			HMODULE hDll;
			int m_ui32NumberOfFeatures;

			OpenViBE::float64 m_f64Class1;
			OpenViBE::float64 m_f64Class2;

			static const int maxModelSize = 50000;
			unsigned char serializedModel[maxModelSize];
			int modelActualSize;

			typedef double (__stdcall *TrainFunc)(double values[], int maxSize, int actualSize, int vectorsSize);
			TrainFunc trainFunc;

			typedef double (__stdcall *ClassifyFunc)(double values[], int maxSize, int featuresCount);
			ClassifyFunc classifyFunc;

			typedef LPTSTR (__stdcall *GetConfig)(unsigned char bytes[], int maxSize, int actualSize);
			GetConfig getConfig;

			typedef int (__stdcall *SetConfig)(unsigned char bytes[], int maxSize);
			SetConfig setConfig;

			OpenViBE::boolean m_initialized;

			virtual OpenViBE::boolean train(
				const OpenViBEToolkit::IFeatureVectorSet& rFeatureVectorSet)
			{
				if (!this->m_initialized)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Could not initalize Bayesenian classifier!\n";
					return false;
				}

				std::map <  OpenViBE::float64,  OpenViBE::uint64 > l_vClassLabels;

				for(OpenViBE::uint32 i=0; i<rFeatureVectorSet.getFeatureVectorCount(); i++)
				{
					l_vClassLabels[rFeatureVectorSet[i].getLabel()]++;
				}

				if(l_vClassLabels.size() != 2)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "A BPM classifier can only be trained with 2 classes, not more, not less - got " << (OpenViBE::uint32)l_vClassLabels.size() << "\n";
					return false;
				}
				
				m_ui32NumberOfFeatures=rFeatureVectorSet[0].getSize();//enabled full feature set
				OpenViBE::uint32 l_ui32NumberOfFeatureVectors=rFeatureVectorSet.getFeatureVectorCount();

				//m_ui32NumberOfFeatures = 20; //reduce the number of features for testing
				int l_ui32NumberOfFeaturesWithOutput = m_ui32NumberOfFeatures  + 1;

				const int maxSize = 300 * 3000;
			
				const int actualSize = l_ui32NumberOfFeaturesWithOutput * l_ui32NumberOfFeatureVectors;

				if (maxSize < actualSize) 
				   this->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Problem with sizes!" << "\n";

				double values[maxSize];

				OpenViBE::uint32 k=0;
				
				for (OpenViBE::uint32 i=0;i<l_ui32NumberOfFeatureVectors;i++)
				{
					for (OpenViBE::uint32 j=0;j<m_ui32NumberOfFeatures;j++)
					{
					   values[k] = rFeatureVectorSet.getFeatureVector(i)[j];
					   k++;
					}
					values[k] = rFeatureVectorSet.getFeatureVector(i).getLabel();
					k++;
					//this->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Class Label:" << values[k-1]<< "\n";
				}

				double success = trainFunc(values, maxSize, actualSize, l_ui32NumberOfFeatureVectors);

				//process errors
				if (success==0)
				return true;
				else
				{
					if (success == 1)
					{
						this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Error in Bayes Point Machine classifier. See above!\n";
						return false;
					}
					if (success == 2)
					{
						this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Error in Bayes Point Machine classifier. Try to reduce the number of features to less than 20.\n";
						return false;
					}
				}
			}

			virtual OpenViBE::boolean loadConfiguration(const OpenViBE::IMemoryBuffer& rMemoryBuffer)
			{
				if (!this->m_initialized)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Could not initalize Bayesenian classifier!\n";
					return false;
				}

				#include <iostream>
				using namespace std;

				const OpenViBE::uint8* p = rMemoryBuffer.getDirectPointer();
				
				for (int i=0;i<rMemoryBuffer.getSize();i++)
				{
					serializedModel[i] = * (p + i);
				}

				//cout<< "rMemoryBuffer: " << rMemoryBuffer.getSize() << "\n";
				getConfig(serializedModel, maxModelSize, rMemoryBuffer.getSize());
				
				return true;
			}

			virtual OpenViBE::boolean saveConfiguration(OpenViBE::IMemoryBuffer& rMemoryBuffer)
			{
				if (!this->m_initialized)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Could not initalize Bayesenian classifier!\n";
					return false;
				}

				#include <iostream>
				using namespace std;

				//cout << "Saving configuration ...\n"; 

				modelActualSize = setConfig(serializedModel,maxModelSize);

				rMemoryBuffer.append((OpenViBE::uint8*)&serializedModel,(OpenViBE::uint64)modelActualSize);

				//cout << "Size:" << modelActualSize << "\n";

				return true;
			}

			virtual OpenViBE::boolean classify(
				const OpenViBEToolkit::IFeatureVector& rFeatureVector,
				OpenViBE::float64& rf64Class,
				OpenViBEToolkit::IVector& rClassificationValues)
			{	
				if (!this->m_initialized)
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Could not initalize Bayesenian classifier!\n";
					return false;
				}
				
				const int maxSize = 300;
				double values[maxSize];
				itpp::vec l_oFeatures(rFeatureVector.getBuffer(), rFeatureVector.getSize());
				m_ui32NumberOfFeatures = l_oFeatures.size();

				if (m_ui32NumberOfFeatures > maxSize) 
				{
					this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Input feature vector is too big for this implementation!\n";
					return false;
				}

				//convert to simple structure for transfer
				for(int i=0;i< m_ui32NumberOfFeatures; i++)
				{
					values[i] = l_oFeatures[i];
				}

				//send single feature vector for classification
				double result  = classifyFunc(values, maxSize, m_ui32NumberOfFeatures);

				//set result
				rClassificationValues.setSize(1);

				if (result>0.5) rf64Class = 1; else rf64Class = 2;
			
				rClassificationValues[0] = result;

				return true;
			}


			~CAlgorithmClassifierBayesPointMachine()
			{
			   m_initialized = false;
			}


			_IsDerivedFromClass_Final_(
			   CAlgorithmClassifier,
			   OVP_ClassId_Algorithm_BayesPointMachineClassifier);

			OpenViBE::float64 m_f64Class;

		}; 


		class CAlgorithmClassifierBayesPointMachineDesc : public OpenViBEToolkit::CAlgorithmClassifierDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Bayes Point Machine"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("CNRS/Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Bayes Point Machine classifier based on Infer.net using .NET"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("The actual implementation is .NET 4.0. This box simply acts as a bridge between OpenVibe and the .NET implementation. For more information see this article:http://research.microsoft.com/apps/pubs/default.aspx?id=65611"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_Algorithm_BayesPointMachineClassifier; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::Local::CAlgorithmClassifierBayesPointMachine; }

			virtual OpenViBE::boolean getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const
			{
				CAlgorithmClassifierDesc::getAlgorithmPrototype(rAlgorithmPrototype);
				return true;
			}

			_IsDerivedFromClass_Final_(CAlgorithmClassifierDesc, OVP_ClassId_Algorithm_BayesPointMachineClassifierDesc);
		};
	};
};

//#endif // TARGET_HAS_ThirdPartyITPP

#endif // __OpenViBEPlugins_Algorithm_ClassifierBayesPointMachine_H__

#endif