#ifndef ov_ov_comm_h
#define ov_ov_comm_h

#include "../SerialPort/Serial.h"
#if defined TARGET_OS_Windows

#include "../ParallelPort/ParallelPort.h"

using namespace OpenViBEVRDemos;

#endif 
//use multibyte character code required in VS

#include <iostream>

//every protocol policy requires different scenarios in OpenVibe!
enum eProtocolVersion
{
	TwobitPP=1, //Protocol based on 4 values 0,64,128,192 used with Mitsar amplifier

	EightbitPP, //Protocol that uses values from 0..255

	SoftwareTagging, //Does not use parallel port, but IPC (interprocess communication)
	//software tagging requires that the received stimulations in OpenVibe are seperated on different channels using the "m_software_tagging_target_stimulations_shift" and StimulationTransformer box 

	SerialPort, //Data is send to the serial port. Usually a virtual serial port is used that outputs the signal through USB.

	Robik,

};

class OVComm
{

  public:

	OVComm(eProtocolVersion ver,char* input);//the "input" parameter can be used by any of the tagging methods and can be different value for each one
	~OVComm();

	//Setting P300 target requires thse 2 methods + SetFinalized after each one
	void SetTargetRow(int);//zero based
    void SetTargetColumn(int);//zero based

    //A single flash requires ONE of these methods + SetFinalized
    void SetFlashRow(int);
    void SetFlashColumn(int);

	//These two methods combine the functionality of the other 4
	void SetRow(int, bool isTarget);//zero based
    void SetColumn(int, bool isTarget);//zero based

	void SetFinalized();

	//activates training, usually marks that input data has been provided and processing (xDawn filter trainging, classifier training) should begin
	//historically 192 on parallel port, also requires SetFinalized
    void SetBeginProcessing(); 

	void SetStartExperiment();

	void SetRepetionCompeted();

	void SetResetFlag();//Currently used to reset the Riemann Potato

    //void Train(int repetions,int flashCount);//does everything, no SetFinalized required, not used currently in BI or Robik UI

	void ConfigureTargetBaseRowCode(int newcode);
	void ConfigureTargetBaseColumnCode(int newcode);

	void ConfigureFlashBaseRowCode(int newcode);
	void ConfigureFlashBaseColumnCode(int newcode);

	void ConfigureRepetionCompletedCode(int newcode);

	void ConfigureExperimentStartCode(int newcode);
	void ConfigureTrainingStartCode(int newcode);

	void ConfigureResetCode(int newcode);

	//BI specific
	void BI_SetLevelCompleted();
	void BI_SetPositiveFeedback();
	void BI_SetNegativeFeedback();

	int  lLastError; //serial port errors

	//Returns the target row and column when provided with VRPN result from OpenVibe
	//It is used by the .NET wrapper
	int* ProcessVRPNResult(int numberOfRows,int numberOfColumns,double VRPNData[]);

   private:

	  eProtocolVersion m_eProtocol;

	  #if defined TARGET_OS_Windows
	  ParallelPort* m_pport;
      #endif

	  serialib* m_serial;

	  //Start codes definition variables for hardware tagging

	  int m_targetBaseRow; //used in Brain Invaders
	  int m_targetBaseColumn; //used in Brain Invaders

	  int m_flashBaseRow; //used in Brain Invaders
	  int m_flashBaseColumn; //used in Brain Invaders

	  int m_repetionCompleted;

	  int m_experimentStart; //Experiment Start
	  int m_beginTraining;   //Experiment End, used in Brain Invaders

	  int m_resetFlag; //used for reseting the Riemann potato

	  //end codes defintion 

	  void WriteSerial(int input);

	  #if defined ROBIK_TAGGING
	  int WriteMedoc(int input);//for RoBIK project
	  #endif 
};


#endif

