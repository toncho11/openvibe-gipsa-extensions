/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_Streaming_OutputChannel_H__
#define __OpenViBEPlugins_Streaming_OutputChannel_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "ovpCInputChannel.h"

/**
	Use this class to receive data and stimulations channels
*/
namespace OpenViBEPlugins
{
	namespace Streaming
	{
		class COutputChannel
		{
		private:
			typedef enum
			{	SIGNAL_CHANNEL,
				STIMULATION_CHANNEL,
			} channel_t;
		public:

			OpenViBE::boolean initialize(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm);
			OpenViBE::boolean uninitialize();
			
			void sendStimulation(OpenViBE::IStimulationSet* stimset, OpenViBE::uint64 startTimestamp, OpenViBE::uint64 endTimestamp);
			void prepareBlock1(OpenViBE::float64* pSrc, OpenViBE::uint64 startTimestamp, OpenViBE::uint64 endTimestamp);
			void prepareBlock2(OpenViBE::float64* pSrc);

			void sendHeader(const CInputChannel& inputChannel1, const CInputChannel& inputChannel2);
			OpenViBE::boolean   isWorking() {return m_bIsWorking;}

			OpenViBE::uint64 getSamplingRate(const bool firstBlock) {return firstBlock ? m_ui64SamplingRate1 : m_ui64SamplingRate2;}
			OpenViBE::uint64 getNbOfChannels(const bool firstBlock) {return firstBlock ? m_ui64NbOfChannels1 : m_ui64NbOfChannels2;}
			OpenViBE::uint64 getNbOfSamples(const bool firstBlock) {return firstBlock ? m_ui64NbOfSamples1 : m_ui64NbOfSamples2;}

		private:
			size_t              sizeofBlock(const OpenViBE::uint64 nbSamples) {return size_t(nbSamples*sizeof(OpenViBE::float64));}
			size_t              sizeBlockDst1() {return size_t(m_ui64NbOfChannels1*m_ui64NbOfSamples1);}
			size_t              sizeofBlockDst1() {return size_t(sizeBlockDst1()*sizeof(OpenViBE::float64));}
			OpenViBE::float64*  bufferBlockDst1() {return ip_pMatrixSignal->getBuffer();}
			OpenViBE::float64*  bufferBlockDst2() {return bufferBlockDst1() + sizeBlockDst1();}
			OpenViBE::float64*  bufferBlockDst2(const OpenViBE::uint64 channelIndex, const OpenViBE::uint64 offset = 0) {return bufferBlockDst2() + channelIndex*m_ui64NbOfSamples1 + offset;}
			size_t              sizeBlockSrc() {return size_t(m_ui64NbOfChannels2*m_ui64NbOfSamples2);}
			size_t              sizeofBlockSrc() {return size_t(sizeBlockSrc()*sizeof(OpenViBE::float64));}
			OpenViBE::float64*  bufferBlockSrc() {return &m_oMatrixWork[0];}
			OpenViBE::float64*  bufferBlockSrc(const OpenViBE::uint64 channelIndex, const OpenViBE::uint64 offset = 0) {return bufferBlockSrc() + channelIndex*m_ui64NbOfFullSamples2 + offset;}
			
			bool                appendBlock2(OpenViBE::float64* pSrc);
			void                processBlock2();
			void                interpolateBlock2();
			void                shiftBlock2();
			OpenViBE::boolean   sendSignal();

		protected:

			std::vector<OpenViBE::float64>                                          m_oMatrixWork;

			OpenViBE::boolean                                                       m_bIsWorking;
			OpenViBE::uint64                                                        m_ui64StartTimestamp;
			OpenViBE::uint64                                                        m_ui64EndTimestamp;
			OpenViBE::uint32                                                        m_ui32CurrentSamples;
			OpenViBE::uint32                                                        m_ui32CurrentSamplesLim;
			OpenViBE::uint32                                                        m_ui32ProcessedSamples;
			OpenViBE::uint32                                                        m_ui64InterpolateIndex;
			OpenViBE::uint64                                                        m_ui64InterpolateShift;
		
			OpenViBE::uint64                                                        m_ui64SamplingRate1;
			OpenViBE::uint64                                                        m_ui64NbOfChannels1;
			OpenViBE::uint64                                                        m_ui64NbOfSamples1;
			OpenViBE::uint64                                                        m_ui64SamplingRate2;
			OpenViBE::uint64                                                        m_ui64NbOfChannels2;
			OpenViBE::uint64                                                        m_ui64NbOfSamples2;
			OpenViBE::uint64                                                        m_ui64NbOfFullSamples2;

			// parent memory
			OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>*     m_pTBoxAlgorithm;
			
			// signal section
			OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamEncoderSignal;
			
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* >	    op_pMemoryBufferSignal;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* >				ip_pMatrixSignal;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::uint64 >				ip_ui64SamplingRateSignal;			
			
			// stimulation section
			OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamEncoderStimulation;

			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* >		op_pMemoryBufferStimulation;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* >		ip_pStimulationSetStimulation;
		};
	};
};

#endif // __OpenViBEPlugins_OutputChannel_H__
