This source can be compiled with the git source tree of OpenVibe:

https://gitlab.inria.fr/openvibe


This is for OpenVibe 2.2 or later. It works with OpenVibe 3.4.

Place the contents of this folder in: openvibe-src\extras\externals\openvibe-plugins-gipsa
Like that: openvibe-src\extras\externals\openvibe-plugins-gipsa\src\ovpCBoxAlgorithmP300Tagger.cpp

The OpneVibe build system will produce the openvibe-plugins-gipsa.dll.

The file will be placed in: \openvibe-src\dist\x64\Release\bin

Start the designer with openvibe-designer.cmd from folder: \openvibe-src\dist\x64\Release

In OpenVibe and you will have the category "Gipsa-lab" in the Designer.

NOTE:
Another version of the designer exists in openvibe-2.2.0-src\dist\designer-Release-x86 which will NOT contain the gipsa-lab extensions.