/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#if defined TARGET_HAS_COMMUNICATION_LIB

#ifndef __OpenViBEPlugins_BoxAlgorithm_Tagger_H__
#define __OpenViBEPlugins_BoxAlgorithm_Tagger_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>
#include <deque>
#include <vector>

#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>

#include <sstream>
#include <exception>

#include "Communication/OVComm.h"

#define OVP_ClassId_BoxStimulationTransformer       OpenViBE::CIdentifier(0x57035FC6, 0x03D53C93)
#define OVP_ClassId_BoxStimulationTransformerDesc   OpenViBE::CIdentifier(0x43CA7942, 0x19345340)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/*
		  Performs hardware tagging or software tagging based on input stimulations.
		  This box performs tagging without UI interaction. Thus it allow software and hardware tagging
		  to be compared more precisely. Usually the output is saved to a GDF and analyzed with Matlab. 
		*/
		class CBoxTagger : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(OpenViBE::uint32 ui32InputIndex);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxStimulationTransformer);

			//std::vector < ConversionEntry > m_vConversionEntries;

		protected:

			//stimulation input 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoderTrigger;
			OpenViBE::Kernel::TParameterHandler <const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulationSetTrigger;

			OpenViBE::uint64 m_uint64StimCode1;
			OpenViBE::uint64 m_uint64StimCode2;

			OVComm* commHardwareT;//gipsa
			OVComm* commSoftwareT;//gipsa
			OpenViBE::uint64 m_uint64rawValue;
		};

		class CBoxTaggerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Tagger"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Performs hardware tagging or/and software tagging based on input stimulations."); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			//virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Signal processing/Basic"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-jump-to"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxStimulationTransformer; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessing::CBoxTagger; }

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Input stimulation channel",		OV_TypeId_Stimulations);

				//3 parameters: value, stim on, stim off
				rBoxAlgorithmPrototype.addSetting("Stimulation on:"     ,OV_TypeId_Stimulation,"OVTK_GDF_Lights_On");
				rBoxAlgorithmPrototype.addSetting("Stimulation off:"    ,OV_TypeId_Stimulation,"OVTK_GDF_Lights_Off");
				rBoxAlgorithmPrototype.addSetting("P300 row value:",OV_TypeId_Integer,"5");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxStimulationTransformerDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_Tagger_H__

#endif