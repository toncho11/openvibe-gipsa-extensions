/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_BoxAlgorithm_RiemannPotato_H__
#define __OpenViBEPlugins_BoxAlgorithm_RiemannPotato_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>
#include <deque>
#include <vector>

#include <fstream>
#include <string>

#include <sstream>
#include <exception>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;

//#include <itpp/signal/fastica.h>
#include <itpp/base/algebra/eigen.h>
#include <itpp/base/algebra/inv.h>
#include <itpp/base/converters.h>
#include <itpp/stat/misc_stat.h>

#include <itpp/itsignal.h>

#define OVP_ClassId_BoxAlgorithm_RiemannPotato     OpenViBE::CIdentifier(0x79FE0051, 0x49376D18)
#define OVP_ClassId_BoxAlgorithm_RiemannPotatoDesc OpenViBE::CIdentifier(0x0F282389, 0x4BA664C2)

namespace OpenViBEPlugins
{
	namespace SignalProcessingGpl
	{
		class CBoxAlgorithmRiemannPotato : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(const size_t ui32InputIndex);
			virtual bool process(void);

			double CalculateRiemannDistance(itpp::mat A,itpp::mat B);
			void UpdateReferencePoint(itpp::mat current,double alpha);
			void UpdateTreshold(double riemannenanDistance,double alpha);
            void Reset(IBoxIO* rDynamicBoxContext,int chunkCount);


			itpp::mat convert(const IMatrix& rMatrix);

			_IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_RiemannPotato);

		protected:

			//epoched signal input
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalDecoder;

			//> init input stimulation
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoderTrigger;
			OpenViBE::Kernel::TParameterHandler <const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulationSetTrigger;

			//OpenViBE::Kernel::TParameterHandler < uint64_t > op_ui64SamplingRateTrigger;

			//Output stimulation channel
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer1;

			//parameters
			uint64_t m_ui64StartTrainingStim;
			uint64_t m_ui64StopTrainingStim;
			uint64_t m_ui64StartArtifactStim;
			uint64_t m_ui64StopArtifactStim;
            uint64_t m_ui64ResetStim;

			uint64_t  m_ui64AdaptationSpeed;//change to 32
			double m_f64ThresholdParameter; //should be const 

			itpp::mat m_currentReferencePoint;

			double m_f64Threshold;
			uint64_t  m_ui64Iteration;//change to 32 
			double m_currentMean;
			double m_currentStd;
            bool m_bStartTrain;

			bool m_isInArtifactNow;
		};

		class CBoxAlgorithmRiemannPotatoDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Riemann Potato"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev and Alexandre Barachant PhD"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Automatic artifact detection using Riemannian geometry."); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("The input stimulation channel expects: Start/Stop training (box on/off) or reset stimulation that will restart the training. The output channel supplies the Start/Stop Artifact stimulations that give the interval of the artifact."); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-jump-to"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_RiemannPotato; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessingGpl::CBoxAlgorithmRiemannPotato; }

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				//Input
				rBoxAlgorithmPrototype.addInput  ("Epoched signal",		           OV_TypeId_Signal		);
				rBoxAlgorithmPrototype.addInput  ("Input stimulation channel",	   OV_TypeId_Stimulations);

				//Output
				rBoxAlgorithmPrototype.addOutput ("Output stimulations channel",   OV_TypeId_Stimulations	); //when artefact starts and when it ends

				//Parameters
				rBoxAlgorithmPrototype.addSetting("Start training stimulation",    OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStart"	);
				rBoxAlgorithmPrototype.addSetting("Stop trining stimulation",      OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStop"	);
				rBoxAlgorithmPrototype.addSetting("Start artifact stimulation",    OV_TypeId_Stimulation, "OVTK_StimulationId_SegmentStart"	);
				rBoxAlgorithmPrototype.addSetting("Stop artifact stimulation",     OV_TypeId_Stimulation, "OVTK_StimulationId_SegmentStop"	);
				rBoxAlgorithmPrototype.addSetting("Reset Box stimulation",         OV_TypeId_Stimulation, "OVTK_StimulationId_Reset"	);

				rBoxAlgorithmPrototype.addSetting("Speed of adaptation:",      OV_TypeId_Integer,    "100");
				rBoxAlgorithmPrototype.addSetting("Threshold parameter:",                OV_TypeId_Float,      "2.5");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_RiemannPotatoDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_RiemannPotato_H__
