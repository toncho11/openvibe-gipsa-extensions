/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu, Goyat Matthieu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCBoxAlgorithmPulsarTagging.h"

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessing;

#include <iostream>

namespace
{
	class _AutoCast_
	{
	public:
		_AutoCast_(IBox& rBox, IConfigurationManager& rConfigurationManager, const uint32_t ui32Index) : m_rConfigurationManager(rConfigurationManager) { rBox.getSettingValue(ui32Index, m_sSettingValue); }
		operator uint64_t (void) { return m_rConfigurationManager.expandAsUInteger(m_sSettingValue); }
		operator int64_t (void) { return m_rConfigurationManager.expandAsInteger(m_sSettingValue); }
		operator double (void) { return m_rConfigurationManager.expandAsFloat(m_sSettingValue); }
		operator bool (void) { return m_rConfigurationManager.expandAsBoolean(m_sSettingValue); }
		operator const CString (void) { return m_sSettingValue; }
	protected:
		IConfigurationManager& m_rConfigurationManager;
		CString m_sSettingValue;
	};
};

bool CBoxAlgorithmPulsarTagging::initialize(void)
{
	const IBox& l_rStaticBoxContext=this->getStaticBoxContext();

	//> init Trigger input 1
	m_pSignalDecoderTrigger1=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalDecoder));
	m_pSignalDecoderTrigger1->initialize();
	ip_pMemoryBufferToDecodeTrigger1.initialize(m_pSignalDecoderTrigger1->getInputParameter(OVP_GD_Algorithm_SignalDecoder_InputParameterId_MemoryBufferToDecode));
	op_pDecodedMatrixTrigger1.initialize(m_pSignalDecoderTrigger1->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Matrix));
	op_ui64SamplingRateTrigger.initialize(m_pSignalDecoderTrigger1->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Sampling));

	//> init Trigger input 2
	m_pSignalDecoderTrigger2=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalDecoder));
	m_pSignalDecoderTrigger2->initialize();
	ip_pMemoryBufferToDecodeTrigger2.initialize(m_pSignalDecoderTrigger2->getInputParameter(OVP_GD_Algorithm_SignalDecoder_InputParameterId_MemoryBufferToDecode));
	op_pDecodedMatrixTrigger2.initialize(m_pSignalDecoderTrigger2->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Matrix));
	
	//> used to decode the next value 
	m_pNextSignalDecoderTrigger=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalDecoder));
	m_pNextSignalDecoderTrigger->initialize();
	m_ip_pNextMemoryBufferToDecodeTrigger.initialize(m_pNextSignalDecoderTrigger->getInputParameter(OVP_GD_Algorithm_SignalDecoder_InputParameterId_MemoryBufferToDecode));
	m_op_pNextDecodedMatrixTrigger.initialize(m_pNextSignalDecoderTrigger->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Matrix));

	//> init stimulation output 1
	m_pStimulationEncoder1=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationEncoder));
	m_pStimulationEncoder1->initialize();
	ip_pStimulationsToEncode1.initialize(m_pStimulationEncoder1->getInputParameter(OVP_GD_Algorithm_StimulationEncoder_InputParameterId_StimulationSet));
	op_pEncodedMemoryBuffer1.initialize(m_pStimulationEncoder1->getOutputParameter(OVP_GD_Algorithm_StimulationEncoder_OutputParameterId_EncodedMemoryBuffer));

	//> get conversion list and parallel port policy
	m_sConversionListFileLocation=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_ui32Policy=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	m_f64Last1 = 0; 
	m_f64BeforeLast1 = 0;

	m_f64Last2 = 0; 
	m_f64BeforeLast2 = 0;

	m_ui32CurrentTrialStimulation = OVTK_StimulationId_Label_01;

	m_i32TriggerChannelCount=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	return true;
}

bool CBoxAlgorithmPulsarTagging::uninitialize(void)
{
	// uninit trigger input
	m_pSignalDecoderTrigger1->uninitialize();
	ip_pMemoryBufferToDecodeTrigger1.uninitialize();
	op_pDecodedMatrixTrigger1.uninitialize();
	op_ui64SamplingRateTrigger.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pSignalDecoderTrigger1);

	
	// uninit trigger input
	m_pSignalDecoderTrigger2->uninitialize();
	ip_pMemoryBufferToDecodeTrigger2.uninitialize();
	op_pDecodedMatrixTrigger2.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pSignalDecoderTrigger2);

	// uninit the extra value processing
	m_pNextSignalDecoderTrigger->uninitialize();
	m_ip_pNextMemoryBufferToDecodeTrigger.uninitialize();
	m_op_pNextDecodedMatrixTrigger.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pNextSignalDecoderTrigger);
	
	// uninit stimulation output 1 
	m_pStimulationEncoder1->uninitialize();
	ip_pStimulationsToEncode1.uninitialize();
	op_pEncodedMemoryBuffer1.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationEncoder1);

	return true;
}

bool CBoxAlgorithmPulsarTagging::processInput(const size_t ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

bool CBoxAlgorithmPulsarTagging::process(void)
{
	//> Get dynamic box context
	IBoxIO* l_pDynamicBoxContext=getBoxAlgorithmContext()->getDynamicBoxContext();

	uint32_t trialsChannelIndex = 0;

	// Process trials on first input channel
	for(uint32_t j=0; j<l_pDynamicBoxContext->getInputChunkCount(trialsChannelIndex); j++)
	{
			ip_pMemoryBufferToDecodeTrigger1=l_pDynamicBoxContext->getInputChunk(trialsChannelIndex,j);
			m_pSignalDecoderTrigger1->process();

			ip_pStimulationsToEncode1->clear();//st 1
			

			uint64_t l_ui64ChunkStartTime = l_pDynamicBoxContext->getInputChunkStartTime(trialsChannelIndex, j);
			uint64_t l_ui64ChunkEndTime = l_pDynamicBoxContext->getInputChunkEndTime(trialsChannelIndex, j);

			// Process trials on first input channel
			if(m_pSignalDecoderTrigger1->isOutputTriggerActive(OVP_GD_Algorithm_SignalDecoder_OutputTriggerId_ReceivedBuffer))
			{
				int size = op_pDecodedMatrixTrigger1->getDimensionSize(1);
			
				for (uint32_t i = 0; i<op_pDecodedMatrixTrigger1->getDimensionSize(1); i++)
				{
					//get value 
					double l_f64CurrentValue1 = op_pDecodedMatrixTrigger1->getBuffer()[i];
			
					//only policy 3 is available which corrects for a hardware glitch
					m_ui32Policy = 3;

					bool valueCheckHF = ((m_i32TriggerChannelCount == 2) ? ((l_f64CurrentValue1 != 0) && (m_f64Last1 != 0)) : (l_f64CurrentValue1 == 1) && (m_f64Last1 == 1));
					bool valueCheckRE = ((m_i32TriggerChannelCount == 2) ? (l_f64CurrentValue1 != 0) : (l_f64CurrentValue1==1));

					if (m_ui32Policy == 3) //detects a hardware glitch of 1 sample that we ignore and take the value of the next sample, one sample delay is induced in the end when tagging but the correct stimulation is produced
					{
						if (
							( //hardware fix
							valueCheckHF
							&& (m_f64Last1 != m_f64BeforeLast1) 
							&& (l_f64CurrentValue1 != m_f64Last1) 
							&& (m_f64BeforeLast1==0) 
							)
						   ||
							( //regular case
							valueCheckRE 
							&& (m_f64BeforeLast1 == 0)
							&& (l_f64CurrentValue1==m_f64Last1)
							)
						   )
						   {
							   uint64_t l_ui64sampleTime = l_ui64ChunkStartTime + uint64_t(((i) / double(op_ui64SamplingRateTrigger) )*(1LL << 32));
								ip_pStimulationsToEncode1->appendStimulation(m_ui32CurrentTrialStimulation,l_ui64sampleTime, 0); //add to st1
							}
						}
				

						m_f64BeforeLast1 = m_f64Last1;
						m_f64Last1 = l_f64CurrentValue1;
				 }
			}

			//output stimulation set 1
		    op_pEncodedMemoryBuffer1=l_pDynamicBoxContext->getOutputChunk(0);
			m_pStimulationEncoder1->process(OVP_GD_Algorithm_StimulationEncoder_InputTriggerId_EncodeBuffer);
			l_pDynamicBoxContext->markOutputAsReadyToSend(0,l_ui64ChunkStartTime ,l_ui64ChunkEndTime );
			
		    l_pDynamicBoxContext->markInputAsDeprecated(0,j);

	}
	
	//1 channels, GTEC 
	//2 channels, BIOPAC 
	int32_t classChannelIndex = m_i32TriggerChannelCount - 1; 

	// Process classes on second input channel or the same
	for (uint32_t j = 0; j<l_pDynamicBoxContext->getInputChunkCount(classChannelIndex); j++)
	{
			ip_pMemoryBufferToDecodeTrigger2=l_pDynamicBoxContext->getInputChunk(classChannelIndex,j);
			m_pSignalDecoderTrigger2->process();

			uint64_t l_ui64ChunkStartTime = l_pDynamicBoxContext->getInputChunkStartTime(classChannelIndex, j);
			uint64_t l_ui64ChunkEndTime = l_pDynamicBoxContext->getInputChunkEndTime(classChannelIndex, j);

			if(m_pSignalDecoderTrigger2->isOutputTriggerActive(OVP_GD_Algorithm_SignalDecoder_OutputTriggerId_ReceivedBuffer))
			{
				int size = op_pDecodedMatrixTrigger2->getDimensionSize(1);
			
				for (uint32_t i = 0; i<op_pDecodedMatrixTrigger2->getDimensionSize(1); i++)
				{
					//get value 
					double l_f64CurrentValue2 = op_pDecodedMatrixTrigger2->getBuffer()[i];
			
					//only policy 3 is available which corrects for a hardware glitch
					m_ui32Policy = 3;

					bool valueCheckHF = ((m_i32TriggerChannelCount == 2) ? ((l_f64CurrentValue2 != 0) && (m_f64Last1 != 0)) : (l_f64CurrentValue2 == 2) && (m_f64Last1 == 2));
					bool valueCheckRE = ((m_i32TriggerChannelCount == 2) ? (l_f64CurrentValue2 != 0) : (l_f64CurrentValue2==2));

					if (m_ui32Policy == 3) //detects a hardware glitch of 1 sample that we ignore and take the value of the next sample, one sample delay is induced in the end when tagging but the correct stimulation is produced
					{
						if (
							( //hardware fix
							valueCheckHF
							&& (m_f64Last2 != m_f64BeforeLast2) 
							&& (l_f64CurrentValue2 != m_f64Last2) 
							&& (m_f64BeforeLast2==0) 
							)
						   ||
							( //regular case
							valueCheckRE
							&& (m_f64BeforeLast2 == 0)
							&& (l_f64CurrentValue2==m_f64Last2)
							)
						   )
						   {
							  m_ui32CurrentTrialStimulation = m_ui32CurrentTrialStimulation + 1;	
						   }
					}
				

						m_f64BeforeLast2 = m_f64Last2;
						m_f64Last2 = l_f64CurrentValue2;
				 }
			}
			
		    l_pDynamicBoxContext->markInputAsDeprecated(classChannelIndex,j);
     }

	return true;
}// End Process
