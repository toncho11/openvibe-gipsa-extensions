/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_Decimation_Signal_Merger_H__
#define __OpenViBEPlugins_Decimation_Signal_Merger_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <vector>
#include <cstdio>
#include <fstream>
#include "ovpCInputChannel.h"
#include "ovpCOutputChannel.h"

#define OVP_ClassId_BoxAlgorithm_StreamMergerWithResampling     OpenViBE::CIdentifier(0x2368472D, 0x18AF4A69)
#define OVP_ClassId_BoxAlgorithm_StreamMergerWithResamplingDesc OpenViBE::CIdentifier(0x6C68231B, 0x22620E18)

namespace OpenViBEPlugins
{
	namespace Streaming
	{
		class CBoxAlgorithmStreamMergerWithResampling : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:
			CBoxAlgorithmStreamMergerWithResampling();

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(const size_t ui32InputIndex);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_StreamMergerWithResampling);

		protected:

			CInputChannel     m_oCInputChannel1;
			CInputChannel     m_oCInputChannel2;
			
			COutputChannel    m_oCOutputChannel;
		};

		class CBoxAlgorithmStreamMergerWithResamplingDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Stream merger with resampling"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Gelu Ionescu"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Uses the frequency of the first channel as a reference and resamples the second one to this frequency. Then it merges the the two input signals"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("This box overcomes some of the problems with the original Stream Merger box such as the requirements for: same frequency, same number of samples per chunk, etc."); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-sort-ascending"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_StreamMergerWithResampling; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::Streaming::CBoxAlgorithmStreamMergerWithResampling; }

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Input 1", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput  ("Input Stimulations 1",  OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput  ("Input 2", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput  ("Input Stimulations 2",  OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput ("Merged Data", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput ("Output stimulation", OV_TypeId_Stimulations);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StreamMergerWithResamplingDesc);
		};
	};
};

#endif // __OpenViBEPlugins_Decimation_Signal_Merger_H__
