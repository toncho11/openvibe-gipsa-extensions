/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_Streaming_OutputChannel_H__
#define __OpenViBEPlugins_Streaming_OutputChannel_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "ovpCInputChannel.h"

/**
	Use this class to receive data and stimulations channels
*/
namespace OpenViBEPlugins
{
	namespace Streaming
	{
		class COutputChannel
		{
		private:
			typedef enum
			{	SIGNAL_CHANNEL,
				STIMULATION_CHANNEL,
			} channel_t;
		public:

			bool initialize(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm);
			bool uninitialize();
			
			void sendStimulation(OpenViBE::IStimulationSet* stimset, uint64_t startTimestamp, uint64_t endTimestamp);
			void prepareBlock1(double* pSrc, uint64_t startTimestamp, uint64_t endTimestamp);
			void prepareBlock2(double* pSrc);

			void sendHeader(const CInputChannel& inputChannel1, const CInputChannel& inputChannel2);
			bool   isWorking() {return m_bIsWorking;}

			uint64_t getSamplingRate(const bool firstBlock) {return firstBlock ? m_ui64SamplingRate1 : m_ui64SamplingRate2;}
			uint64_t getNbOfChannels(const bool firstBlock) {return firstBlock ? m_ui64NbOfChannels1 : m_ui64NbOfChannels2;}
			uint64_t getNbOfSamples(const bool firstBlock) {return firstBlock ? m_ui64NbOfSamples1 : m_ui64NbOfSamples2;}

		private:
			size_t              sizeofBlock(const uint64_t nbSamples) {return size_t(nbSamples*sizeof(double));}
			size_t              sizeBlockDst1() {return size_t(m_ui64NbOfChannels1*m_ui64NbOfSamples1);}
			size_t              sizeofBlockDst1() {return size_t(sizeBlockDst1()*sizeof(double));}
			double*  bufferBlockDst1() {return ip_pMatrixSignal->getBuffer();}
			double*  bufferBlockDst2() {return bufferBlockDst1() + sizeBlockDst1();}
			double*  bufferBlockDst2(const uint64_t channelIndex, const uint64_t offset = 0) {return bufferBlockDst2() + channelIndex*m_ui64NbOfSamples1 + offset;}
			size_t              sizeBlockSrc() {return size_t(m_ui64NbOfChannels2*m_ui64NbOfSamples2);}
			size_t              sizeofBlockSrc() {return size_t(sizeBlockSrc()*sizeof(double));}
			double*  bufferBlockSrc() {return &m_oMatrixWork[0];}
			double*  bufferBlockSrc(const uint64_t channelIndex, const uint64_t offset = 0) {return bufferBlockSrc() + channelIndex*m_ui64NbOfFullSamples2 + offset;}
			
			bool                appendBlock2(double* pSrc);
			void                processBlock2();
			void                interpolateBlock2();
			void                shiftBlock2();
			bool   sendSignal();

		protected:

			std::vector<double>                                          m_oMatrixWork;

			bool                                                       m_bIsWorking;
			uint64_t                                                        m_ui64StartTimestamp;
			uint64_t                                                        m_ui64EndTimestamp;
			uint32_t                                                        m_ui32CurrentSamples;
			uint32_t                                                        m_ui32CurrentSamplesLim;
			uint32_t                                                        m_ui32ProcessedSamples;
			uint32_t                                                        m_ui64InterpolateIndex;
			uint64_t                                                        m_ui64InterpolateShift;
		
			uint64_t                                                        m_ui64SamplingRate1;
			uint64_t                                                        m_ui64NbOfChannels1;
			uint64_t                                                        m_ui64NbOfSamples1;
			uint64_t                                                        m_ui64SamplingRate2;
			uint64_t                                                        m_ui64NbOfChannels2;
			uint64_t                                                        m_ui64NbOfSamples2;
			uint64_t                                                        m_ui64NbOfFullSamples2;

			// parent memory
			OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>*     m_pTBoxAlgorithm;
			
			// signal section
			OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamEncoderSignal;
			
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* >	    op_pMemoryBufferSignal;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* >				ip_pMatrixSignal;
			OpenViBE::Kernel::TParameterHandler < uint64_t >				ip_ui64SamplingRateSignal;			
			
			// stimulation section
			OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamEncoderStimulation;

			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* >		op_pMemoryBufferStimulation;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* >		ip_pStimulationSetStimulation;
		};
	};
};

#endif // __OpenViBEPlugins_OutputChannel_H__
