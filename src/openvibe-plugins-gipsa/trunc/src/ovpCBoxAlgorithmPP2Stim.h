/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu, Goyat Matthieu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_BoxAlgorithm_PP2Stim_H__
#define __OpenViBEPlugins_BoxAlgorithm_PP2Stim_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>
#include <deque>
#include <vector>

#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>

#include <sstream>
#include <exception>

#define OVP_ClassId_BoxAlgorithm_PP2Stim     OpenViBE::CIdentifier(0x64BA3B50, 0x69804FCB)
#define OVP_ClassId_BoxAlgorithm_PP2StimDesc OpenViBE::CIdentifier(0x764A32D9, 0x7877536A)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmPP2Stim : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(const size_t ui32InputIndex);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_PP2Stim);

			struct ConversionEntry //maps incoming number values to output stimulation and selects output channel
			{
				uint64_t Number;
				uint64_t StimulationCode;
				uint32_t OutputChannel;

				ConversionEntry(uint64_t pNumber, uint64_t pStimulationCode, uint32_t pOutputChannel) : Number(pNumber), StimulationCode(pStimulationCode), OutputChannel(pOutputChannel)
				{

				}
			};

			std::vector < ConversionEntry > m_vConversionEntries;

		protected:

			//trigger input
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalDecoderTrigger;
			OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pDecodedMatrixTrigger;
			OpenViBE::Kernel::TParameterHandler < uint64_t > op_ui64SamplingRateTrigger;

			//used for next processing
			//OpenViBE::Kernel::IAlgorithmProxy* m_pNextSignalDecoderTrigger;
	        //OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > m_ip_pNextMemoryBufferToDecodeTrigger;
	        //OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > m_op_pNextDecodedMatrixTrigger;

			//Stimulation 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer1;

			//Stimulation 2
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder2;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode2;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer2;

			//OpenViBE::uint64 m_ui64StimulationToTrigUp;
			//OpenViBE::uint64 m_ui64StimulationToTrigDown;
			OpenViBE::CString m_sConversionListFileLocation;
			uint64_t m_ui32Policy;

			double m_f64CurrentValue;
			double m_f64Last;
			double m_f64BeforeLast;

			//Example file:
			//64,0x00008101,1
            //128,0x00008102,2
            //192,0x00008103,1
			bool ReadConversionList();

		};

		class CBoxAlgorithmPP2StimDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Parallel Port to Stimulation"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Goyat Matthieu"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Convert Trigger to stimulation"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			//virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Signal processing/Basic"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-jump-to"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_PP2Stim; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessing::CBoxAlgorithmPP2Stim; }

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Trigger channel",		OV_TypeId_Signal		);
				rBoxAlgorithmPrototype.addOutput ("Output stimulations 1",	OV_TypeId_Stimulations	);
				rBoxAlgorithmPrototype.addOutput ("Output stimulations 2",	OV_TypeId_Stimulations	);

				/*rBoxAlgorithmPrototype.addSetting("Stimulation for rising edge",    OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00"	);
				rBoxAlgorithmPrototype.addSetting("Stimulation for downward edge",  OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00"	);*/
				rBoxAlgorithmPrototype.addSetting("Conversion list location:",OV_TypeId_Filename,"");
				
				//1 normal rising edge
				//3 enables correction for tranformation from usb to parallel port with Arduino converter
				rBoxAlgorithmPrototype.addSetting("Policy:",OV_TypeId_Integer, "3");


				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_PP2StimDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_Stim2Signal_H__
