/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_BoxAlgorithm_StimulationTransformer_H__
#define __OpenViBEPlugins_BoxAlgorithm_StimulationTransformer_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>
#include <deque>
#include <vector>

#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>

#include <sstream>
#include <exception>

#define OVP_ClassId_BoxStimulationTransformer       OpenViBE::CIdentifier(0x2E0D2C8D, 0x1DA84D36)
#define OVP_ClassId_BoxStimulationTransformerDesc   OpenViBE::CIdentifier(0x1BD92639, 0x39B5671A)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/*
		  Converts stimulations from one to another (changes stimulation code) and redirects to specific output channel 
		*/
		class CBoxStimulationTransformer : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(const size_t ui32InputIndex);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxStimulationTransformer);

			struct ConversionEntry //maps incoming stimulations to output stimulations with the channel requested
			{
				uint64_t InputStimulation;
				uint64_t OutputStimulation;
				uint32_t OutputChannel;

				ConversionEntry(uint64_t pInputStimulation, uint64_t pOutputStimulation, uint32_t pOutputChannel) : InputStimulation(pInputStimulation), OutputStimulation(pOutputStimulation), OutputChannel(pOutputChannel)
				{

				}
			};

			std::vector < ConversionEntry > m_vConversionEntries;

		protected:

			//stimulation input 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoderTrigger;
			OpenViBE::Kernel::TParameterHandler <const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulationSetTrigger;

			//Stimulation 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer1;

			//Stimulation 2
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder2;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode2;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer2;

			OpenViBE::CString m_sConversionListFileLocation;

			//Example file:
			//0x00008100,0x00008101,1
            //0x0000810A,0x00008102,2
            //0x0000810B,0x00008103,1
			bool ReadConversionList();
		};

		class CBoxStimulationTransformerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Stimulation Transformer"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Replaces stimulations and/or redirects them."); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("Enables you to replace one stimulation with another one (from a list) and to redirect stimulations to another channel."); }
			//virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Signal processing/Basic"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-jump-to"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxStimulationTransformer; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessing::CBoxStimulationTransformer; }

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Input stimulation channel",		OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput ("Output stimulations 1",	        OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput ("Output stimulations 2",	        OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Transformation file list location:",OV_TypeId_Filename,"");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxStimulationTransformerDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_StimulationTransformer_H__
