//#ifndef _DEBUG 

#include "ovpCBoxAlgorithmXDAWN2SpatialFilterTrainer.h"

#include <complex>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <map>

#include <itpp/base/algebra/eigen.h>
#include <itpp/base/algebra/inv.h>
#include <itpp/base/converters.h>
#include <itpp/stat/misc_stat.h>
#include <itpp/itbase.h>

//These lines are needed for use of cout and endl


using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessingGpl;
using namespace OpenViBE::Toolkit;

using namespace itpp;

using std::cout;
using std::endl;
// Taken from http://techlogbook.wordpress.com/2009/08/12/adding-generalized-eigenvalue-functions-to-it
//            http://techlogbook.wordpress.com/2009/08/12/calling-lapack-functions-from-c-codes
//            http://sourceforge.net/projects/itpp/forums/forum/115656/topic/3363490?message=7557038
//
// http://icl.cs.utk.edu/projectsfiles/f2j/javadoc/org/netlib/lapack/DSYGV.html
// http://www.lassp.cornell.edu/sethna/GeneDynamics/NetworkCodeDocumentation/lapack_8h.html#a17

namespace
{
	extern "C"
	{
		// This symbol comes from LAPACK
/*
		void zggev_(char *jobvl, char *jobvr, int *n, std::complex<double> *a,
			int *lda, std::complex<double> *b, int *ldb, std::complex<double> *alpha,
			std::complex<double> *beta, std::complex<double> *vl,
			int *ldvl, std::complex<double> *vr, int *ldvr,
			std::complex<double> *work, int *lwork, double *rwork, int *info);
*/
		int dsygv_(int* itype, char* jobz, char* uplo, int* n, double* a,
			int* lda, double* b, int* ldb, double* w, double* work, int* lwork, int* info);
	}
};

namespace itppext2
{
	bool eig(const itpp::mat &A, const itpp::mat &B, itpp::vec &d, itpp::mat &V)
	{
		it_assert_debug(A.rows() == A.cols(), "eig: Matrix A is not square");
		it_assert_debug(B.rows() == B.cols(), "eig: Matrix B is not square");
		it_assert_debug(A.rows() == B.cols(), "eig: Matrix A and B don't have the same size");

		int worksize=4*A.rows(); // This may be choosen better!
		itpp::mat l_A(A);
		itpp::mat l_B(B);
		itpp::vec l_W(A.rows());
		itpp::vec l_Work(worksize);
		l_W.zeros();
		l_Work.zeros();

		int itype=1; // 1: Ax=lBx 2: ABx=lx 3: BAx=lx
		char jobz = 'V', uplo = 'U';
		int n=l_A.rows();
		double* a=l_A._data();
		int lda=n;
		double* b=l_B._data();
		int ldb=n;
		double* w=l_W._data();
		int lwork=worksize;
		double* work=l_Work._data();
		int info=0;

		dsygv_(&itype, &jobz, &uplo, &n, a, &lda, b, &ldb, w, work, &lwork, &info);

		d=l_W;
		V=l_A;

		return (info == 0);
	}

	itpp::mat convert(const IMatrix& rMatrix)
	{
		itpp::mat l_oResult(
			rMatrix.getDimensionSize(1),
			rMatrix.getDimensionSize(0));
		std::memcpy(l_oResult._data(), rMatrix.getBuffer(), rMatrix.getBufferElementCount()*sizeof(double));
		return l_oResult.transpose();
	}
}

bool CBoxAlgorithmXDAWN2SpatialFilterTrainer::initialize(void)
{
	// CString   l_sSettingValue=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	// uint64 l_ui64SettingValue=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	// float64 l_f64SettingValue=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	// ...

	//input
	m_pStimulationDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationDecoder));
	m_pStimulationDecoder->initialize();

	m_pSignalDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalDecoder));
	m_pSignalDecoder->initialize();
	//end of input 

	//Output
	//output stimulations
	m_oStimulationEncoder.initialize(*this,1);
	//start output spatial filters
	m_pMatrixStreamEncoder=&getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixEncoder));
	m_pMatrixStreamEncoder->initialize();
	IMatrix *l_oEncodeDecisionMatrix=&op_pMatrixToEncode;
	m_pMatrixStreamEncoder->getInputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_InputParameterId_Matrix)->setValue(&l_oEncodeDecisionMatrix);
	//end of output spatial filters 
	//end of Output
  
  op_ui64SamplingRate.initialize(m_pSignalDecoder->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Sampling));

	m_ui64StimulationIdentifier=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_sSpatialFilterConfigurationFilename=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_ui64FilterDimension=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	m_f64EpochDuration=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);

	m_bHasSentHeader=false;

	return true;
}

bool CBoxAlgorithmXDAWN2SpatialFilterTrainer::uninitialize(void)
{
	m_pSignalDecoder->uninitialize();
	m_pStimulationDecoder->uninitialize();

	this->getAlgorithmManager().releaseAlgorithm(*m_pSignalDecoder);
	this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoder);

	//for the stimulations
	m_oStimulationEncoder.uninitialize();
	//for the spatial filters
	m_pMatrixStreamEncoder->uninitialize();
	getAlgorithmManager().releaseAlgorithm(*m_pMatrixStreamEncoder);

	op_ui64SamplingRate.uninitialize();

	m_pSignalDecoder=NULL;
	m_pStimulationDecoder=NULL;
	m_pMatrixStreamEncoder = NULL;

	return true;
}

bool CBoxAlgorithmXDAWN2SpatialFilterTrainer::processInput(const size_t ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

namespace
{
	typedef struct
	{
		uint64_t m_ui64StartTime;
		uint64_t m_ui64EndTime;
		CMatrix* m_pMatrix;
	} SChunk;
  typedef struct
	{
		uint64_t m_ui64StimStartTime;
	} SStim;
  std::vector < SStim > l_vTargetStimulations;
  std::vector < SStim > l_vNonTargetStimulations;
};

bool CBoxAlgorithmXDAWN2SpatialFilterTrainer::process(void)
{
	IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

	bool l_bShouldTrain=false;
	uint64_t l_ui64TrainDate, l_ui64TrainChunkStartTime, l_ui64TrainChunkEndTime;
	uint32_t i, j, k;
	std::vector < SChunk >::const_iterator it;
    std::vector < SStim >::const_iterator itstim;

	TParameterHandler < IMemoryBuffer* > l_oOutputMemoryBufferHandle(m_pMatrixStreamEncoder->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_OutputParameterId_EncodedMemoryBuffer));
  
  for(i=0; i<l_rDynamicBoxContext.getInputChunkCount(0); i++)
	{
		TParameterHandler < const IMemoryBuffer* > ip_pMemoryBuffer(m_pStimulationDecoder->getInputParameter(OVP_GD_Algorithm_StimulationDecoder_InputParameterId_MemoryBufferToDecode));
		ip_pMemoryBuffer=l_rDynamicBoxContext.getInputChunk(0, i);
		m_pStimulationDecoder->process();
		
		//header
		if(m_pStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationDecoder_OutputTriggerId_ReceivedHeader))
		{
			m_oStimulationEncoder.encodeHeader();
			l_rDynamicBoxContext.markOutputAsReadyToSend(0,l_rDynamicBoxContext.getInputChunkStartTime(0, i),l_rDynamicBoxContext.getInputChunkEndTime(0, i));
		}

		//buffer
		if(m_pStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationDecoder_OutputTriggerId_ReceivedBuffer))
		{
			TParameterHandler < IStimulationSet* > op_pStimulationSet(m_pStimulationDecoder->getOutputParameter(OVP_GD_Algorithm_StimulationDecoder_OutputParameterId_StimulationSet));
			// Loop on stimulations
      for(j=0; j<op_pStimulationSet->getStimulationCount(); j++)
			{
        // Check for Train Stimulation
				l_bShouldTrain |= (op_pStimulationSet->getStimulationIdentifier(j)==m_ui64StimulationIdentifier);
       
        // Target Stimulation
        if(op_pStimulationSet->getStimulationIdentifier(j)==OVTK_StimulationId_Target)
        {
          SStim target;
          target.m_ui64StimStartTime = op_pStimulationSet->getStimulationDate(j);
          l_vTargetStimulations.push_back(target);
          l_vNonTargetStimulations.push_back(target);
        }

        // Non Target Stimulation
        if(op_pStimulationSet->getStimulationIdentifier(j)==OVTK_StimulationId_NonTarget)
        {
            // Process Flash Stim
          SStim nonTarget;
          nonTarget.m_ui64StimStartTime = op_pStimulationSet->getStimulationDate(j);
          l_vNonTargetStimulations.push_back(nonTarget);
            //this->getLogManager() << LogLevel_Info << "Received Non Target stimulation\n";
        }          
			}
			if(l_bShouldTrain)
			{
				l_ui64TrainDate = op_pStimulationSet->getStimulationDate(j);
				l_ui64TrainChunkStartTime = l_rDynamicBoxContext.getInputChunkStartTime(0, i);
				l_ui64TrainChunkEndTime = l_rDynamicBoxContext.getInputChunkEndTime(0, i);
			}
		}

		//End
		if(m_pStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationDecoder_OutputTriggerId_ReceivedEnd))
		{
			m_oStimulationEncoder.encodeEnd();
			l_rDynamicBoxContext.markOutputAsReadyToSend(0,l_rDynamicBoxContext.getInputChunkStartTime(0, i),l_rDynamicBoxContext.getInputChunkEndTime(0, i));
		}
		l_rDynamicBoxContext.markInputAsDeprecated(0, i);
	}
  // Start Training
	if(l_bShouldTrain)
	{
		this->getLogManager() << LogLevel_Info << "Received train stimulation - be patient\n";

		this->getLogManager() << LogLevel_Trace << "Decoding signal...\n";

		bool l_bIsContinuous=true;
		uint64_t l_ui64StartTime = uint64_t(-1);
		uint64_t l_ui64EndTime = uint64_t(-1);
		std::vector < SChunk > l_vSignalChunk;
		for(i=0; i<l_rDynamicBoxContext.getInputChunkCount(1); i++)
		{
			TParameterHandler<const IMemoryBuffer*> ip_pMemoryBuffer(m_pSignalDecoder->getInputParameter(OVP_GD_Algorithm_SignalDecoder_InputParameterId_MemoryBufferToDecode));
			ip_pMemoryBuffer=l_rDynamicBoxContext.getInputChunk(1, i);
			m_pSignalDecoder->process();
			if(m_pSignalDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalDecoder_OutputTriggerId_ReceivedHeader))
			{
				for(it=l_vSignalChunk.begin(); it!=l_vSignalChunk.end(); it++) delete it->m_pMatrix;
				l_vSignalChunk.clear();

				l_bIsContinuous=true;
				l_ui64StartTime=l_rDynamicBoxContext.getInputChunkStartTime(1, i);
				l_ui64EndTime=l_rDynamicBoxContext.getInputChunkEndTime(1, i);
			}
			if(m_pSignalDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalDecoder_OutputTriggerId_ReceivedBuffer))
			{
				TParameterHandler<IMatrix*> ip_pMatrix(m_pSignalDecoder->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Matrix));
				SChunk l_oChunk;
				l_oChunk.m_ui64StartTime=l_rDynamicBoxContext.getInputChunkStartTime(1, i);
				l_oChunk.m_ui64EndTime=l_rDynamicBoxContext.getInputChunkEndTime(1, i);
				l_oChunk.m_pMatrix=new CMatrix;
				OpenViBE::Toolkit::Matrix::copy(
					*l_oChunk.m_pMatrix,
					*ip_pMatrix);
				l_vSignalChunk.push_back(l_oChunk);

				if(l_oChunk.m_ui64StartTime!=l_ui64EndTime)
				{
					l_bIsContinuous=false;
				}
				l_ui64StartTime=l_oChunk.m_ui64StartTime;
				l_ui64EndTime=l_oChunk.m_ui64EndTime;
			}
			if(m_pSignalDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalDecoder_OutputTriggerId_ReceivedEnd))
			{
			}
			l_rDynamicBoxContext.markInputAsDeprecated(1, i);
		}

		if(!l_bIsContinuous)
		{
			this->getLogManager() << LogLevel_Error << "Input signal is not continuous... Can't continue\n";
			return true;
		}

    
		if(l_vTargetStimulations.size()==0)
		{
			this->getLogManager() << LogLevel_Error << "No evoked potential received... Can't continue\n";
			return true;
		}   
   
		// WARNING - OpenViBE matrices are transposed ITPP matrices !

		this->getLogManager() << LogLevel_Trace << "Converting OpenViBE matrices to IT++ matrices...\n";

		uint32_t l_ui32ChunkCount = l_vSignalChunk.size();
		uint32_t l_ui32ChannelCount = l_vSignalChunk.begin()->m_pMatrix->getDimensionSize(0);
		uint32_t l_ui32SampleCountPerChunk = l_vSignalChunk.begin()->m_pMatrix->getDimensionSize(1);
		uint32_t l_ui32SampleCountPerERP = (uint64_t)(op_ui64SamplingRate*m_f64EpochDuration);
		uint64_t l_ui64SignalStartTime = l_vSignalChunk.begin()->m_ui64StartTime;
		uint64_t l_ui64SignalEndTime = l_vSignalChunk.rbegin()->m_ui64EndTime;
    
		itpp::mat l_oSignalMatrix(l_ui32ChannelCount, l_ui32ChunkCount*l_ui32SampleCountPerChunk);
		for(i=0, it=l_vSignalChunk.begin(); it!=l_vSignalChunk.end(); it++, i++)
		{
			itpp::mat l_oMatrix=itppext2::convert(*it->m_pMatrix);
			l_oSignalMatrix.set_submatrix(0, i*l_ui32SampleCountPerChunk, l_oMatrix);
		}

    // Building D1 matrix with target
		itpp::mat l_oD1Matrix(l_ui32ChunkCount*l_ui32SampleCountPerChunk, l_ui32SampleCountPerERP);
		l_oD1Matrix.clear();
		for(itstim=l_vTargetStimulations.begin(); itstim!=l_vTargetStimulations.end(); itstim++)
		{
			uint64_t l_ui64ERPStartTime = itstim->m_ui64StimStartTime;
			uint32_t l_ui32ERPStartIndex = ((l_ui64ERPStartTime - l_ui64SignalStartTime)*(l_oSignalMatrix.cols())) / (l_ui64SignalEndTime - l_ui64SignalStartTime);

			for(k=0; k<l_ui32SampleCountPerERP && l_ui32ERPStartIndex+k < l_oD1Matrix.rows(); k++)
			{
				l_oD1Matrix(l_ui32ERPStartIndex+k,k)=1;
			}
		}

    // Building D2 matrix with target and non target
		itpp::mat l_oD2Matrix(l_ui32ChunkCount*l_ui32SampleCountPerChunk, l_ui32SampleCountPerERP);
		l_oD2Matrix.clear();
		for(itstim=l_vNonTargetStimulations.begin(); itstim!=l_vNonTargetStimulations.end(); itstim++)
		{
			uint64_t l_ui64ERPStartTime = itstim->m_ui64StimStartTime;
			uint32_t l_ui32ERPStartIndex=((l_ui64ERPStartTime-l_ui64SignalStartTime)*(l_oSignalMatrix.cols() ))/(l_ui64SignalEndTime-l_ui64SignalStartTime);
			//for(k=0; k<l_ui32SampleCountPerERP; k++)
			for(k=0; k<l_ui32SampleCountPerERP && l_ui32ERPStartIndex+k < l_oD2Matrix.rows(); k++)
			{
				l_oD2Matrix(l_ui32ERPStartIndex+k,k)=1;
			}
		}
    
   // l_oD2Matrix = l_oD2Matrix + l_oD1Matrix;

    // Build D matrix by concatenation of D1 and D2
    itpp::mat l_oDMatrix = itpp::concat_horizontal(l_oD1Matrix,l_oD2Matrix);

#if 0
		std::stringstream s2;
		std::stringstream s3;

		s2 << "Signal Matrix :\n" << l_oSignalMatrix << "\n";
		s3 << "D Matrix :\n" << l_oDMatrix.transpose() << "\n";


#endif
    // P = inv(D'*D)*D'*X
    // P = (P1 P2)
    itpp::mat P = (itpp::inv(l_oDMatrix.transpose()*l_oDMatrix)*l_oDMatrix.transpose()) * l_oSignalMatrix.transpose();

    itpp::mat P1 = P.get_rows(0,l_ui32SampleCountPerERP-1);

    // A = cov(P1'*D1)
    itpp::mat A = (P1.transpose()*(l_oD1Matrix.transpose()*l_oD1Matrix)*P1);
#if 1
		std::stringstream s4;
		s4 << "A :\n" << A << "\n";
		this->getLogManager() << LogLevel_Debug << s4.str().c_str() << "\n";
#endif
    // B = cov(X)
		itpp::mat B=(l_oSignalMatrix*l_oSignalMatrix.transpose());
#if 1
		std::stringstream s5;
		s5 << "B :\n" << B << "\n";
		this->getLogManager() << LogLevel_Debug << s5.str().c_str() << "\n";
#endif

		this->getLogManager() << LogLevel_Trace << "Computing generalized eigen vector decomposition...\n";

		itpp::mat l_oEigenVector;
		itpp::vec l_oEigenValue;
        // V = eig(A,B)

		if(itppext2::eig(A, B, l_oEigenValue, l_oEigenVector)) //if decompoisition successful then output result to: file,console,streamed matrix channel 
		{
			std::map < double, itpp::vec > l_vEigenVector;
			
            for(i=0; i<l_ui32ChannelCount; i++)
			{
				itpp::vec l_oVector=l_oEigenVector.get_col(i);
				l_vEigenVector[l_oEigenValue[i]]=l_oVector/itpp::norm(l_oVector);
			}

#if 0
			std::stringstream s6;
			std::stringstream s7;

			s6 <<  "l_oEigenValues :\n" << l_oEigenValues << "\n";
			s7 <<  "l_oEigenVectors :\n" << l_oEigenVectors << "\n";

			this->getLogManager() << LogLevel_Fatal << s6.str().c_str() << "\n";
			this->getLogManager() << LogLevel_Fatal << s7.str().c_str() << "\n";
#endif

			std::map < double, itpp::vec >::const_reverse_iterator it;

			//start writing file
			FILE* l_pFile=::fopen(m_sSpatialFilterConfigurationFilename.toASCIIString(), "wb");
			if(!l_pFile)
			{
				this->getLogManager() << LogLevel_Error << "The file [" << m_sSpatialFilterConfigurationFilename << "] could not be opened for writing...";
				return false;
			}

			::fprintf(l_pFile, "<OpenViBE-SettingsOverride>\n");
			::fprintf(l_pFile, "\t<SettingValue>");
			for(it=l_vEigenVector.rbegin(), i=0; it!=l_vEigenVector.rend() && i<m_ui64FilterDimension; it++, i++)
			{
				for(j=0; j<l_ui32ChannelCount; j++)
				{
					::fprintf(l_pFile, "%e ", it->second[j]);
				}
			}
			::fprintf(l_pFile, "</SettingValue>\n");
			::fprintf(l_pFile, "\t<SettingValue>%d</SettingValue>\n", uint32_t(m_ui64FilterDimension));
			::fprintf(l_pFile, "\t<SettingValue>%d</SettingValue>\n", l_ui32ChannelCount);
			::fprintf(l_pFile, "</OpenViBE-SettingsOverride>\n");
			::fclose(l_pFile);
			//end of writing to file

			//start output to console
			this->getLogManager() << LogLevel_Info << "Training finished... Eigen values are ";
			for(it=l_vEigenVector.rbegin(), i=0; it!=l_vEigenVector.rend() && i<m_ui64FilterDimension; it++, i++)
			{
				this->getLogManager() << " | " << double(it->first);
			}
			this->getLogManager() << "\n";
			//end output to console

			//start prepare filters
			//this->getLogManager() << LogLevel_Warning << "Ready to output spatial filters.\n";

			op_pMatrixToEncode.setDimensionCount(2);
			op_pMatrixToEncode.setDimensionSize(0, m_ui64FilterDimension);
			op_pMatrixToEncode.setDimensionSize(1, l_ui32ChannelCount);

			//this->getLogManager() << LogLevel_Warning << "Done configure matrix.\n";
			//every row is a different filter
			//every column is a coefficient (for each channel) for the filter

			//if (op_pMatrixToEncode.getBuffer()!=NULL) this->getLogManager() << LogLevel_Warning << "Matrix exists.\n";

			for(it=l_vEigenVector.rbegin(), i=0; it!=l_vEigenVector.rend() && i<m_ui64FilterDimension; it++, i++)
			{
				for(j=0; j<l_ui32ChannelCount; j++)
				{
					//this->getLogManager() << LogLevel_Warning << (OpenViBE::float64)it->second[j] << "\n";
					op_pMatrixToEncode.getBuffer()[i * l_ui32ChannelCount + j ] = (double)it->second[j];
				}				
			}

			m_pMatrixStreamEncoder->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeBuffer);
			//this->getLogManager() << LogLevel_Warning << "Spatial filters encoded.\n";
			//end of prepare filters
		}
		else
		{
			this->getLogManager() << LogLevel_ImportantWarning << "Generalized eigen vector decomposition failed...\n";
			return true;
		}

		this->getLogManager() << LogLevel_Info << "xDAWN Spatial filter trained successfully.\n";
    
        //start output stimulations on first channel
		m_oStimulationEncoder.getInputStimulationSet()->appendStimulation(OVTK_StimulationId_TrainCompleted, l_ui64TrainDate, 0);
		m_oStimulationEncoder.encodeBuffer();
		l_rDynamicBoxContext.markOutputAsReadyToSend(0,l_ui64TrainChunkStartTime, l_ui64TrainChunkEndTime);
		//end output stimulations channel

		//start output filters on second channel
        if(!m_bHasSentHeader)
		{
			//this->getLogManager() << LogLevel_Warning << "Sending header for filters.\n";
			l_oOutputMemoryBufferHandle=l_rDynamicBoxContext.getOutputChunk(1);
			m_pMatrixStreamEncoder->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeHeader);
			l_rDynamicBoxContext.markOutputAsReadyToSend(1,l_ui64TrainChunkStartTime, l_ui64TrainChunkEndTime);
			m_bHasSentHeader=true;
			//this->getLogManager() << LogLevel_Warning << "Done Sending header for filters.\n";
		}

		//this->getLogManager() << LogLevel_Warning << "Sending buffer matrix.\n";
		l_oOutputMemoryBufferHandle=l_rDynamicBoxContext.getOutputChunk(1); //1 is filters
		m_pMatrixStreamEncoder->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeBuffer);
		l_rDynamicBoxContext.markOutputAsReadyToSend(1,l_ui64TrainChunkStartTime, l_ui64TrainChunkEndTime);
		this->getLogManager() << LogLevel_Warning << "Matrix with filters sent successfully.\n";
		//end of output filters
	}

	return true;
}

//#endif
