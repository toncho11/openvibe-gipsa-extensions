/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu, Goyat Matthieu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#ifndef __OpenViBEPlugins_BoxAlgorithm_PulsarTagging_H__
#define __OpenViBEPlugins_BoxAlgorithm_PulsarTagging_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>
#include <deque>
#include <vector>

#include <fstream>
#include <string>

#include <sstream>
#include <exception>

#define OVP_ClassId_BoxAlgorithm_PulsarTagging     OpenViBE::CIdentifier(0x3E4B6CFB, 0x7F141E37)
#define OVP_ClassId_BoxAlgorithm_PulsarTaggingDesc OpenViBE::CIdentifier(0x70B31901, 0x7286199B)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmPulsarTagging : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(const size_t ui32InputIndex);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_PulsarTagging);

			struct ConversionEntry //maps incoming number values to output stimulation and selects output channel
			{
				uint64_t Number;
				uint64_t StimulationCode;
				uint32_t OutputChannel;

				ConversionEntry(uint64_t pNumber, uint64_t pStimulationCode, uint32_t pOutputChannel) : Number(pNumber), StimulationCode(pStimulationCode), OutputChannel(pOutputChannel)
				{

				}
			};

			std::vector < ConversionEntry > m_vConversionEntries;

		protected:

			//trigger input 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalDecoderTrigger1;
			OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pDecodedMatrixTrigger1;
			OpenViBE::Kernel::TParameterHandler < uint64_t > op_ui64SamplingRateTrigger;

			//trigger input 2
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalDecoderTrigger2;
			OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger2;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pDecodedMatrixTrigger2;

			//used for next processing
			OpenViBE::Kernel::IAlgorithmProxy* m_pNextSignalDecoderTrigger;
	        OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > m_ip_pNextMemoryBufferToDecodeTrigger;
	        OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > m_op_pNextDecodedMatrixTrigger;

			//Stimulation 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer1;

			//OpenViBE::uint64 m_ui64StimulationToTrigUp;
			//OpenViBE::uint64 m_ui64StimulationToTrigDown;
			OpenViBE::CString m_sConversionListFileLocation;
			uint64_t m_ui32Policy;

			//OpenViBE::float64 m_f64CurrentValue1;
			double m_f64Last1;
			double m_f64BeforeLast1;

	        //OpenViBE::float64 m_f64CurrentValue2;
			double m_f64Last2;
			double m_f64BeforeLast2;


			//Example file:
			//64,0x00008101,1
            //128,0x00008102,2
            //192,0x00008103,1

			uint64_t m_ui32CurrentTrialStimulation;
			int32_t m_i32TriggerChannelCount;

		};

		class CBoxAlgorithmPulsarTaggingDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Pulsar tagging converter"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Converts tagging channels to stimulations accroding to Pulsar"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			//virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Signal processing/Basic"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-jump-to"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_PulsarTagging; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessing::CBoxAlgorithmPulsarTagging; }

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Trigger channel",		OV_TypeId_Signal		);
				rBoxAlgorithmPrototype.addInput  ("Trigger channel",		OV_TypeId_Signal		);
				rBoxAlgorithmPrototype.addOutput ("Output stimulations 1",	OV_TypeId_Stimulations	);
				rBoxAlgorithmPrototype.addSetting("Number of trigger channels (2 BIOPAC, 1 GTEC):",OV_TypeId_Integer, "2");
				
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_PulsarTaggingDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_PulsarTagging_H__
