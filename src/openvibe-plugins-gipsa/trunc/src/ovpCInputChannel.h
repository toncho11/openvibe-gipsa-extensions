#ifndef __OpenViBEPlugins_Streaming_InputChannel_H__
#define __OpenViBEPlugins_Streaming_InputChannel_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

/**
	Use this class to receive send and stimulations channels
*/

namespace OpenViBEPlugins
{
	namespace Streaming
	{
		class CInputChannel
		{
		private:
			typedef enum
			{	SIGNAL_CHANNEL,
				STIMULATION_CHANNEL,
				NB_CHANNELS,
			} channel_t;
						
		public:

			CInputChannel(const uint16_t ui16InputIndex = 0);
			~CInputChannel();
			bool initialize(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm);
			bool uninitialize();

			bool			isLastChannel(const uint32_t ui32InputIndex) { return ui32InputIndex == m_ui32StimulationChannel; }
			bool			isWorking() { return m_bIsWorking; }
			bool           waitForSignalHeader();
			uint32_t            getNbOfStimulationBuffers();
			uint32_t            getNbOfSignalBuffers();
			OpenViBE::IStimulationSet*  getStimulation(uint64_t& startTimestamp, uint64_t& endTimestamp, const uint32_t stimulationIndex);
			OpenViBE::IStimulationSet*  discardStimulation(const uint32_t stimulationIndex);
			double*          getSignal(uint64_t& startTimestamp, uint64_t& endTimestamp, const uint32_t signalIndex);
			double*          discardSignal(const uint32_t signalIndex);
			uint64_t            getSamplingRate() const {return op_ui64SamplingRateSignal;}
			uint64_t            getNbOfChannels() const {return op_pMatrixSignal->getDimensionSize(0);}
			uint64_t            getNbOfSamples() const {return op_pMatrixSignal->getDimensionSize(1);}
			uint64_t            getStartTimestamp() const {return m_ui64StartTimestamp;}
			uint64_t            getEndTimestamp() const {return m_ui64EndTimestamp;}
			const char*                 getChannelName(uint32_t index) { return op_pMatrixSignal->getDimensionLabel(0,index); }
			const OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* >& getOpMatrix() const {return op_pMatrixSignal;}
		
		protected:
			uint32_t														m_ui32SignalChannel;
			uint32_t														m_ui32StimulationChannel;
			bool                                                       m_bIsWorking;

			uint64_t                                                        m_ui64StartTimestamp;
			uint64_t                                                        m_ui64EndTimestamp;
		
			OpenViBE::IStimulationSet*                                              m_oIStimulationSet;

			// parent memory
			OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>*     m_pTBoxAlgorithm;
			
			// signal section
			OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamDecoderSignal;
			
			OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* >	ip_pMemoryBufferSignal;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* >				op_pMatrixSignal;
			OpenViBE::Kernel::TParameterHandler < uint64_t >				op_ui64SamplingRateSignal;
			
			
			// stimulation section
			OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamDecoderStimulation;

			OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* >	ip_pMemoryBufferStimulation;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* >		op_pStimulationSetStimulation;
		};
	};
};

#endif // __OpenViBEPlugins_InputChannel_H__
