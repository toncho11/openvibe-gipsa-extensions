/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu,

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#if defined TARGET_HAS_COMMUNICATION_LIB

#ifndef __OpenViBEPlugins_BoxAlgorithm_P300Tagger_H__
#define __OpenViBEPlugins_BoxAlgorithm_P300Tagger_H__

#include "ovp_defines.h"//may need to adjust
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gtk/gtk.h>
#include <map>
#include <list>

#include "Communication/OVComm.h"

#define OVP_ClassId_BoxAlgorithm_P300Tagger     OpenViBE::CIdentifier(0x4E3B4689, 0x3C115913)
#define OVP_ClassId_BoxAlgorithm_P300TaggerDesc OpenViBE::CIdentifier(0x7E6C79B8, 0x647C7228)

namespace OpenViBEPlugins
{
	namespace SimpleVisualisation
	{
		/*
		  Class that performs software or hardware tagging while P300 stimulations  
		*/
		class CBoxAlgorithmP300Tagger : public OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(OpenViBE::uint32 ui32Index);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_P300Tagger);

		private:

			typedef struct
			{
				::GtkWidget* pWidget;
				::GtkWidget* pChildWidget;
				::GdkColor oBackgroundColor;
				::GdkColor oForegroundColor;
				::PangoFontDescription* pFontDescription;
			} SWidgetStyle;

			typedef void (CBoxAlgorithmP300Tagger::*_cache_callback_)(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);

			void _cache_build_from_table_(::GtkTable* pTable);
			void _cache_for_each_(_cache_callback_ fpCallback, void* pUserData);
			void _cache_for_each_if_(int iLine, int iColumn, _cache_callback_ fpIfCallback, _cache_callback_ fpElseCallback, void* pIfUserData, void* pElseUserData);
			void _cache_change_null_cb_(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_background_cb_(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_foreground_cb_(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_font_cb_(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_collect_widget_cb_(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_collect_child_widget_cb_(CBoxAlgorithmP300Tagger::SWidgetStyle& rWidgetStyle, void* pUserData);

		protected:

			OpenViBE::CString m_sInterfaceFilename;
			OpenViBE::uint64 m_ui64RowStimulationBase;
			OpenViBE::uint64 m_ui64ColumnStimulationBase;

			::GdkColor m_oFlashBackgroundColor;
			::GdkColor m_oFlashForegroundColor;
			OpenViBE::uint64 m_ui64FlashFontSize;
			::PangoFontDescription* m_pFlashFontDescription;
			::GdkColor m_oNoFlashBackgroundColor;
			::GdkColor m_oNoFlashForegroundColor;
			OpenViBE::uint64 m_ui64NoFlashFontSize;
			::PangoFontDescription* m_pNoFlashFontDescription;
			::GdkColor m_oTargetBackgroundColor;
			::GdkColor m_oTargetForegroundColor;
			OpenViBE::uint64 m_ui64TargetFontSize;
			::PangoFontDescription* m_pTargetFontDescription;
			::GdkColor m_oSelectedBackgroundColor;
			::GdkColor m_oSelectedForegroundColor;
			OpenViBE::uint64 m_ui64SelectedFontSize;
			::PangoFontDescription* m_pSelectedFontDescription;

		private:

			OpenViBE::Kernel::IAlgorithmProxy* m_pSequenceStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pTargetStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pTargetFlaggingStimulationEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pRowSelectionStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pColumnSelectionStimulationDecoder;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pSequenceMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pTargetMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IStimulationSet*> ip_pTargetFlaggingStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pSequenceStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pTargetStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pTargetFlaggingMemoryBuffer;
			OpenViBE::uint64 m_ui64LastTime;

			::GtkBuilder* m_pMainWidgetInterface;
			::GtkBuilder* m_pToolbarWidgetInterface;
			::GtkWidget* m_pMainWindow;
			::GtkWidget* m_pToolbarWidget;
			::GtkTable* m_pTable;
			::GtkLabel* m_pResult;
			::GtkLabel* m_pTarget;
			OpenViBE::uint64 m_ui64RowCount;
			OpenViBE::uint64 m_ui64ColumnCount;

			int m_iLastTargetRow;
			int m_iLastTargetColumn;
			int m_iTargetRow;
			int m_iTargetColumn;
			int m_iSelectedRow;
			int m_iSelectedColumn;

			bool m_bTableInitialized;

			std::map < unsigned long, std::map < unsigned long, CBoxAlgorithmP300Tagger::SWidgetStyle > > m_vCache;
			std::list < std::pair < int, int > > m_vTargetHistory;

			//OVComm* commParallelP;//gipsa
			OVComm* commSoftwareT;//gipsa
			OVComm* commSerialP;//gipsa
		};

		class CBoxAlgorithmP300TaggerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("P300 Tagger"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Yann Renard and Anton Andreev"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("INRIA/Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Performs a P300 stimulations and tags each flash(row/column) using hardware or software tagging."); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("P300 Tagger is based on P300 Speller Visualisation developed by INRIA. Tagging can be software through IPC between this box and the acquistion server or hardware using the parallel port. Tagging is performed accroding to a protocol."); }
			//virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Visualisation/Presentation"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.2"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-select-font"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_P300Tagger; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SimpleVisualisation::CBoxAlgorithmP300Tagger; }

			virtual bool hasFunctionality(OpenViBE::Kernel::EPluginFunctionality ePF) const { return ePF == OpenViBE::Kernel::PluginFunctionality_Visualization; }
			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput ("Sequence stimulations",            OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput ("Target stimulations",              OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput ("Row selection stimulations",       OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput ("Column selection stimulations",    OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addOutput("Target / Non target flagging",     OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Interface filename",              OV_TypeId_Filename,    "../share/openvibe-plugins/simple-visualisation/p300-speller.ui");
				rBoxAlgorithmPrototype.addSetting("Row stimulation base",            OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Column stimulation base",         OV_TypeId_Stimulation, "OVTK_StimulationId_Label_07");

				rBoxAlgorithmPrototype.addSetting("Flash background color",          OV_TypeId_Color,       "10,10,10");
				rBoxAlgorithmPrototype.addSetting("Flash foreground color",          OV_TypeId_Color,       "100,100,100");
				rBoxAlgorithmPrototype.addSetting("Flash font size",                 OV_TypeId_Integer,     "100");

				rBoxAlgorithmPrototype.addSetting("No flash background color",       OV_TypeId_Color,       "0,0,0");
				rBoxAlgorithmPrototype.addSetting("No flash foreground color",       OV_TypeId_Color,       "50,50,50");
				rBoxAlgorithmPrototype.addSetting("No flash font size",              OV_TypeId_Integer,     "75");

				rBoxAlgorithmPrototype.addSetting("Target background color",         OV_TypeId_Color,       "10,40,10");
				rBoxAlgorithmPrototype.addSetting("Target foreground color",         OV_TypeId_Color,       "60,100,60");
				rBoxAlgorithmPrototype.addSetting("Target font size",                OV_TypeId_Integer,     "100");

				rBoxAlgorithmPrototype.addSetting("Selected background color",       OV_TypeId_Color,       "70,20,20");
				rBoxAlgorithmPrototype.addSetting("Selected foreground color",       OV_TypeId_Color,       "30,10,10");
				rBoxAlgorithmPrototype.addSetting("Selected font size",              OV_TypeId_Integer,     "100");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300TaggerDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_P300Tagger_H__

#endif //is code to be compiled
