#include <openvibe/ov_all.h>

//GIPSA plugins headers
#include "ovpCBoxAlgorithmPP2Stim.h"
#include "ovpCBoxAlgorithmSignal2Stim.h"
#include "ovpCBoxStimulationTransformer.h"
#include "ovpCBoxAlgorithmStreamedMatrixMerger.h"
#include "ovpCBoxUpsample.h"
#include "ovpCBoxAlgorithmTrainMDM.h"
#include "ovpCBoxAlgorithmXDAWN2SpatialFilterTrainer.h"
#include "ovpCBoxRiemannPotato.h"
#include "ovpCBoxAlgorithmProcessMDM.h"
#include "ovpCBoxAlgorithmStreamMergerWithResampling.h"
#include "ovpCBoxAlgorithmPulsarTagging.h"
#include "ovpCBoxAlgorithmStimulationSwitcher.h"
//next GIPSA include to your new box

//Disabled:
//#include "ovpCBoxAlgorithmBrainampFileWriter.h"
//#include "ovpCBoxLSLExport.h"
//#include "ovpCBoxTagger.h"
//#include "ovpCBoxAlgorithmP300Tagger.h"
//#include "ovpCAlgorithmClassifierBayesPointMachine.h"

OVP_Declare_Begin();

	//Register drop downs BrainampFileWriter
	context.getTypeManager().registerEnumerationType (OVP_TypeId_BinaryFormat, "Binary format select");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_BinaryFormat, "INT_16", OVP_TypeId_BinaryFormat_int16.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_BinaryFormat, "UINT_16", OVP_TypeId_BinaryFormat_uint16.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_BinaryFormat, "IEEE_FLOAT_32", OVP_TypeId_BinaryFormat_float32.toUInteger());
	
	//Register boxes
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmPP2StimDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmSignal2StimDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxStimulationTransformerDesc);
	OVP_Declare_New(OpenViBEPlugins::Streaming::CBoxAlgorithmStreamedMatrixMergerDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessingGpl::CBoxAlgorithmXDAWN2SpatialFilterTrainerDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessingGpl::CBoxAlgorithmRiemannPotatoDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmUpsampleDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmTrainMDMDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmProcessMDMDesc);
	OVP_Declare_New(OpenViBEPlugins::Streaming::CBoxAlgorithmStreamMergerWithResamplingDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmPulsarTaggingDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmStimulationSwitcherDesc);
	//next GIPSA OVP_Declare_New
	
	//Disabled:
	//OVP_Declare_New(OpenViBEPlugins::FileIO::CBoxAlgorithmBrainampFileWriterDesc);
	//OVP_Declare_New(OpenViBEPlugins::Streaming::CBoxAlgorithmCBoxAlgorithmLSLExportDesc);
	//OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxTaggerDesc);
	//OVP_Declare_New(OpenViBEPlugins::SimpleVisualisation::CBoxAlgorithmP300TaggerDesc)

OVP_Declare_End();
