 /* Project: Gipsa-lab plugins for OpenVibe
  * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu,

  * This file is part of "Gipsa-lab plugins for OpenVibe".
  * You can redistribute it and/or modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
  * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
  * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
  * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
 #ifndef __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriter_H__
 #define __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriter_H__

 #include "ovp_defines.h"
 #include <openvibe/ov_all.h>
 #include <toolkit/ovtk_all.h>

 #include <cstdlib>
 #include <cstdio>
 #include <fstream>

 #include <boost/date_time/posix_time/posix_time.hpp>

 #define OVP_ClassId_BoxAlgorithm_BrainampFileWriter     OpenViBE::CIdentifier(0x5E2F22DF, 0x3FFA4A16)
 #define OVP_ClassId_BoxAlgorithm_BrainampFileWriterDesc OpenViBE::CIdentifier(0x5EAC4297, 0x42814DB4)

 namespace OpenViBEPlugins
 {
	 namespace FileIO
	 {
		 class CBoxAlgorithmBrainampFileWriter : public OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		 {
		 public:

			 CBoxAlgorithmBrainampFileWriter(void);
			 virtual void release(void) { delete this; }

			 virtual bool initialize(void);
			 virtual bool uninitialize(void);
			 virtual bool processInput(const size_t ui32InputIndex);
			 virtual bool process(void);

			 bool writeHeaderFile(void);
			 std::string getShortName(std::string fullpath); //of a file
			 std::string FormatTime(boost::posix_time::ptime now);
	        
	
			 template <class type> bool	saveBuffer(const type myDummy)
			 {
				 std::vector<type> output(m_pMatrix->getBufferElementCount());

				 if(output.size() != m_pMatrix->getBufferElementCount())
					 return false;

				 uint32_t l_uint32ChannelCount     = m_pMatrix->getDimensionSize(0);
				 uint32_t l_uint32SamplesPerChunk  = m_pMatrix->getDimensionSize(1);
				 double* input                  = m_pMatrix->getBuffer();

				 for (uint32_t k=0; k < l_uint32ChannelCount; k++) 
				 {
					 for (uint32_t j=0; j < l_uint32SamplesPerChunk; j++)
					 {
						 uint32_t index = (k * l_uint32SamplesPerChunk) + j;
						 output[j * l_uint32ChannelCount + k] = type(input[index]);
					 }
				 }
					
				 m_oDataFile.write ((char*) &output[0], m_pMatrix->getBufferElementCount() * sizeof(type));

				 return true;
			 }

			 _IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_BrainampFileWriter);
	
		 protected:

			 enum EBinaryFormat
			 {
				 BinaryFormat_Integer16,
				 BinaryFormat_UnsignedInteger16,
				 BinaryFormat_Float32,
			 };

			 //input signal 1
			 OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoder;
			 OpenViBE::IMatrix* m_pMatrix;

			 OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > ip_pMemoryBuffer;
			 OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pMatrix;
			 OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pMinMaxFrequencyBand;
			 OpenViBE::Kernel::TParameterHandler < uint64_t > op_ui64SamplingFrequency;

			
			 //input stimulation 1 
			 OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoderTrigger;
			 OpenViBE::Kernel::TParameterHandler <const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			 OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulationSetTrigger;

			 std::string m_sHeaderFilename;
			 std::string m_sDataFilename;
			 std::string m_sMarkerFilename;

			 std::ofstream m_oHeaderFile;
			 std::ofstream m_oDataFile;
			 std::ofstream m_oMarkerFile;

			 uint32_t m_ui32BinaryFormat;

			 uint32_t m_uint32StimulationCounter;
		 };

		 class CBoxAlgorithmBrainampFileWriterListener : public OpenViBE::Toolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >
		 {
		 public:

			 virtual bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index)
			 {
				 return true;
			 }

			 _IsDerivedFromClass_Final_(OpenViBE::Toolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		 };

		 class CBoxAlgorithmBrainampFileWriterDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		 {
		 public:

			 virtual void release(void) { }

			 virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Brainamp file writer"); }
			 virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev"); }
			 virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("INRIA"); }
			 virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Writes signal in the Brainamp file format. Stimulations are outputed as OpenVibe interger codes."); }
			 virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("You must select the location of the output header file .vhdr. The .eeg and .vmrk files will be created with the same name and in the same folder."); }
			 virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			 virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			 virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-open"); }

			 virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_BrainampFileWriter; }
			 virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::FileIO::CBoxAlgorithmBrainampFileWriter; }
			 virtual OpenViBE::Plugins::IBoxListener* createBoxListener(void) const               { return new CBoxAlgorithmBrainampFileWriterListener; }
			 virtual void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const { delete pBoxListener; }

			 virtual bool getBoxPrototype(
				 OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			 {
				 rBoxAlgorithmPrototype.addInput  ("Streamed matrix",  OV_TypeId_Signal);
				 rBoxAlgorithmPrototype.addInput  ("Input stimulation channel",	   OV_TypeId_Stimulations); 

				 rBoxAlgorithmPrototype.addSetting("Header filename",         OV_TypeId_Filename, "record-[$core{date}-$core{time}].vhdr");
				 rBoxAlgorithmPrototype.addSetting("Binary format",           OVP_TypeId_BinaryFormat, "INT_16");

				 return true;
			 }

			 _IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_BrainampFileWriterDesc);
		 };
	 };
 };

 #endif // __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriter_H__
