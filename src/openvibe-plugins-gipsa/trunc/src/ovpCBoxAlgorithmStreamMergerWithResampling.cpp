/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCBoxAlgorithmStreamMergerWithResampling.h"
#include <iostream>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::Streaming;

CBoxAlgorithmStreamMergerWithResampling::CBoxAlgorithmStreamMergerWithResampling()
	: m_oCInputChannel1(0)
	, m_oCInputChannel2(1)
{
}

bool CBoxAlgorithmStreamMergerWithResampling::initialize(void)
{
	m_oCInputChannel1.initialize(this);
	m_oCInputChannel2.initialize(this);
	
	m_oCOutputChannel.initialize(this);

	return true;
}

bool CBoxAlgorithmStreamMergerWithResampling::uninitialize(void)
{
	m_oCInputChannel1.uninitialize();
	m_oCInputChannel2.uninitialize();
	
	m_oCOutputChannel.uninitialize();

	return true;
}

bool CBoxAlgorithmStreamMergerWithResampling::processInput(const size_t ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	
	return true;
}

bool CBoxAlgorithmStreamMergerWithResampling::process(void)
{
	// wait for header on both channels
	if(!m_oCInputChannel1.isWorking() || !m_oCInputChannel2.isWorking())
	{	
		if(!m_oCInputChannel1.isWorking())                                      // wait for signal header on the first channel
			m_oCInputChannel1.waitForSignalHeader();
		
		if(!m_oCInputChannel2.isWorking())                                      // wait for signal header on the second channel
			m_oCInputChannel2.waitForSignalHeader();
	
		if(m_oCInputChannel1.isWorking() && m_oCInputChannel2.isWorking())
			m_oCOutputChannel.sendHeader(m_oCInputChannel1, m_oCInputChannel2); // send the outpout header
	}
	else
	{
		// the stimulations on the first channel are sent directly to the output without any modification
		for(uint32_t index=0, nb=m_oCInputChannel1.getNbOfStimulationBuffers(); index < nb; index++)
		{	
			uint64_t l_u64StartTimestamp, l_u64EndTimestamp; 
			OpenViBE::IStimulationSet* l_pIStimulationSet = m_oCInputChannel1.getStimulation(l_u64StartTimestamp, l_u64EndTimestamp, index);

			if(!l_pIStimulationSet)
				break;

			m_oCOutputChannel.sendStimulation(l_pIStimulationSet, l_u64StartTimestamp, l_u64EndTimestamp);
		}
		
		// the stimulations on the second channel are discarded
		for(uint32_t index=0, nb=m_oCInputChannel2.getNbOfStimulationBuffers(); index < nb; index++)
		{	
			if(!m_oCInputChannel2.discardStimulation(index))
				break;
		}
			
		// process the data of the first input only if there is not resampling in progress (i.e. the data was processed and sent)
		if(!m_oCOutputChannel.isWorking())
		{	if(m_oCInputChannel1.getNbOfSignalBuffers())
			{	
				uint64_t l_u64StartTimestamp, l_u64EndTimestamp; 
				double* l_pData = m_oCInputChannel1.getSignal(l_u64StartTimestamp, l_u64EndTimestamp, 0);

				if(l_pData)
					m_oCOutputChannel.prepareBlock1(l_pData, l_u64StartTimestamp, l_u64EndTimestamp);
		    }
		}
		else // process the data of the second input only if the resampling is in progress
		{	if(m_oCInputChannel2.getNbOfSignalBuffers())
			{	
				uint64_t l_u64StartTimestamp, l_u64EndTimestamp; 
				double* l_pData = m_oCInputChannel2.getSignal(l_u64StartTimestamp, l_u64EndTimestamp, 0);

				if(l_pData)
					m_oCOutputChannel.prepareBlock2(l_pData);
			}
		}
	}

	return true;
}
