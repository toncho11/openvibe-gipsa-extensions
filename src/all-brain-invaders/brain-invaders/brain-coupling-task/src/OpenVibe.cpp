/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Française contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) “Brain Invaders”: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#if defined TARGET_OS_Windows

#include "OpenVibe.h"

OpenVibe::OpenVibe()
{

}

void OpenVibe::StartScenario()
{
	bool hide=false;

	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);

	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
    si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
    si.hStdOutput =  GetStdHandle(STD_OUTPUT_HANDLE);
    si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
	if (hide) 
	   si.wShowWindow = SW_HIDE;
	else
       si.wShowWindow = SW_MINIMIZE;

    ZeroMemory( &pi, sizeof(pi) );

	//string scenario = "C:\\PROGRA~2\\openvibe\\share\\openvibe-scenarios\\bci\\motor-imagery\\motor-imagery-bci-2-classifier-trainer.xml";
	string scenario = "C:\\PROGRA~2\\openvibe\\share\\openvibe-scenarios\\test-ov-control.xml";

	if (!IfFileExists(scenario)) {cout << "Error. File does not exits: " << scenario << ". Execution failed!" << endl; return;}

	string ov_folder = GetOpenVibeRootFolder();
	if (ov_folder=="" || !IfDirectoryExists(ov_folder)) {cout << "Error. OpenVibe Directory does not exits. Execution failed!" << endl; return;}

	string ovExecutable = GetOpenVibeRootFolder() + string("/openvibe-designer.cmd");
	if (!IfFileExists(ovExecutable)) {cout << "Error. File does not exits: " << ovExecutable << ". Execution failed!" << endl; return;}
	//else cout<< "executable: " << ovExecutable << endl;

	string cmd = "/C " + ovExecutable;
	cmd += (string(" --no-session-management ") + string(" --play ") + scenario);
	if (hide) cmd.append(" --no-gui ");

	string pathToCmd = GetCmdOnWindows();

    // Start the child process. 
	if( !CreateProcess( LPSTR(pathToCmd.c_str()),   //Module name here is cmd
		LPSTR(cmd.c_str()),        // Command line - path to OpenVibe
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        CREATE_NO_WINDOW,              //Creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
        //printf( "CreateProcess failed (%d).\n", GetLastError() );
		cout << "CreateProcess failed: \n" << GetLastError() << endl;
        return;
    }
	 
	cout << " Process ID:" << pi.dwProcessId << endl;

	m_openvibe_handle = pi.hProcess;
	m_openvibe_processid = pi.dwProcessId;

    // Wait until child process exits.
    //WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    //CloseHandle( pi.hProcess );
    
}

void OpenVibe::StopScenario()
{
	KillProcessTree(m_openvibe_processid,2000,"openvibe-designer.exe");

	//Helper::delay(2000);
    //::CloseHandle(m_openvibe_handle);
	//CloseHandle( pi.hThread );
}

bool __fastcall OpenVibe::KillProcessTree(DWORD myprocID, DWORD dwTimeout, string processname)
{
  //Processes must be 32 bit even on 64 bit system including if you use "cmd" (use: C:\\Windows\\syswow64\\cmd.exe)

  bool bRet = true;
  HANDLE hWnd;
  PROCESSENTRY32 pe;

  memset(&pe, 0, sizeof(PROCESSENTRY32));
  pe.dwSize = sizeof(PROCESSENTRY32);

  HANDLE hSnap = :: CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

  if (::Process32First(hSnap, &pe))
  {
	  BOOL bContinue = TRUE;

	int i=0;
    // kill child processes
	while (bContinue)
    {
		  //cout << pe.szExeFile <<endl;

		  if (pe.th32ParentProcessID == myprocID)
		  {
			//ShowMessage ("Gleich - KILL PID: " + AnsiString(pe.th32ProcessID));

			// Rekursion
			KillProcessTree(pe.th32ProcessID, dwTimeout, processname);

			HANDLE hChildProc = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe.th32ProcessID);

			if (hChildProc)
			{
			  //GetWindowThreadProcessId((HWND)hWnd, &myprocID);
			  //// CLOSE Message s
			  //PostMessage((HWND)hWnd, WM_CLOSE, 0, 0) ;

			  //if (WaitForSingleObject(hChildProc, dwTimeout) == WAIT_OBJECT_0)
			  //  bRet = true;
			  //else
			  //{
			  //  bRet = TerminateProcess(hChildProc, 0);
			  //}
			  ::TerminateProcess(hChildProc, 1);
			  i++;

			  ::CloseHandle(hChildProc);
			}
		  }

		  //kill any designer instance left
		  if (boost::iequals(string(pe.szExeFile),string(processname)))
		  {
			  HANDLE hwnd = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe.th32ProcessID);
			  ::TerminateProcess(hwnd, 1);
			  i++;
		  }

          bContinue = ::Process32Next(hSnap, &pe);
    }

	cout << "Killed processes:" << i <<endl;

    // kill the main process
    HANDLE hProc = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, myprocID);

    if (hProc)
    {
		cout << "Main process killed." <<endl;
        ::TerminateProcess(hProc, 1);
        ::CloseHandle(hProc);
    }
	else 
	{
		cout << "Main process not detected!" <<endl;
	}
  }
  return bRet;
}

OpenVibe::~OpenVibe()
{
	KillProcessTree(m_openvibe_processid,2000,"openvibe-designer.exe");
}

bool OpenVibe::IsOS64bitWindows()
{
	//64 bit windows has 2 program files
	return (IfDirectoryExists("C:\\PROGRA~1") && IfDirectoryExists("C:\\PROGRA~2"));
}

string OpenVibe::GetCmdOnWindows()
{
	string result;
	if (IsOS64bitWindows())
		result = "C:\\Windows\\syswow64\\cmd.exe";
	else result = "C:\\WINDOWS\\system32\\cmd.exe";

	return result;
}

bool OpenVibe::IfDirectoryExists(string strPath)
{
	return  boost::filesystem::is_directory(strPath);
}

bool OpenVibe::IfFileExists(string strPath)
{
	return boost::filesystem::is_regular_file(strPath);
}

string OpenVibe::GetOpenVibeRootFolder()
{
	//check if current executable is in openvibe folder         
	boost::filesystem3::path path = boost::filesystem::current_path();

	string testfile = path.generic_string() + string("/../../../openvibe.conf");

	if (IfFileExists(testfile)) 
	{
		boost::filesystem3::path dir(path.generic_string() + string("/../../../.."));
		
		string result = GetShortName(dir.normalize().generic_string());

		//cout << result << endl;
		return result;
	}

	if (IfDirectoryExists(string("C:\\PROGRA~2\\openvibe"))) return string("C:\\PROGRA~2\\openvibe");

	if (IfDirectoryExists(string("C:\\PROGRA~1\\openvibe"))) return string("C:\\PROGRA~1\\openvibe");

	return string("");
}

string OpenVibe::GetShortName(string path)
{
	long     length = path.length();
    TCHAR*   buffer = NULL;
	buffer = new TCHAR[length];

	GetShortPathName(path.c_str(), buffer, length);

	return string(buffer);
}

#endif
