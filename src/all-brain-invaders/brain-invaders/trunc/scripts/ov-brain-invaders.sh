#!/bin/bash
export OV_PATH_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#export OV_PATH_BIN="$OV_PATH_ROOT/bin"
#export OV_PATH_LIB="$OV_PATH_ROOT/lib"
export OV_PATH_DATA="$OV_PATH_ROOT/share/openvibe"

OpenViBE_base=..
export LD_LIBRARY_PATH=$OpenViBE_base/dist/lib:$OpenViBE_dependencies/lib:$LD_L$
export LC_ALL=C
renice 19 $$

cp "$OpenViBE_base/dist/share/openvibe-applications/brain-invaders/brain-invaders/resources.cfg-base" "$OpenViBE_base/dist/share/openvibe-applications/brain-invaders/brain-invaders/resources.cfg" 

cat "$OpenViBE_base/dist/share/openvibe/kernel/resources.cfg" >> "$OpenViBE_base/dist/share/openvibe-applications/brain-invaders/brain-invaders/resources.cfg"

cd "$OpenViBE_base/dist/share/openvibe-applications/brain-invaders/brain-invaders"

../../../../bin/Brain-invaders-dynamic ../../../../share/brain-invaders.conf
