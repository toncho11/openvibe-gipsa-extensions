///* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
// * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
// * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
// * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
// * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
// 
// * This file is part of Brain Invaders.
// * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
//
//#if defined TARGET_OS_Windows
//
//#ifndef __Openvibe_H__
//#define __Openvibe_H__
//
//#include <stdlib.h>
////#include <cmath>
////#include <random>
//
////#include <iostream>
//#include <windows.h>
////#include <iomanip>
//#include <iostream>
//#include <string.h>
//using namespace std;
//
//#include <tlhelp32.h> //Windows tool help library
//#include "Helper.h"
//
//#include <boost/algorithm/string.hpp>
//#include <boost/filesystem.hpp> //file exits
//
///*!
//	 * \author Anton Andreev (Gipsa)
//	 * \date 2013-05-20
//	 * \brief Class that starts/stops OpenVibe
//	 *
//	 * \details 
//	 * 
//	 */
//class OpenVibe
//{
//  public:
//
//	OpenVibe();
//	~OpenVibe();
//
//	/**
//	* \brief 
//	*/
//    void StartScenario();
//
//    void OpenVibe::StopScenario();
//
//    string GetOpenVibeRootFolder();
//
//  private:
//
//      bool __fastcall KillProcessTree(DWORD myprocID, DWORD dwTimeout,string processname);
//	  bool IsOS64bitWindows(); //if not it is assumed 32 bit
//	  string GetCmdOnWindows();
//	  bool IfDirectoryExists(string strPath);
//	  bool IfFileExists(string strPath);
//	  string GetShortName(string path);
//
//	  HANDLE m_openvibe_handle;
//      DWORD m_openvibe_processid;
//};
//
//#endif
//
//#endif 
