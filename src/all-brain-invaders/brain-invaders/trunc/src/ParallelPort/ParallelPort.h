#if defined TARGET_OS_Windows

#ifndef __OpenViBEApplication_ParallelPort_H__
#define __OpenViBEApplication_ParallelPort_H__

#define _PPort_DLLFileName_ "inpout32.dll"

#include <Windows.h>


class ParallelPort{

	public:

		ParallelPort::ParallelPort();
		ParallelPort::ParallelPort(int portNumber);

		void ppTAG(unsigned int x);
		int ParallelPortAdr;

	private:

		int instantiateParallelPort();

		typedef int (__stdcall *PPort_IN)(short PortAddress);
		typedef int (__stdcall *PPort_OUT)(short PortAddress, short data);

		HINSTANCE g_hPPortInstance;
		PPort_IN g_fpPPort_IN;
		PPort_OUT g_fpPPort_OUT;

};


#endif

#endif