/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_BrainInvadersScreen_H__
#define __OpenViBEApplication_BrainInvadersScreen_H__

#include "SpaceInvadersScreen.h"
#include "BrainInvadersApplication.h"
#include "Alien.h"
#include "AlienBlock.h"
#include "SoundManager.h"
#include "../Config.h"
#include "../Helper.h"
#include <random>
#include "../OpenVibe.h"
using namespace BrainInvaders;

#include <Ogre.h>
#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	#include "Overlay/OgreOverlayManager.h"
#endif

#define MAXMATRIXSIZE 25
#define MAXBLOCKS 36

namespace BrainInvaders {

	/*!
	 * \author Gijs van Veen (Gipsa), Anton Andreev (Gipsa)
	 * \date 2012-03-19
	 * \brief 
	 *
	 * \details
	 * 
	 */
	class BrainInvadersScreen : public SpaceInvadersScreen{

	public:

		/**
		* \brief Constructor.
		* \param ScrMan The BrainInvadersApplication class managing this screen.
		* \param mSM The Ogre::SceneManager managing all elements.
		*/
		BrainInvadersScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config);

		/**
		* \brief Destructor.
		*/
		~BrainInvadersScreen();

		void ResetTimer();

	protected:

		///**
		//* \brief Loads all the Assets this screen will need.
		//*/
		//virtual void LoadAssets() = 0;

		/**
		* \brief Loads a Level. See level specification for details. It will check the game mode "Training/Online/Multiplyer" and load the correct level.
		* \param The number of the level.
		* \return true if level correctly loaded.
		*/
		bool LoadLevel(int level);

		/**
		* \brief Sets the flashing sequence of the Rows/columns.
		*/
		bool GenerateFlashSequence();

		/**
		* \brief Reads in the target sequence for training-mode. "count" is the amount of targets requested
		*/
		std::deque<std::pair<int,int> > GenerateTargetSequence(int count);

		/**
		* \brief Cleans all the matrices of the BrainInvadersScreen.
		*/
		void CleanMatrices();

		/**
		* \brief Shuffles all the Aliens in the Matrix so that they appear to flash randomly.
		*/
		void ShuffleMatrix();

		/**
		* \brief Acquires flash group,sets the flash duration and visualizes the flash
		*/
		void FlashNext();

		/**
		* \brief Unflashes all Flashed Aliens.
		*/
		void UnFlashGroup();

		/**
		* \brief  Gets the next time between a flash.
		*/
		double GetNextISI();

		/**
		* \brief Finds the next target in the training mode.
		* \return true if there is a next target.
		*/
		bool GetNextTarget();

		/**
		* \brief Processes the VRPN result from OpenVibe for data related to shooting
		* \return Returns true if there was a message/flag processed.
		*/
		bool ProcessVRPNTargeting();

		/**
		* \brief Processes the VRPN result for messages using VRPN buttons. These messages act like flags for specific control events from OpenVine to Brain Invaders 
		* \return Returns true if there was a signal processed.
		*/
		bool ProcessVRPNMessages();

		/**
		* \brief Calculates the best score and selects an alien to shoot.
		*/
		void CalculateTarget(Alien*& p_toShoot, std::vector<double>& p_vTabP300, const std::vector<double>& p_vCurrentTargetResponse, bool& p_bResetTabP300);

		///**
		//* \brief Code for shooting a random Alien. Temp code.
		//*/
		//virtual bool ShootAlien() = 0;

		/**
		* \brief Returns the current Time in milliseconds.
		*/
		int GetTimeInMilliSeconds();

		int alienMatrixActualRowsCount();

		int alienMatrixActualColumnsCount();

		/**
		* \brief Returns rows + columns count (for 3x2 it will return 5)
		*/
		int totalRowsAndColumns();

		/**
		* \brief Returns the total number of flashes 
		*/
		int flashesPerRepetition();

		/**
		* \brief Saves the next state and puts the current game state to "OnHold". Then the game continues from this saved (next state). On hold means that the games is paused/freezed.
		*/
		void PutOnHold();

		void AdjustMeanISI(); //this is to increase the time between two flashes if the user is not performing well

		//The numbers must be regenerated because the parameters of the distribution changes when ISIAdaptation = true
		void GenerateNewISISequence();

		//Returns the alien at specified visual position (on the screen)
		//This is due to shuffling of the alienMatrix.
		Alien* GetAlienAtVisualPosition(int row, int column);

		//Variables:

		Config m_config;

		Overlay * m_overlay;											//!< The Overlay (GUI) of the BrainInvadersScreen.

		Timer stateTimer;											//!< Timer used to jump to new gameplay state.
		Timer movementTimer;										//!< Timer used to move the Alien blocks.
		Timer levelTimer;											//!< This timer seems redundant

		int m_currentLevel;											//!< The current Level the game is at.
		int m_totalLevelsPlayed;									//!< Total levels played which includes (won + lost)
			
		SoundManager *soundManager;									//!< Manages the sound played by the game.

		CAbstractVrpnPeripheral * m_poVrpnPeripheral;				//!< Peripheral to handle communications through the VRPN.
		
		Alien * alienMatrix[MAXMATRIXSIZE][MAXMATRIXSIZE];			//!< Matrix of all the Aliens in the game.
		AlienBlock * alienBlocks[MAXBLOCKS];						//!< List of all the AlienBlocks in the game.

		//double prevTime;											//!< previous Time Since last Update. Used to ignore the avaraging of the Ogre Timestamp.
		double elapsedTime;											//!< Elapsed time in seconds since last reset.
		double flashTime;											//!< Time in seconds a flash should last.
		double ISITime;												//!< Time in seconds the pause between flashes should last.

		enum state
		{													//!< Drawing States.
			BlackScreen,
			SendTargetInfo,
			ShowTarget,
			Flash, //this is the state while a flashed group of aliens is visible on the screen
			ISI, //state between flashes (no alien group is visible)
			Pause,
			WinScreen,
			LooseScreen,
			Shoot,
			ShowExplosion,
			FinishTrain,
			OnHold, //added by Anton for pausing when artifact is detected in OpenVibe
		};

		std::deque<std::pair<int,char> > m_vFlashSequence;			//!< The sequence in which to flash rows and columns

		std::deque<std::pair<int,int> > m_vTargetSequence;			//!< Target sequence for training mode.
		std::deque<std::pair<int,int> > m_vTargetSequencePlayer2;	//!< Target sequence for training mode.

		std::deque<double> m_vISISequence;          //!< A list of ISI values (times between two stimulations/blinks)

		std::pair<int,int> m_targetAlien;
		std::pair<int,int> m_targetAlienPlayer2;

		Alien* m_toShoot;												//!< Alien to shoot at the end of a repetition. In multiplayer this is Player 1
		Alien* m_toShootPlayer2;										//!< Alien to shoot at the end of a repetition. In multiplayer this is Player 2
		
		bool m_bResetTabP300;										    //!< Determines whether the P300 values should be added to the previous one. For optimizing detection. It starts with 'true' a level load and after the first shot changes to 'false'. 
		bool m_bResetTabP300Player2;									//!< Determines whether the P300 values should be added to the previous one. For optimizing detection. It starts with 'true' a level load and after the first shot changes to 'false'. 

		//two matrices represented as vectors:
		std::vector<double> m_vCurrentTargetResponse;					//!< List of P300 respone for each column/row for a repetition. These values are provided by VRPN
		std::vector<double> m_vTabP300;								    //!< List of scores used to determine the selected item. Used to accumulate values for rows and columns.

		//two matrices represented as vectors:
		std::vector<double> m_vCurrentTargetResponsePlayer2;			//!< List of P300 respone for each column/row for a repetition. These values are provided by VRPN
		std::vector<double> m_vTabP300Player2;						    //!< List of scores used to determine the selected item. Used to accumulate values for rows and columns.

		int m_currentRepetition;												//!< Current repetiton/attempts within level.

		int m_score;													//!< Player 1 score (can be negative)
		int m_scorePlayer2;                                             //!< Player 2 score (can be negative)

		bool m_isFirstTargetDestroyedCoop2Targets;

		int m_currentState;											//!< Current drawing state.
		
		int m_flashesSinceLastRepetition;

		//bool isLoading;												//!< Indicates whether game is loading something.

		bool m_isTrainingMode;											//!< True if in training-mode.

		bool m_onHold; 

		int m_cachedState;

		Helper m_helper;

		bool m_firstPlay; //!< Used for operations that must be performed only when one level has been played

		/*#if defined TARGET_OS_Windows
		OpenVibe m_openvibe;
        #endif*/
	};
};
#endif