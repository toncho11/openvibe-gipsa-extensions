/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_SoundManager_H__
#define __OpenViBEApplication_SoundManager_H__

#pragma comment(lib,"openal32.lib")
#pragma comment(lib,"alut.lib")

#if defined TARGET_OS_Windows
#include <al.h>
#include <alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif


#define NUMBERSOUNDS 6

namespace BrainInvaders {

	/*!
	 * \author Gijs van Veen (Gipsa)
	 * \date 2012-03-19
	 * \brief Manages all sounds in Brain Invaders
	 *
	 * \details Class that loads and plays all sounds used by Brain Invaders.
	 * 
	 */
	class SoundManager{

	public:
		/**
		* \brief Contructor.
		*/
		SoundManager();

		/**
		* \brief Destructor.
		*/
		~SoundManager();

		/**
		* \brief Plays the next movement sound
		*/
		void move();

		/**
		* \brief Plays the explosion sound.
		*/
		void explode();

	private:

		int currentSound;					//!< Keeps track of which movement sound has been played.

		ALCdevice* device;					//!< Audio Device.
		ALCcontext* context;				//!< Audio Context.

		ALuint source[NUMBERSOUNDS];		//!< All the sounds that can be played.
		ALuint buffer[NUMBERSOUNDS];		//!< Sound Buffers.

		ALsizei size,freq;					//!< Some settings to configure sound manager.
		ALenum 	format;
		ALvoid 	*data;

		enum{								//!< List of sounds present.
			move0,
			move1,
			move2,
			move3,
			mothership,
			explosion
		};

	};

};

#endif