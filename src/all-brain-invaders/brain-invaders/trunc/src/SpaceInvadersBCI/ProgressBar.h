/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_PROGBAR_H__
#define __OpenViBEApplication_PROGBAR_H__

#include <Ogre.h>
using namespace Ogre;

#include <string>
using namespace std;

namespace BrainInvaders {

	/*!
	 * \author Gijs van Veen (Gipsa)
	 * \date 2012-03-19
	 * \brief A bar to keep track of player progress in a level.
	 *
	 * \details A Bar that displays the amount of shots a player still has left during the level.
	 * 
	 */
	class ProgressBar{

	public:
		
		/**
		* \brief Constructor.
		* \param mSM Pointer to the sceneManager for drawing purposes
		*/
		ProgressBar(Ogre::SceneManager *mSM, int p_maxRepetitions);

		/**
		* \brief Destructor
		*/
		~ProgressBar();

		/**
		* \brief Sets the visibility of the progress bar.
		* \param isVisible Determines whether the bar should become visible.
		*/
		void visible(bool isVisible);

		/**
		* \brief Tells the bar that a new repetition begins, thus adjusting what is displayed.
		* \brief validShot If true a valid shot was fired. If false, some error in the protocol created an invalid shot.
		*/
		void nextRepetition(bool validShot);

		/**
		* \brief Resets the progress bar to a specified amount of repetitions.
		* \param repetitions The amount of repetitions that the progress bar should display.
		*/
		void reset(int repetitions);

	private:

		SceneManager* sceneManager;					//<! SceneManager for the Ogre settings.
		SceneNode* barNode;							//<! Node that the progress bar elements are linked to.

		int m_maxRepetitions;							//<! Amount of repetitions the progress bar should display.
		int m_currentRepetition;						//<! Amount of repetitions in the progress bar already fired.

		Entity* barDots[24];			//<! Target display for each repetition.

	};

};
#endif