/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_WLScreen_H__
#define __OpenViBEApplication_WLScreen_H__

#include "SpaceInvadersScreen.h"
#include "BrainInvadersApplication.h"

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
#include <Overlay\OgreOverlayElement.h>
#else
#include <OgreTextAreaOverlayElement.h>
#endif

#include "GameScreen.h"
using namespace BrainInvaders;

#include <Ogre.h>
#include <CEGUI.h>
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#else
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#endif
#include <OIS.h>
using namespace Ogre;

#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
	const CEGUI::UVector2 USize;
};
#endif

namespace BrainInvaders {

	/*!
	 * \author Gijs van Veen (Gipsa)
	 * \date 2012-03-19
	 * \brief Screen to display whether a level was Win or Lost.
	 *
	 * \details Screen that is displayed at the end of each level and tells the player whether he was successful or failed.
	 * 
	 */
	class WinLooseScreen : public SpaceInvadersScreen{

	public:

		/**
		* \brief Constructor.
		* \param ScrMan BrainInvadersApplication managing this Screen.
		* \param mSM The Ogre::SceneManager managing all assets.
		* \param mWM The CEGUI::WindowManager managing the Window drawn in.
		* \param m_poSheet The CEGUI::Window to draw the GUI on.
		* \param GS The GameScreen this screen is pausing.
		*/
		WinLooseScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CEGUI::WindowManager *mWM, CEGUI::Window *m_poSheet, GameScreen *GS, int pauseBetweenLevels);
		
		/**
		* \brief Sets the visibility of this screen
		* \param isVisible Indicates the visibility of the screen.
		*/
		virtual void visible(bool isVisible);

		/**
		* \brief Updates the GUI.
		* \param timeSinceLastUpdate The time in seconds since the GUI was last updated.
		*/
		virtual bool update(double timeSinceLastUpdate);
		
		/**
		* \brief Sets the screens visibility.
		* \param isVisible Indicates whether the screen should be visible.
		*/
		virtual void keyPressed(const OIS::KeyEvent& evt);

		bool m_isLastGameWon;									//!< True if the previous game was won.

		int m_scoreP1;                                  //!< score of the player1
		int m_scoreP1new;                               //!< new score of the player1, used to compute the performance during one level
		int m_scoreP2;                                  //!< score of the player2
		int m_scoreP2new;                               //!< new score of the player2, used to compute the performance during one level

		Timer timerWinLoose;									//!< Timer to ensure a minimum of screen time.

	private:
		
		CEGUI::WindowManager *m_poGUIWindowManager;		//!< The CEGUI::WindowManager this GUI is drawn on.

		std::vector<CEGUI::Window*> WinLooseWindows;

		double elapsedTime;								//!< Time elapsed.
		
		//bool startedLoading;							//!< Determines whether the next level is being loaded.

		GameScreen *gameScreen;							//!< GameScreen this Screen pauses.

		bool skipScreen;

		unsigned long m_pauseBetweenLevels;

		void createOgreWindow(CEGUI::Window *m_poSheet, 
	                          std::string imageResourceParth, 
							  std::string windowName, 
							  CEGUI::UVector2 windowPosition, 
							  CEGUI::USize windowSize);

	};
};
#endif