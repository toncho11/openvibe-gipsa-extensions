/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __Helper_H__
#define __Helper_H__

#include <stdlib.h>
#include <cmath>

#include <random>

//for console color print
#include <iostream>
#if defined TARGET_OS_Windows
#include <windows.h>
#endif
#include <iomanip>
using namespace std;

#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

/*!
	 * \author Anton Andreev (Gipsa-lab)
	 * \date 2013-05-01
	 * \brief A general helper class for Brain Invaders
	 *
	 * \details 
	 * 
	 */
class Helper
{
  public:

	Helper();
	/**
	* \brief Returns a random non repetive sequnece from [0..n) 
	*/
    static int* GenerateRandomNonRepetiveSequnece(int n);

	/**
	* \brief Generate a random number following the exponential distribution. This is a custom implementation.

	*/
	double GenerateRandomNumberExpCustom(double expDistMean);

	/**
	* \brief Generate a random number following the exponential distribution. This implementation uses the new C++ built in functions.
	*/
	double GenerateRandomNumberExpBuiltIn(double expDistMean);

	/**
	* \brief Generate a random number following the exponential distribution. This is a version that depending on the mean it uses different functions 
	* to generate numbers in way that is closest to the exponetial distributions. Comes from the these of Gregoire Cattan.
	* min = 0.05f
	* max = 1
	*/
	double GenerateRandomNumberExpAdaptive(float mean, float min, float max);

	/**
	* \brief Generate a random number following the uniform distribution between 0 and 1. 
	*/
	double GenerateRandomNumberUniformZeroOne();

	/**
	* \brief Generate a random number following the uniform distribution. Equivalent to: int rand (void);
	*/
	double GenerateRandomNumberUniform(int max);

	/**
	* \brief Generate a random number following the uniform distribution between two numbers a and b
	*/
	float GenerateRandomNumberUniform(float a, float b);

	#if defined TARGET_OS_Windows
	//start print code
	enum colour { DARKBLUE = 1, DARKGREEN, DARKTEAL, DARKRED, DARKPINK, DARKYELLOW, GRAY, DARKGRAY, BLUE, GREEN, TEAL, RED, PINK, YELLOW, WHITE };

	struct setcolour
	{
		colour _c;
		HANDLE _console_handle;


			setcolour(colour c, HANDLE console_handle)
				: _c(c), _console_handle(0)
			{ 
				_console_handle = console_handle;
			}
	};

	friend basic_ostream<char>& operator<<(basic_ostream<char> &s, const setcolour &ref)
	{
		SetConsoleTextAttribute(ref._console_handle, ref._c);
		return s;
	}
    #endif
	//end print code

	/**
	* \brief Sleep function
	*/
	static void delay(int milliseconds);

	static std::string GetCurrentDateTime();

	static std::string GetEnvVariable(string name);

	static void DoConsolePause();


  private:

	mt19937 m_rng;

	float NextISI_1(float mean, float min, float max);

	float NextISI_2(float mean, float min, float max);

	float NextISI_3(float mean, float min, float max);

	float Bound(float a_f, float a_fMin, float a_fMax);

};

#endif

