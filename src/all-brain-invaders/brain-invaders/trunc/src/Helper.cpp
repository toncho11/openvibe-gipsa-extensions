#include "Helper.h"

Helper::Helper()
{
	std::random_device rseed; //used to generate a single seed number 
    m_rng = mt19937(rseed());
}

int* Helper::GenerateRandomNonRepetiveSequnece(int n)
{
	int* numbers = new int[n];
	int* result  = new int[n];

	for (int i=0;i<n;i++)
	{
		*(numbers+i) = i; 
	}

	for (int i=0;i<n;i++)
	{
		int m = rand() % (n - i);
		*(result+i) = numbers[m];

		numbers[m] = numbers[n - i - 1];
	}

	delete[] numbers; 

	return result;
}

double Helper::GenerateRandomNumberExpCustom(double expDistMean)
{
	double x = (double) rand() / (double) RAND_MAX;
	double landa = 1 / expDistMean;
	double result = landa * std::exp(-landa * x);
		
	return result;
}

double Helper::GenerateRandomNumberExpBuiltIn(double expDistMean)
{
    double const exp_dist_lambda = 1 / expDistMean;

	exponential_distribution<> exp_disp = exponential_distribution<>(exp_dist_lambda);

    double result = exp_disp(m_rng);

	return result;
}

// new functions that comes from the anlysis of Gregoire Cattan
float Helper::NextISI_1(float mean, float min = 0.05f, float max = 1)
{
	float b = std::exp(-min / mean);
	float a = std::exp(-max / mean);

	float randNumber = Helper::GenerateRandomNumberUniform(a, b);

	float result = -std::log(randNumber) * mean ;

	return result;
}

float  Helper::NextISI_2(float mean, float min = 0.05f, float max = 1)
{
	float correction = 9.4223 * std::pow(mean, 6) - 27.085 * std::pow(mean, 5) + 31.97 * std::pow(mean, 4) - 19.595 *
		std::pow(mean, 3) + 7.2719 * std::pow(mean, 2) - 1.1045 * mean;

	float value = NextISI_1(mean, min, max) + correction;
	return Bound(value, min, max);
}

float  Helper::NextISI_3(float mean, float min = 0.05f, float max = 1)
{
	return min + NextISI_1(mean - min, 0, max - min);
}

double  Helper::GenerateRandomNumberExpAdaptive(float mean, float min = 0.05f, float max = 1)
{
	if (mean <= 0.25)
	{
		return NextISI_3(mean, min, max);
		//return GenerateRandomNumberUniform(min, max);
	}
	if (mean <= 0.4) //(0.25..04]
	{
		return NextISI_2(mean, min, max);
	}
	// Here, real mean will not match expected mean
	return NextISI_1(mean, min, max); //> 0.4
}

float  Helper::Bound(float a_f, float a_fMin, float a_fMax)
{
	return a_f < a_fMin ? a_fMin : a_f > a_fMax ? a_fMax : a_f;
}

double Helper::GenerateRandomNumberUniformZeroOne()
{
    uniform_real_distribution<double> uniform_dist = uniform_real_distribution<double>(0.0,1.0);

	double result = uniform_dist(m_rng);

	return result;
}

double Helper::GenerateRandomNumberUniform(int max)
{
	uniform_int_distribution<int> uniform_dist = uniform_int_distribution<int>(0,max);

	double result = uniform_dist(m_rng);

	return result;
}

float Helper::GenerateRandomNumberUniform(float a, float b)
{
	uniform_real_distribution<float> uniform_dist = uniform_real_distribution<float>(a, b);

	float result = uniform_dist(m_rng);

	return result;
}

void Helper::delay(int milliseconds)
{
	boost::this_thread::sleep(boost::posix_time::milliseconds(milliseconds));
}

std::string Helper::GetCurrentDateTime()
{
  using namespace boost;

  std::stringstream msg;
  const boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
  boost::posix_time::time_facet* const f=new boost::posix_time::time_facet("%H:%M:%S");//%d-%b-%Y %H:%M:%S
  
  msg.imbue(std::locale(msg.getloc(),f));

  msg << now;
  string result = msg.str();
  return result;
}

std::string Helper::GetEnvVariable(string name)
{
   #include <stdlib.h>

   char *tmp;

   tmp = getenv( name.c_str() );

   string result="";
   if( tmp != NULL )
   {
      result = tmp;
   }

   return result;
}

void Helper::DoConsolePause()
{
	#if defined TARGET_OS_Windows 
		system("pause");
	#else
		system("read -t5 -n1 -r -p \"Press any key in the next five seconds...\"");
	#endif
}
