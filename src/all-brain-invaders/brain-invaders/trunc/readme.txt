This source can be compiled with the git source tree of OpenVibe:

https://gitlab.inria.fr/openvibe

This is for OpenVibe 2.2 or later:

Place the contents of this folder folder in: \openvibe\extras\externals\brain_invaders

Then (re)compile OpenVibe.

The instructions for playing Brain Invaders are available in the \doc folder or online.

Start it with either the BrainInvaders Launcher or \openvibe-src\dist\extras-Release-x86\ov-brain-invaders.cmd

A troubleshooting guide is available online.