/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "Config.h"
#include "Helper.h"

Config::Config()
{
	CommProtocol = SerialPort;

	ShuffleEnabled = true;

	FlashNonTargetTime = 0.1;
	FlashTargetTime = 0.15;
	PauseTime = 2;

	Lives = 8;    
	RepetitionsPerLife = 1; //!< How many repetition to show to the user before getting a result and losing a life
	
	MaxTrainingRepetitionsPerTarget = 8;
	MaxTrainingTargets = 5;

	PauseBetweenLevels = 3000; //3 seconds 

	LogToDisk = true;
	LogToConsole =true;

	#if defined TARGET_OS_Windows
	  ComPortName = "COM5";
    #else
	  ComPortName = "/dev/ttyACM0";
    #endif

	VrpnPeripheral = "openvibe-vrpn@localhost";

	MinISI = 0.05;
	MaxISI = 1;
	MeanISI = 0.2;

	PauseBetweenRowsAndColumns = 0.400;
	PauseAfterDestruction = 0.5;

	VRPNTargetReceivedTimeout = 2;

	ShowOgreSetup = false;

	ISIAdaptationEnabled = false; //for now it is not tested
	OGREDebugLoggingEnabled = false;

	PercentISIAdapt = 20;

	#if defined TARGET_OS_Windows
	   ManageOpenVibeEnabled = false;
    #else
       ManageOpenVibeEnabled = false;
    #endif

	SavePath = Helper::GetEnvVariable("savepath");
}

bool Config::LoadConfigurationFile(int argc, char **argv)
{
	char* filename = argv[1];
	printf("Searching for config file: %s\n", filename);

    std::ifstream infile(filename);

	if (!infile.good())
	{
		printf("\nConfig file not detected!\n");

		#if defined TARGET_OS_Windows 
		system("pause");
        #else
	    system("read -t5 -n1 -r -p \"Press any key in the next five seconds...\"");
        #endif

		return false;
	}

	printf("Reading config file...\n");

	std::string line;
	while (std::getline(infile, line))
	{
		//cout << "line = " << line << endl;
		if (line.size()==0 || line[0]=='#') continue; 
		std::istringstream iss(line);
		//printf("%s\n",line.c_str());
		char a[100];
		char b[100];

		if (line=="") continue;
		if (!(iss >> a >> b)) { cout <<"Line not processed:" << line << endl; continue; } // error
		
		//cout << "a=" << a << " b=" << b <<endl;
		if (strcmp(a,"CommProtocol:")==0 && strcmp(b,"8bit")==0) {CommProtocol = EightbitPP;}
		if (strcmp(a,"CommProtocol:")==0 && strcmp(b,"software")==0) {CommProtocol = SoftwareTagging;}
		if (strcmp(a,"CommProtocol:")==0 && strcmp(b,"serial")==0) {CommProtocol = SerialPort;}
		if (strcmp(a,"CommProtocol:")==0 && strcmp(b,"robik")==0) {CommProtocol = Robik;}

		if (strcmp(a,"ComPortName:")==0 && strcmp(b,"")!=0) {ComPortName = strdup(b);}
		
		if (strcmp(a,"ShuffleEnabled:")==0)
			if (strcmp(b,"true")==0) {ShuffleEnabled=true;}
		    else if (strcmp(b,"false")==0) {ShuffleEnabled=false;}
		
		if (strcmp(a,"FlashNonTargetTime:")==0 && strcmp(b,"")!=0) {FlashNonTargetTime = to_double(b);}
		if (strcmp(a,"FlashTargetTime:")==0 && strcmp(b,"")!=0) {FlashTargetTime = to_double(b);}
		if (strcmp(a,"PauseBetweenLevels:")==0 && strcmp(b,"")!=0) {PauseBetweenLevels = atoi(b);}
		
		//if (strcmp(a,"MaxRepetitions:")==0 && strcmp(b,"")!=0) {MaxRepetitions = atoi(b);}
		if (strcmp(a,"MaxTrainingTargets:")==0 && strcmp(b,"")!=0) {MaxTrainingTargets = atoi(b);}

		if (strcmp(a,"VrpnPeripheral:")==0 && strcmp(b,"")!=0) {VrpnPeripheral  = strdup(b);}
		
		if (strcmp(a,"LogToDisk:")==0)
			if (strcmp(b,"true")==0) {LogToDisk = true;} 
		    else if (strcmp(b,"false")==0) {LogToDisk = false;} 

		if (strcmp(a,"LogToConsole:")==0)
			if (strcmp(b,"true")==0) {LogToConsole = true;}
		    else if (strcmp(b,"false")==0) {LogToConsole = false;}

		if (strcmp(a,"MinISI:")==0 && strcmp(b,"")!=0) {MinISI = to_double(b);}
		if (strcmp(a,"MaxISI:")==0 && strcmp(b,"")!=0) {MaxISI = to_double(b);}
		if (strcmp(a,"MeanISI:")==0 && strcmp(b,"")!=0) {MeanISI = to_double(b);}
		if (strcmp(a,"PauseBetweenRowsAndColumns:")==0 && strcmp(b,"")!=0) {PauseBetweenRowsAndColumns = to_double(b);}

		if (strcmp(a,"PauseAfterDestruction:")==0 && strcmp(b,"")!=0) {PauseAfterDestruction = to_double(b);}

		if (strcmp(a,"ShowOgreSetup:")==0)
			if (strcmp(b,"true")==0) {ShowOgreSetup = true;}
		    else if (strcmp(b,"false")==0) {ShowOgreSetup = false;}	

		if (strcmp(a,"VRPNTargetReceivedTimeout:")==0 && strcmp(b,"")!=0) {VRPNTargetReceivedTimeout = to_double(b);}

		if (strcmp(a,"ISIAdaptationEnabled:")==0)
			if (strcmp(b,"true")==0) {ISIAdaptationEnabled=true;}
		    else if (strcmp(b,"false")==0) {ISIAdaptationEnabled=false;}

		if (strcmp(a,"OGREDebugLoggingEnabled:")==0)
			if (strcmp(b,"true")==0) {OGREDebugLoggingEnabled=true;}
		    else if (strcmp(b,"false")==0) {OGREDebugLoggingEnabled=false;}

		if (strcmp(a,"PercentISIAdapt:")==0 && strcmp(b,"")!=0) {PercentISIAdapt = atoi(b);}

		if (strcmp(a,"ManageOpenVibeEnabled:")==0)
			if (strcmp(b,"true")==0) {ManageOpenVibeEnabled=true;}
		    else if (strcmp(b,"false")==0) {ManageOpenVibeEnabled=false;}

        if (strcmp(a,"Lives:")==0 && strcmp(b,"")!=0) {Lives = atoi(b);}
		if (strcmp(a,"RepetitionsPerLife:")==0 && strcmp(b,"")!=0) {RepetitionsPerLife = atoi(b);}
		if (strcmp(a,"MaxTrainingRepetitionsPerTarget:")==0 && strcmp(b,"")!=0) {MaxTrainingRepetitionsPerTarget = atoi(b);}
			
	}
	
	//return BConfig;
	cout << "Done." << endl;
	return true;
}

bool Config::LoadConfigurationCmd(int argc, char **argv)
{
	printf("Reading config from command line...\n");

	CommProtocol = SerialPort;//default protocol number

	if (argc >= 2){
		if(strcmp(argv[1],"2bit") == 0)
			CommProtocol = TwobitPP;
		else if(strcmp(argv[1],"8bit") == 0)
			CommProtocol = EightbitPP;
		else if(strcmp(argv[1],"software") == 0)
			CommProtocol = SoftwareTagging;
		else if(strcmp(argv[1],"serial") == 0)
			CommProtocol = SerialPort;
	}

	ShuffleEnabled = true;
	if (argc >= 3)
		ShuffleEnabled = (strcmp(argv[2],"true") == 0);

	LogToDisk = true;
	if (argc >= 4)
		LogToDisk = (strcmp(argv[3],"true") == 0);

	LogToConsole = false;
	if (argc >= 5)
		LogToConsole = (strcmp(argv[4],"true") == 0);

	return true;
}


std::string Config::GetConfiguration()
{
	std::stringstream msg;

	msg << endl << "Current configuration:\n";

	msg << "Installer version: 1.9_08_09_2013" << endl;
	if (CommProtocol == TwobitPP) msg << "Communication Protocol = 2 bit parallel port (deprecated)" << endl;
	else if (CommProtocol == EightbitPP) msg << "Communication Protocol = 8 bit parallel port" << endl;
	else if (CommProtocol == SoftwareTagging) msg << "Communication Protocol =  software tagging" << endl;
	else if (CommProtocol == SerialPort) msg << "Communication Protocol = serial port" << endl;
	else if (CommProtocol == Robik) msg << "Communication Protocol = robik" << endl;
	else  msg << "Communication Protocol = INVALID!!!" << endl;

	msg << "ShuffleEnabled = " << ShuffleEnabled << endl;
	msg << "ComPortName = \"" << ComPortName << "\"" << endl;
	msg << "FlashTime for non-target (in seconds) = " << FlashNonTargetTime << endl;
	msg << "FlashTime for target (in seconds) = " << FlashTargetTime << endl;
	msg << "Lives = " << Lives << endl;
	msg << "RepetitionsPerLife = " << RepetitionsPerLife << endl;
	msg << "MaxTrainingRepetitionsPerTarget = " << MaxTrainingRepetitionsPerTarget << endl;
	msg << "MaxTrainingTargets = " << MaxTrainingTargets << endl;
	msg << "PauseBetweenLevels (in milliseconds) = " << PauseBetweenLevels << endl;

	msg << "MinISI (in seconds) = " << MinISI << endl;
	msg << "MaxISI (in seconds) = " << MaxISI << endl;
	msg << "MeanISI (in seconds) = " << MeanISI << endl;
	msg << "PauseBetweenRowsAndColumns (in seconds) = " << PauseBetweenRowsAndColumns << endl;

	msg << "PauseAfterDestruction (in seconds) = " << PauseAfterDestruction << endl;

	msg << "VRPNTargetReceivedTimeout (in seconds) = " << VRPNTargetReceivedTimeout << endl;

	msg << "LogToDisk = " << LogToDisk << endl;
	msg << "LogToConsole = " << LogToConsole << endl;
	msg << "VrpnPeripheral = " << VrpnPeripheral << endl;
	msg << "ISI Adaptation Enabled = " << ISIAdaptationEnabled << endl;
	msg << "OGRE Debug Logging Enabled = " << OGREDebugLoggingEnabled << endl;
	msg << "Percent ISI Adapt = " <<  PercentISIAdapt << endl;
	msg << "Manage OpenVibe Enabled = " <<  ManageOpenVibeEnabled << endl;
	msg << "Save path = " << ((SavePath!="") ? SavePath : "not set")  << endl;
	msg << "NREP env variable = " << ((Helper::GetEnvVariable("NREP") != "") ? Helper::GetEnvVariable("NREP") : "not set") << endl;
	msg << endl;

	return msg.str();
}

void Config::PrintConfiguration()
{
	cout << Config::GetConfiguration();
}

double Config::to_double ( const char *p )
{
	std::stringstream ss ( p );
	double result = 0;
	ss>> result;
	return result;
}

int Config::MaxRepetitions(bool isTraining)
{
	if (isTraining)
	{
		return MaxTrainingRepetitionsPerTarget * MaxTrainingTargets;
	}
	else
	{
	  return Lives * RepetitionsPerLife;
	}
}