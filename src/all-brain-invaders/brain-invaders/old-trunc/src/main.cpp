/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "SpaceInvadersBCI/CSpaceInvadersBCI.h"
#include "Communication/OVComm.h"
#include "Config.h"

#if 0
#if defined OVA_OS_Linux
namespace CEGUI
{
	Exception::Exception(const String& s)
	{
		Exception(s, "", "", 0);
	}
}
#endif
#endif

void PrintLogo()
{
	printf("BRAIN INVADERS application started ! \n\n");
	printf("\n");
	printf("                                       \n");
    printf("             ##             ###        \n");
    printf("             ##             ###        \n");
    printf("               ###        ##           \n");
    printf("               ###        ##           \n");
    printf("             ##################        \n");
    printf("             ##################        \n");
    printf("          #####   ########  ######     \n");
    printf("       :::#####:::########::######::   \n");
    printf("       #############################   \n");
    printf("       ###   ##################   ##   \n");
    printf("       ###   ##################   ##   \n");
    printf("       ###   ##             ###   ##   \n");
    printf("       ###   ##             ###   ##   \n");
    printf("               #####�  #####           \n");
    printf("               #####�  #####           \n");
    printf("               ������  �����           \n");
    printf("               ������  �����           \n");
    printf("       ���   ��             ���   ��   \n");
    printf("       ���   ������������������   ��   \n");
    printf("       �����������������������������   \n");
    printf("          �����   ��������  ������     \n");
	printf("                                       \n");
    printf("                                       \n");
    printf("                 �#########            \n");
    printf("                 �#########            \n");
    printf("           #######################     \n");
    printf("         �.#######################��   \n");
    printf("        ############################   \n");
    printf("        #######     #####    #######   \n");
    printf("        #######     #####    #######   \n");
    printf("        ############################   \n");
    printf("        ############################   \n");
    printf("               #####     ####          \n");
    printf("             ::##:��:::::��##:::       \n");
    printf("             ####.  #####  #####       \n");
    printf("        #####                   ####   \n");
    printf("        #####                   ####   \n");
    printf("        �����                   ����   \n");
    printf("        �����                   ����   \n");
    printf("             ����   �����  �����       \n");
    printf("               �����     ����          \n");
    printf("        ����������������������������   \n");
    printf("    ������           �������     ����  \n");
	printf("                                       \n");
    printf("                                       \n");
	printf("                  ######               \n");
    printf("                  ######               \n");
    printf("               ###########�            \n");
    printf("               ###########�            \n");
    printf("             #################         \n");
    printf("             #################         \n");
    printf("          ######   ######   #####      \n");
    printf("          ######   ######   #####      \n");
    printf("          #######################      \n");
    printf("          #######################      \n");
    printf("                ###      ##�           \n");
    printf("                ###      ##�           \n");
    printf("             ###   ######   ##         \n");
    printf("             ###   ######   ##         \n");
    printf("          ###   ###      ##�  ###      \n");
    printf("          ###   ###      ##�  ###      \n");
    printf("          ���   ���      ���  ���      \n");
    printf("          ���   ���      ���  ���      \n");
    printf("             ���   ������   ��         \n");
    printf("                ���      ���           \n");
    printf("                ���      ��            \n");
    printf("       �������         ��������������� \n");
	printf("\n");
}

Config GetConfiguration(int argc, char **argv)
{
	Config BConfig;
	//set default
	bool success = false;
	if (argc>1)
	{
	   bool success = BConfig.LoadConfigurationFile(argc,argv);

	   if (!success)
	   {
		   /*printf("Processing configuration file has failed!\n");
		   success = BConfig.LoadConfigurationCmd(argc,argv);*/
		   printf("Processing configuration file has failed! Using built-in default values.\n");
	   }
	}

	return BConfig;
}

void PerformPause()
{
	cout << endl;
	#if defined TARGET_OS_Windows 
	system("pause");
    #else
	  //system("read -t5 -n1 -r -p \"Press any key in the next five seconds...\"");
    #endif
}

int main(int argc, char **argv)
{
	//PrintLogo();

	printf("\nInitializing Brain Invaders...\n");
	printf("website: http://code.google.com/p/openvibe-gipsa-extensions/wiki/BrainInvaders\n\n");

	Config BConfig = GetConfiguration(argc,argv);
	BConfig.PrintConfiguration();

	BrainInvadersApplication * app = new CSpaceInvadersBCI(BConfig);

	PerformPause();
	
	//start 3d engine
	app->go();

	delete app;

	return 0;
}