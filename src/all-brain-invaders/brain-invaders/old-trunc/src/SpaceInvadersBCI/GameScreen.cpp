/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "GameScreen.h"
#include "CSpaceInvadersBCI.h"
#include "AlienBlock.h"
using namespace BrainInvaders;

#include <Ogre.h>
#include <OgreFont.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>
using namespace Ogre;

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sstream>
#include <time.h>
#include <math.h>
#include <vector>
//#include <system/Time.h> //for sleep function
using namespace std;

#include "../tinyxml/tinyxml.h"

GameScreen::GameScreen(CSpaceInvadersBCI *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config) : 
           BrainInvadersScreen(ScrMan, mSM, vrpnPeripheral, config)

{
	LoadAssets();

	progressBar = new ProgressBar(sceneManager,m_config.Lives);

	SetUpOverlay();  
	screenManager->LogEvent("Brain Invaders: Overlay done",true,false);
}

void GameScreen::LoadAssets()
{
	// Loading in all sprites beforehand makes sure animations dont lag.
	Entity *tempEntity = sceneManager->createEntity("temp","cube.mesh");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_1_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_1_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_1_F");
	sceneManager->destroyEntity("temp");
}

bool GameScreen::update(double timeSinceLastUpdate)
{
	bool moved = false;
	int milliTime = movementTimer.getMilliseconds();
	
	// Move all the blocks.
	if (!m_onHold)
	{
		for (int i = 0; i < MAXBLOCKS; i++){
		
			if(alienBlocks[i] != NULL && (m_currentState != BlackScreen && m_currentState != ShowTarget)){
				bool temp = alienBlocks[i]->updateBlock(milliTime);
				moved = moved || temp;
			}
		}
	}
	
	// If there is a movement, play a sound.
	if (moved){
		screenManager->LogEvent("Alien grid moved",true,false);
		soundManager->move();
		movementTimer.reset();
	}
	
	// Switch over GameStates
	switch(m_currentState){

		case BlackScreen:

			// wait specific time and depending on the game logic choose the next state 

			// Go to Pause Time
			if (stateTimer.getMilliseconds() >= 1000*m_config.PauseTime){
				// For training we need to select the target.
				
				if (m_isTrainingMode)
				{
					// No target then end training.
					if (!GetNextTarget()){//get next target
						screenManager->communicationHandler->SetBeginProcessing();
						m_currentState = FinishTrain;
						stateTimer.reset();
						break;
					}
				}

				stateTimer.reset();
				
				m_currentState = SendTargetInfo;
				visible(true);
				
				// Show the current Target because we leave black screen
				for (int i = 0; i < MAXMATRIXSIZE; i++)
					for (int j = 0; j < MAXMATRIXSIZE; j++)
						if (alienMatrix[i][j] != NULL)
							alienMatrix[i][j]->ShowTargetMode();
				screenManager->LogEvent("Show Target",true,false);
			}
			break;

		//this state seems redundant, it only prints the target data to the console
		case SendTargetInfo:
			if (stateTimer.getMilliseconds() >= 150)
			{
				stateTimer.reset();

				//start logging
				screenManager->LogEvent("\n\n======Start New Repetition ======\n",true,false);
				std::stringstream ss;
				ss << "Send Target Info\n\tRow: " << m_targetAlien.second << "\n\tColumn: " << m_targetAlien.first;
				screenManager->LogEvent(ss.str(),true,false);
				//end logging

				m_currentState = ShowTarget;
			}
			break;

		case ShowTarget:	
			{
				// if it is the first repetion then give the user a time to focus 
			    // and if not then this delay variable is the time of the alien destruction animation
				int delay = (m_currentRepetition==0) ? 2 :  m_config.PauseAfterDestruction;

				if (stateTimer.getMilliseconds() >= 1000 * delay)
				{
					stateTimer.reset();

					screenManager->communicationHandler->SetFinalized();
				
					// Make sure all is unflashed (also stops Target display)
					for (int i = 0; i < MAXMATRIXSIZE; i++)
						for (int j = 0; j < MAXMATRIXSIZE; j++)
							if (alienMatrix[i][j] != NULL)
								alienMatrix[i][j]->UnFlash();//aliens change to unflash sprite

					m_currentState = Flash;//start flashing again

					FlashNext();//picks which row or column to flash next
				}
			}
			break;

		case Flash: //wait for the flash to complete, unflash it
			
			if (stateTimer.getMilliseconds() >= 1000*flashTime)
			{
				stateTimer.reset();

				//mark the end of SetRow() or SetColumn() from FlashNext()
				screenManager->communicationHandler->SetFinalized();

				this->UnFlashGroup();//Flash Finished, Unflash

				// Set up the next flash.
				m_vFlashSequence.pop_front();

				//Are we at the end of the repetition.
				bool l_bMoreRowsToFlash = (!(m_flashesSinceLastRepetition >= flashesPerRepetition()));

				if (l_bMoreRowsToFlash) //within a repetition 
				{
					// If there is a next flash, go to ISI state.
					m_currentState = ISI;
				}
				else// Else handle a new repetition
				{
					m_flashesSinceLastRepetition = 0;

					GenerateFlashSequence(); //refill flash sequence, because it is reduced after each repetion to a 0 count

					m_currentRepetition++; //moved

					if (m_isTrainingMode) //training mode repetition
					{ 
						// For training we need to go back to Black Screen State (= target selection)
						m_currentState = ISI;//keep flashing
						//m_currentRepetition++;

						if (m_currentRepetition >= m_config.MaxTrainingRepetitionsPerTarget)
						{
							m_currentRepetition = 0;
							m_currentState = BlackScreen;//go black screen to act as pause and show the new target
						}
					}
					else //online mode repetition
					{
						// Start preparing next repetition.
						Helper::delay(50);//System::Time::sleep(50);
						screenManager->communicationHandler->SetRepetionCompeted();
						Helper::delay(50);//System::Time::sleep(50);
						screenManager->communicationHandler->SetFinalized();
						
						//check if it is the time for shooting
						if (m_currentRepetition % m_config.RepetitionsPerLife == 0)
						     m_currentState = Shoot;
					}
				}

			}
			break;

		case ISI: // Flash Next Alien Group
			
			if (stateTimer.getMilliseconds() >= 1000*ISITime) //wait ISI(time between flashes) before flashing
			{	
				if (m_flashesSinceLastRepetition + 1 == this->alienMatrixActualRowsCount())
					//We make the time between flashing all rows and all columns to be fixed.
					//We knows that rows are first and m_flashesSinceLastRepetition is being reset every repetion.
					ISITime = m_config.PauseBetweenRowsAndColumns; 
				else
					ISITime = GetNextISI();
				
				m_currentState = Flash;
				
				stateTimer.reset();
				
				FlashNext(); //acquire flash target,set the flash duration, and visualize the flash
			}
			break;
        
		case OnHold:
			//cout << "games paused due to artifact/bad signal ...";

			if (ProcessVRPNMessages() && m_onHold==false) //exit OnHold state
			{
				m_currentState = m_cachedState;

				std::stringstream ss;
	            ss << "score: " << m_score;
				overlay->getChild("PanelName")->getChild("TextAreaName")->setCaption(ss.str().c_str());

				/*cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
				cout<<"--------------------GAME STARTED--------------------"<<endl;
				cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;*/
			}
			else
			{
				Helper::delay(30);
				//cout<<"No message"<<endl;
			}
			break;

		case Shoot:
			{   
				bool validShotDetected = ProcessVRPNTargeting(); //this function is called many times, bacuase the "Shoot" state does not change until the timeout below expire or result has been received

				if (stateTimer.getMilliseconds() >= (m_config.VRPNTargetReceivedTimeout * 1000) || validShotDetected) //wait for OpenVibe to respond
				{
					stateTimer.reset();

					//shoot an alient whether provided from OpenVibe or random
					m_currentState = SendTargetInfo;
					screenManager->communicationHandler->SetFinalized();
					
                    // Shoot the selected Alien.
					bool isTargetShot = ShootAlien(); //sets state to WinScreen if target is hit
					
		            if (isTargetShot) 
				    {
						m_currentState = WinScreen;
					}
					else
					{
						// Start a new repetition, go to LooseScreen if this is the last one.
						//m_currentRepetition++; //moved to flash state

						if (m_currentRepetition >= m_config.MaxRepetitions(m_isTrainingMode))
						{
							screenManager->LogEvent("Level lost.",true,true);
							m_currentState = LooseScreen;
						}
					}

					//detects and sends to OV that level has been completed
					if (m_currentState == WinScreen || m_currentState == LooseScreen)
					{
					    screenManager->communicationHandler->BI_SetLevelCompleted();
						Helper::delay(50);//System::Time::sleep(50);
						screenManager->communicationHandler->SetFinalized();
					}
				}
			}
			break;

		case FinishTrain:

			// Finishes the training of the game.
			if (stateTimer.getMilliseconds() >= 150)
			{
				stateTimer.reset();
				screenManager->communicationHandler->SetFinalized();
				screenManager->FinishGame();
			}
			break;

		case WinScreen:

			// Loads new level and shows winning screen.
			if (stateTimer.getMilliseconds() >= 2000)
			{
				stateTimer.reset();
				currentLevel++;

				//Show Load Screen -> FinishLevel() -> processLevel() -> Show Game Screen (back here)
				screenManager->ActivateLoadScreen(true);
			}
			break;

		case LooseScreen:
			// Resets the current level and shortly shows a loosing screen
			if (stateTimer.getMilliseconds() >= 2000)
			{
				stateTimer.reset();

				//Show Load Screen -> FinishLevel() -> processLevel() -> Show Game Screen (back here)
				screenManager->ActivateLoadScreen(false);
			}
			break;

		default:
			break;
	}

	//PutOnHold() checks if the last message is to pause (to put on hold) and then set the current state to "OnHold"
	//While in the OnHold state the game checks if the flag for resuiming is raised. If so it reverts to the planned state.
	//If again we get pause/on hold message then we do nothing - actually we wait in the "OnHold" state where it is safe to wait and where we check the queue for the resume message/flag.
	//if m_onHold == true then messages/flags are now checked in the "OnHold" state, so we don't check here
	if (!m_onHold) 
	{	
		bool stateChanged = ProcessVRPNMessages();
		if (stateChanged && m_onHold)
	    {
		   PutOnHold();
		}
	}

	return true;
}

bool GameScreen::ShootAlien()
{
	string type = "";
	// If we do not have a proper target, pick a random one and tell the progress bar we did a false shot.
	if (m_toShoot == NULL)
	{
		type = "random ";
		progressBar->nextRepetition(false);
		int randRow;
		int randColumn;
		
		while(m_toShoot == NULL || !m_toShoot->alive)
		{
			randRow = rand() % alienMatrixActualColumnsCount();
		    randColumn = rand() %    alienMatrixActualRowsCount();
			
			//Find the alien in the visual grid in position [randRow, randColumn]
			m_toShoot = GetAlienAtVisualPosition(randRow,randColumn);
		}
	}
	else //Target available
	{
		type = "vrpn supplied ";
		progressBar->nextRepetition(true);
	}

	std::stringstream ss;
	ss << "Shooting " << type << "alien at: row: " << m_toShoot->VisualGridPosRow << " column: " << m_toShoot->VisualGridPosColumn;
	screenManager->LogEvent(ss.str(),true,true);
	ss.str("");//clear stream

	// Shoot the alien.
	int alienType = m_toShoot->destroy();

	// Adjust the core and give an exploding sound.
	if (alienType != -1)
	{
		m_score -= 50;
		soundManager->explode();
	}

	// If target hit, calculate new score
	if (alienType == Alien::AlienType::Target)
    {
		// Get points dependent on the amount of attempts.
		m_score += 16000 * std::pow(0.5, (double) (m_currentRepetition-1));
	}

	// Update the score
	ss << "score: " << m_score;
	overlay->getChild("PanelName")->getChild("TextAreaName")->setCaption(ss.str().c_str());

	//send feedback to OpenVibe whether the user shot the target alien
	if (alienType == Alien::AlienType::Target)
	  screenManager->communicationHandler->BI_SetPositiveFeedback();
	else 
		screenManager->communicationHandler->BI_SetNegativeFeedback();

	Helper::delay(50);//System::Time::sleep(50);
	screenManager->communicationHandler->SetFinalized();
	Helper::delay(50);//System::Time::sleep(50);
	//end of feedback

	return alienType == Alien::AlienType::Target;
}

void GameScreen::visible(bool isVisible)
{
	//cout << "Brain Invaders: Visible start" <<endl; 

	// We also want invisibility if we're at a Black Screen.
	bool areAliensVisible = !(m_currentState == BlackScreen || !isVisible);

	for (int i = 0; i < MAXMATRIXSIZE; i++){
		for(int j = 0; j <MAXMATRIXSIZE; j++){
			if(alienMatrix[i][j] != NULL)
				alienMatrix[i][j]->setVisible(areAliensVisible);
		}
	}

	// Show the progressBar (lets hide it in training Mode)
	progressBar->visible(areAliensVisible && !m_isTrainingMode);

	if (areAliensVisible  && !m_isTrainingMode)
		overlay->show();
	else
		overlay->hide();

	// Reset the timers, since they probably ran out.
	if (isVisible){
		stateTimer.reset();
		movementTimer.reset();
	}

	//cout << "Brain Invaders: Visible done" <<endl;
}

void GameScreen::keyPressed(const OIS::KeyEvent& evt)
{
	if ( evt.key == OIS::KC_ESCAPE)
	{
		// Go to main menu if Escape is hit.
		screenManager->LeaveGame();

		if (m_config.ManageOpenVibeEnabled)
	    {
			screenManager->LogEvent("Stopping OpenVibe ...",true,true);
			#if defined TARGET_OS_Windows
			m_openvibe.StopScenario();
			#endif
		}
	}

	if ( evt.key == OIS::KC_RBRACKET){
		// Cheat Button to load the next level. Usefull for level testing.

		// We don't want to skip flashes, so we reload the sequence for safety reasons.
		GenerateFlashSequence();
		m_currentState = WinScreen;
	}

	//Manually resume game if you need to
	//This sends the reset signal to OpenVibe, OpenVibe sends back a resume game flag from the Potato
	if ( evt.key == OIS::KC_R && m_currentState == OnHold)
	{
		screenManager->communicationHandler->SetResetFlag();
	    Helper::delay(50);//System::Time::sleep(50);
	    screenManager->communicationHandler->SetFinalized();
	    Helper::delay(50);//System::Time::sleep(50);
	}
}

bool GameScreen::ProcessLevel(int level,state gameState)
{
	//Start clean-up
	// Clean old level
	for(int i = 0; i < MAXBLOCKS; i++){
		if(alienBlocks[i] != NULL)
			delete alienBlocks[i];
	}

	// Make sure the matrices are clean so we dont use any old memory
	CleanMatrices();
	//End clean-up

	//Start new level:

	// ISI generation, adapt ISI, set first value
	if (!m_firstPlay && m_config.ISIAdaptationEnabled) 
		AdjustMeanISI();
	GenerateNewISISequence();
	ISITime = GetNextISI();

	// Load level from file
	bool levelLoaded = LoadLevel(level);
	if (!levelLoaded) return false; 

	// Shuffling the matrix to ensure random flashing and such
	ShuffleMatrix();

	// Start a new repetition cycle.
	m_currentRepetition = 0; //restart repetition counter
	m_bResetTabP300 = true;
		
	progressBar->reset(m_config.Lives);
	m_currentState = gameState; //setting initial state

	// We must regeneate the flashes according to the new matrix size in the just loaded level
	GenerateFlashSequence();

	if (m_isTrainingMode)
	{
		// We need targets to train on.
		GenerateTargetSequence();
	}

	m_firstPlay = false;
    //end new level 

	return true;
}

void GameScreen::Initialize(bool isTraining)
{
	screenManager->LogEvent("Initializing BrainInvaders screen ...",true,false);

	m_flashesSinceLastRepetition = 0;
	m_onHold=false;
	flashTime = m_config.FlashNonTargetTime;
	m_isTrainingMode = isTraining;
	m_firstPlay = true;
	state gameState = BlackScreen;

	screenManager->LogEvent(m_config.GetConfiguration(),true,false);

	if (m_config.ManageOpenVibeEnabled)
	{
		#if defined TARGET_OS_Windows
	    m_openvibe.StartScenario();
		#endif
	    screenManager->LogEvent("Waiting for OpenVibe to start ...",true,true);

		//Wait and then resume
		m_onHold = true;
		m_cachedState = BlackScreen; //return state
		gameState = OnHold;
	}

	// Load in the first level, so we can start playing immediately.
	ProcessLevel(1,gameState);//sets state to = BlackScreen

	if (m_config.ManageOpenVibeEnabled)
	{
		Helper::delay(3000);
		//Reinitialize VRPN
	    if (m_poVrpnPeripheral!=NULL) delete m_poVrpnPeripheral;
		m_poVrpnPeripheral = new CAbstractVrpnPeripheral(this->m_config.VrpnPeripheral);
		m_poVrpnPeripheral->init();
		//Helper::delay(2000);
	}
}

void GameScreen::FinishLevel()
{
	// Make sure all blocks and the Matrix are cleaned up.
	for(int i = 0; i < MAXBLOCKS; i++)
	{
		if(alienBlocks[i] != NULL)
			delete alienBlocks[i];
	}

	CleanMatrices();

	// Try to load the next Level;
	bool levelLoadedSuccessfully = ProcessLevel(currentLevel,BlackScreen);
	
	if (!levelLoadedSuccessfully)
	{
		std::stringstream ss;
	    ss << "Final score: " << m_score; 
		screenManager->LogEvent(ss.str(),true,true);

		// If loadLevel is unsuccesful load first level and return to main menu.
		screenManager->FinishGame();
	}
}

GameScreen::~GameScreen()
{
	delete progressBar;
}

void GameScreen::SetUpOverlay()
{
	// Load the Font
	FontManager &fontMgr = FontManager::getSingleton();
	ResourcePtr font = fontMgr.create("GameFont","General");
	font->setParameter("type","truetype");
	font->setParameter("source","Smirnof.ttf");
	font->setParameter("size","20");
	font->setParameter("resolution","96");
	font->load();

	// Load the overlay
	OverlayManager& overlayMgr = OverlayManager::getSingleton();
	Ogre::OverlayContainer* panel = static_cast<OverlayContainer*>(
    overlayMgr.createOverlayElement("Panel", "PanelName"));
	panel->_setPosition(0.05, 0.925);
	panel->setDimensions(300, 120);

	TextAreaOverlayElement* textArea = static_cast<TextAreaOverlayElement*>(
    overlayMgr.createOverlayElement("TextArea", "TextAreaName"));
	textArea->setMetricsMode(Ogre::GMM_PIXELS);
	textArea->setPosition(0, 0);
	textArea->setDimensions(300, 120);
	textArea->setCharHeight(24);
	textArea->setColour(ColourValue(1,1,0));
	// set the font name to the font resource that you just created.
	textArea->setFontName("GameFont");
	// say something

	std::stringstream ss;
	ss << "score: " << m_score;
	textArea->setCaption(ss.str().c_str());

	overlay = overlayMgr.create("OverlayName");
	overlay->add2D(panel);

	panel->addChild(textArea);
}




