/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_CSpaceInvadersBCI_H__
#define __OpenViBEApplication_CSpaceInvadersBCI_H__

#include <Ogre.h>        ////////////////////////difference
using namespace Ogre;        ////////////////////////difference
#include "../BrainInvadersApplication.h"
#include "SpaceInvadersScreen.h"
using namespace BrainInvaders;

#include "../Communication/OVComm.h"
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <boost/date_time.hpp>
#include "../Config.h"
using namespace std;

#define SCREEN_NUM 4


namespace BrainInvaders {

	class SpaceInvadersScreen;

	/*!
	 * \author Gijs van Veen (Gipsa)
	 * \date 2012-03-19
	 * \brief The Brain Invaders Game. Extends BrainInvadersApplication.
	 *
	 * \details Brain Invaders, a BCI controlled version of Space Invaders.
	 * 
	 */
	class CSpaceInvadersBCI : public BrainInvadersApplication
	{
	public:

		/**
		* \brief Constructor.
		* \param newProtocol Integer giving the protocol to use.
		* \param doShuffle true if we want to shuffle the flash matrices
		* \param addValues true if we want to add VRPN values for each repetition.
		*/
		CSpaceInvadersBCI(Config config);

		/**
		* \brief Makes the game Exit.
		*/
		void ExitGame();

		/**
		* \brief Finishes the current game, resetting everything back to default.
		*/
		void FinishGame();

		/**
		* \brief Starts the game at a given level.
		* \param level The level the game should start in.
		*/
		void StartGame(bool Training);

		/**
		* \brief Returns the game to its Pauze screen.
		*/
		void LeaveGame();

		/**
		* \brief Sets up the Loading Screen.
		* \param won Indicated whether the loading screen is applied for winning or loosing players.
		*/
		void ActivateLoadScreen(bool won);

		void LogEvent(std::string eventInfo,bool toFile, bool toConsole);

		OVComm* communicationHandler;							//!< Handles the communication through the parallel port.

		/**
		* \brief Activates the given screen.
		* \param toActivate The screen to activate, as specified in the Enumeration.
		*/
		void activateScreen(int toActivate);

		enum{													//!< Enumeration of all the screens.
			GUI,
			MainGame,
			Loading,
			Keyboard
		};
	protected:


	private:
		/**
		* \brief Initializes the scene, camera, lights and GUI.
		* \return \em true if the scene is successfully set up.
		*/
		virtual bool initialise(void);

		/**
		* \brief Updates the active SpaceInvadersScreen.
		* \param timeSinceLastProcess The time in seconds since the last Update.
		* \param Tells whether game will continue after this frame.
		*/
		virtual bool process(double timeSinceLastProcess);

		/**
		* \brief Processes Keyboard events
		* \param evt The Keyboard Event.
		* \return True if processed correctly.
		*/
		bool keyPressed(const OIS::KeyEvent& evt);

		/**
		* \brief Sets up all the screens.
		*/
		void loadScreens();

		SpaceInvadersScreen* screenManager[SCREEN_NUM];			//!< All the SpaceInvadersScreens used in the game.
		int currentScreen;										//!< The currently active SpaceInvadersScreen.

		bool m_bContinue;										//!< Indicates whether to continue the game.

		bool resetTimer;

		std::ofstream gameLog;

		boost::posix_time::ptime m_StartGameTime;
	};
};
#endif //__OpenViBEApplication_CSpaceInvadersBCI_H__
