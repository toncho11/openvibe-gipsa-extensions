/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_GUIScreen_H__
#define __OpenViBEApplication_GUIScreen_H__

#include "SpaceInvadersScreen.h"
#include "CSpaceInvadersBCI.h"
using namespace BrainInvaders;

#include <Ogre.h>
#include <CEGUI.h>
#include <OIS.h>
using namespace Ogre;

namespace BrainInvaders {

	/*!
	 * \author Gijs van Veen (Gipsa)
	 * \date 2012-03-19
	 * \brief The GUI Screen of Brain Invaders.
	 *
	 * \details This is the Graphical User Interface of the Brain Invaders game.
	 * 
	 */
	class GUIScreen : public SpaceInvadersScreen{

	public:

		/**
		* \brief Constructor.
		* \param ScrMan CSpaceInvadersBCI managing this Screen.
		* \param mSM The Ogre::SceneManager managing all assets.
		* \param mWM The CEGUI::WindowManager managing the Window drawn in.
		* \param m_poSheet The CEGUI::Window to draw the GUI on.
		*/
		GUIScreen(CSpaceInvadersBCI *ScrMan, SceneManager *mSM, CEGUI::WindowManager *mWM, CEGUI::Window *m_poSheet);
		
		/**
		* \brief Sets the visibility of this screen
		* \param isVisible Indicates the visibility of the screen.
		*/
		virtual void visible(bool isVisible);

		/**
		* \brief Updates the GUI.
		* \param timeSinceLastUpdate The time in seconds since the GUI was last updated.
		*/
		virtual bool update(double timeSinceLastUpdate);
		
		/**
		* \brief Keys pressed while this screen is active
		*/
		virtual void keyPressed(const OIS::KeyEvent& evt);

	private:
		CEGUI::WindowManager *m_poGUIWindowManager;		//!< The CEGUI::WindowManager this GUI is drawn on.

		double elapsedTime;								//!< Time elapsed.

		bool startMenuVisible;							//!< Indicates the visibility of the start menu text.

	};
};
#endif