#include "GUIScreen.h"
#include "CSpaceInvadersBCI.h"
using namespace BrainInvaders;

#include <Ogre.h>
/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include <OgreFont.h>
#include <OgreFontManager.h>
#include <CEGUI.h>
#include <OIS.h>
using namespace Ogre;

#include <stdlib.h>
#include <stdlib.h> 

GUIScreen::GUIScreen(CSpaceInvadersBCI *ScrMan, SceneManager *mSM, CEGUI::WindowManager *mWM, CEGUI::Window *m_poSheet) : SpaceInvadersScreen(ScrMan, mSM)
{
	m_poGUIWindowManager = mWM;
	
	const std::string l_sFondMenu = "Resources/GUI/brainInvadersMain.png";
	const std::string l_sTextMenu = "Resources/GUI/startText.png";

	//----------- CREATE WINDOWS -------------//
	// background window
	CEGUI::Window * l_poFondMenu  = m_poGUIWindowManager->createWindow("TaharezLook/StaticImage", "FondMenu");
	l_poFondMenu->setPosition(CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)) );
	l_poFondMenu->setSize(CEGUI::UVector2(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));
	m_poSheet->addChildWindow(l_poFondMenu);	
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageFondMenu",l_sFondMenu); 
	l_poFondMenu->setProperty("Image","set:ImageFondMenu image:full_image");
	l_poFondMenu->setProperty("FrameEnabled","False");
	l_poFondMenu->setProperty("BackgroundEnabled","False");

	// text menu 2
	CEGUI::Window * l_poTextMenu  = m_poGUIWindowManager->createWindow("TaharezLook/StaticImage", "TextMenu");
	l_poTextMenu->setPosition(CEGUI::UVector2(cegui_reldim(0.25f), cegui_reldim(0.8f)) );
	l_poTextMenu->setSize(CEGUI::UVector2(CEGUI::UDim(0.5f, 0.f), CEGUI::UDim(0.1f, 0.f)));
	m_poSheet->addChildWindow(l_poTextMenu);	
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageTextMenu",l_sTextMenu); 
	l_poTextMenu->setProperty("Image","set:ImageTextMenu image:full_image");
	l_poTextMenu->setProperty("FrameEnabled","False");
	l_poTextMenu->setProperty("BackgroundEnabled","False");
}



void GUIScreen::visible(bool visible){
	startMenuVisible = true;
	elapsedTime = 0;
	m_poGUIWindowManager->getWindow("FondMenu")->setVisible(visible);
	m_poGUIWindowManager->getWindow("TextMenu")->setVisible(visible);
}

bool GUIScreen::update(double timeSinceLastUpdate){
	elapsedTime += timeSinceLastUpdate;
	// Lets blink the TextMenu. Gives it a more "gamey" feel.
	if(elapsedTime > 0.8){
		elapsedTime = 0;
		startMenuVisible = !startMenuVisible;
		m_poGUIWindowManager->getWindow("TextMenu")->setVisible(startMenuVisible);
	}
	return true;
}

void GUIScreen::keyPressed(const OIS::KeyEvent& evt){
	if ( evt.key == OIS::KC_ESCAPE){
		// Exit the game completely at escape.
		screenManager->ExitGame();
	}
	if ( evt.key == OIS::KC_SPACE){
		// Space Starts a new Game in on-line mode
		screenManager->StartGame(false);
	}
	if ( evt.key == OIS::KC_T){
		// The T starts the Training round.
		screenManager->StartGame(true);
	}
	if ( evt.key == OIS::KC_K){
		// The K starts the Keyboard screen
		screenManager->activateScreen(CSpaceInvadersBCI::Keyboard);
	}
}