#include "OVComm.h"

#include "OV_software_tagger.h"

#if defined ROBIK_TAGGING
#include "../Medoc/medoc_client_helper.h"
#include "../Medoc/protocol.h"
#define ROBIK_TAGGING_PORT 1980
#endif 

//Brain Invaders specific:
//for level complete
#define OVTK_New_Level                                0x312 // 786 //OVTK_GDF_Cross_On_Screen  
//Positive feedback
#define OVTK_Positive_Feedback                              0x30D // 781 //OVTK_GDF_Feedback_Continuous 
//Negative feedback
#define OVTK_Negative_Feedback                               0x30E // 782 //OVTK_GDF_Feedback_Discrete 

OVComm::OVComm(eProtocolVersion ver,char* input) : m_eProtocol(ver)
{
	m_targetBaseRow=100;
	m_targetBaseColumn=120;

	m_flashBaseRow=20;
	m_flashBaseColumn=40;

	m_repetionCompleted = -1; //must be set manually with a method if used
	m_experimentStart = -1; //must be set manually with a method if used
	m_beginTraining = 192;

	if (ver == SoftwareTagging)
	{
		m_STmessageQueueName = input;
		ST_Init(m_STmessageQueueName);
	}

    #if defined TARGET_OS_Windows

	else if (ver == EightbitPP || ver == TwobitPP)
	{
		std::cout << "Parallel port mode selected!!!" << std::endl;

		int portAddress = 0x378; //use default

		std::string sInput(input);

		if (sInput.size()>0)
		{
			if (is_number(sInput)) //use user supplied address
			{
				portAddress = atoi(input); 
				//std::cout << "Parallel port address set." << std::endl;
			}
		    else std::cout << "ERROR: The parallel port address that you provided is invalid!!!" << std::endl;
		}
		
		std::cout << "The parallel port address is: \"" << portAddress << "\"(in decimal 0x378=888)." << std::endl;
		if (portAddress < 0 || portAddress > 32767) std::cout << "WARNING: The parallel port address that you provided might be INVALID with Inpout32.dll 32 bit version!!!" << std::endl;
		
		m_pport = new OpenViBEVRDemos::ParallelPort(portAddress);
	}

	#endif

	else if (ver == SerialPort)
	{
		m_serial = new serialib();

		#if defined TARGET_OS_Windows
		//special care is taken for ports >9: http://stackoverflow.com/questions/11775185/open-a-com-port-in-c-with-number-higher-that-9
		std::string input_str(input);
		std::transform(input_str.begin(), input_str.end(), input_str.begin(), ::toupper);
		if (input_str=="COM10") input = "\\\\.\\COM10";
		if (input_str=="COM11") input = "\\\\.\\COM11";
		if (input_str=="COM12") input = "\\\\.\\COM12";
		if (input_str=="COM13") input = "\\\\.\\COM13";
		if (input_str=="COM14") input = "\\\\.\\COM14";
		#endif

		lLastError = m_serial->Open(input,115200);
		if ( lLastError == 1 )
		{
           std::cout << "Serial port initialized successfully on port: \"" << input << "\"." << std::endl;
		}
		else
        {
           std::cout << std::endl << "ERROR: Serial port intialization FAILED on port: \"" << input << "\" !!!" << std::endl;
        }
	}

	#if defined ROBIK_TAGGING
	else if (ver == Robik)
	{
		lLastError = MedocClientDoConnect( ROBIK_TAGGING_PORT );
		if ( lLastError == 0 )
		{
           std::cout << std::endl << "Connected to port " << ROBIK_TAGGING_PORT << "." << std::endl;
		}
		else
        {
           std::cout << std::endl << "Medoc: Failed to connect to port " << ROBIK_TAGGING_PORT << std::endl;
        }
	}
	#endif
    
	//to initialize tagging (to send 0 in the case of serial port, so that the the first rising edge is detected)
	this->SetFinalized();
}

void OVComm::SetTargetRow(int rowNumber) //starts from 0
{
	int row = m_targetBaseRow + rowNumber;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{	
		m_pport->ppTAG(128);
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(row);
	}
	else 
	#endif
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(row);
	}
    else
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code =  row;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(row);
	}
    #endif
	
}

void OVComm::SetTargetColumn(int columnNumber) //starts from 0
{
	int column = m_targetBaseColumn + columnNumber;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{	
		m_pport->ppTAG(128);
	}
	else 	
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(column);
	}
	else 
	#endif
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(column);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = column; 
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(column);
	}
    #endif
}

void OVComm::SetFlashRow(int rowNumber) //starts from 0
{
	int row = m_flashBaseRow + rowNumber;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		m_pport->ppTAG(64);
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(row);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(row);
	}
	else
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = row;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(row);
	}
    #endif
}

void OVComm::SetFlashColumn(int columnNumber) //starts from 0
{
	int column = m_flashBaseColumn + columnNumber;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		m_pport->ppTAG(64);
	}
	else 	
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(column);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(column);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = column;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(column);
	}
    #endif
}

//used for testing purposses
void OVComm::TestWriteValue(int value)
{
	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{	
		m_pport->ppTAG(128);
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(value);
	}
	else 
	#endif
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(value);
	}
    else
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code =  value;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(value);
	}
    #endif
	
}

//called after every other set method to finalize it
void OVComm::SetFinalized()
{
	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		m_pport->ppTAG(0);
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(0);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(0);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		//software tagging does not need to perform anything here
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(0);
	}
    #endif
}

//called at the end of the training
void OVComm::SetBeginProcessing()
{
	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		m_pport->ppTAG(m_beginTraining);
	}
	else 	
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(m_beginTraining);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(m_beginTraining);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = m_beginTraining;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(m_beginTraining);
	}
    #endif
}

//called in the beginning - before you start flash
void OVComm::SetStartExperiment()
{
	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		//not used
	}
	else 	
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(m_experimentStart);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(m_experimentStart);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = m_experimentStart;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(m_experimentStart);
	}
    #endif
}

void OVComm::SetRepetionCompeted()
{
	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		//not used
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(m_repetionCompleted);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(m_repetionCompleted);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = m_repetionCompleted;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(m_repetionCompleted);
	}
    #endif
}

void OVComm::SetResetFlag()
{
	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		//not used
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(m_resetFlag);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(m_resetFlag);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = m_resetFlag;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(m_resetFlag);
	}
    #endif
}

void OVComm::BI_SetLevelCompleted()
{
	int code = 104;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		//not used
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(code);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(code);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = OVTK_New_Level;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(code);
	}
    #endif
}

void OVComm::BI_SetPositiveFeedback()
{
	int code = 105;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		//not used
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(code);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(code);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = OVTK_Positive_Feedback;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(code);
	}
    #endif
}

void OVComm::BI_SetNegativeFeedback()
{
	int code = 106;

	#if defined TARGET_OS_Windows
	if (m_eProtocol==TwobitPP)
	{
		//not used
	}
	else 
	if (m_eProtocol==EightbitPP)
	{
		m_pport->ppTAG(code);
	}
	else 
	#endif	
	if (m_eProtocol == SerialPort)
	{
		WriteSerial(code);
	}
	else 
	if (m_eProtocol==SoftwareTagging)
	{
		int stimulation_code = OVTK_Negative_Feedback;
		ST_TagNow(stimulation_code);
	}
	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
		WriteMedoc(code);
	}
    #endif
}

void OVComm::WriteSerial(int input)
{
    char c = (char)input;

	m_serial->WriteChar(c);
}


OVComm::~OVComm()
{
	if (m_eProtocol == SoftwareTagging)
	{
		ST_Clear(m_STmessageQueueName);
	}

	else if (m_eProtocol == SerialPort)
	{
		m_serial->Close();

		delete m_serial;
	}

	#if defined TARGET_OS_Windows
	else if (m_eProtocol == EightbitPP || m_eProtocol == TwobitPP)
	{
		delete m_pport;

	}
    #endif

	#if defined ROBIK_TAGGING
	else if (m_eProtocol == Robik)
	{
	  if ( MedocClientDoDisconnect() == 0 )
            std::cout << std::endl << "Medoc: Disconnected." << std::endl;
        else
            std::cout << std::endl << "Medoc: Failed to disconnect" << std::endl;
	}
    #endif
}

void OVComm::ConfigureTargetBaseRowCode(int newcode)
{
	m_targetBaseRow = newcode;
}

void OVComm::ConfigureTargetBaseColumnCode(int newcode)
{
	m_targetBaseColumn = newcode;
}

void OVComm::ConfigureFlashBaseRowCode(int newcode)
{
	m_flashBaseRow = newcode;
}

void OVComm::ConfigureFlashBaseColumnCode(int newcode)
{
	m_flashBaseColumn = newcode;
}

void OVComm::ConfigureRepetionCompletedCode(int newcode)
{
	m_repetionCompleted = newcode;
}

void OVComm::ConfigureExperimentStartCode(int newcode)
{
	m_experimentStart = newcode;
}

void OVComm::ConfigureTrainingStartCode(int newcode)
{
	m_beginTraining = newcode;
}

void OVComm::ConfigureResetCode(int newcode)
{
    m_resetFlag = newcode;
}

void OVComm::SetRow(int rowNumber, bool isTarget) //starts from 0
{
	if (isTarget)
		SetTargetRow(rowNumber);
	else SetFlashRow(rowNumber);
}

void OVComm::SetColumn(int columnNumber, bool isTarget) //starts from 0
{
	if (isTarget)
		SetTargetColumn(columnNumber);
	else SetFlashColumn(columnNumber);
}

int* OVComm::ProcessVRPNResult(int numberOfRows,int numberOfColumns,double VRPNData[])
{
	int* result = new int[2];

	//select target row
	float min = VRPNData[0];
	int index = 0;

	for (int i=1;i<numberOfRows;i++)
	{
		if (VRPNData[i] < min)
		{
			min = VRPNData[i];
			index = i;
		}
	}
	result[0] = index;
	//end select target row

	//select target column
	min = VRPNData[numberOfRows];//rows are before columns
	index = numberOfRows;

	for (int i=numberOfRows+1;i<numberOfRows+numberOfColumns;i++)
	{
		if (VRPNData[i] < min)
		{
			min = VRPNData[i];
			index = i;
		}
	}

	result[1] = index - numberOfRows;//to make it 0 based
	//end select target column

	return result;
}

#if defined ROBIK_TAGGING
int OVComm::WriteMedoc(int input)
{
	//0x01 0x00 0x07 0x03 0x03 0x14 (MEDOC_SENSOR_SOFT_TRIGGER) 0x07 0x00 0xXX 0xXX
    unsigned char frame[10] = { 0x01,
                                0x00,
                                0x07,
								0x03,
								0x03,
                                MEDOC_SENSOR_SOFT_TRIGGER,
                                0x07,
								0x00,
                                0x00,   // 1000 ms LSB
                                0x00 }; // 1000 ms MSB
	char* byte = (char*)&input;
	frame[8] = *byte;

    if ( MedocClientDoSendFrame( frame, 10 ) == 0 )
	{
		//std::cout << std::endl << "Brain Invaders value: " << input << std::endl;
        //std::cout << std::endl << "Activated trigger " << (int) frame[4] << std::endl;
	}
    else
        std::cout << std::endl << "Failed to activate trigger!" << std::endl;

	return 0;
}
#endif 

bool OVComm::is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}