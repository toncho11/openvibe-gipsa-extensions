This source can be compiled with the git source tree of OpenVibe:

git://scm.gforge.inria.fr/openvibe/openvibe.git

Place the contents of this folder folder in: openvibe\externals\brain-invaders

The instructions for playing Brain Invaders are available in the \doc folder or online.