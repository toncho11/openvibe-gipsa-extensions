﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Management;

namespace brain_invaders_launcher
{
    public class Helper
    {
        public static void OpenLinkInBrowser(string url)
        {
            //workaround since the default way to start a default browser does not always seem to work...
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = "rundll32.exe";
                process.StartInfo.Arguments = "url.dll,FileProtocolHandler " + url;
                process.StartInfo.UseShellExecute = true;
                process.Start();
            }
            catch { }
        }


        public static string Version
        {
            get
            {
                return "2.7.8";
            }
        }


        public static ushort FindPPAddress()
        {
            /*
             * Standard Parallel Port (SPP) 
             * Enhanced Parallel Port (EPP) FASTER THAN SPP
             * Extended Capabilities Port (ECP) allows for bidirectional, most computers
             */

            ushort SPPIOLength = 8;
            ushort ECPIOLength = 4;

            ushort result = 0;

            SelectQuery portsQuery = new SelectQuery("Win32_PnPEntity",
                "ClassGUID=\"{4D36E978-E325-11CE-BFC1-08002BE10318}\"");

            ManagementObjectSearcher portSearcher = new ManagementObjectSearcher(portsQuery);

            foreach (ManagementObject port in portSearcher.Get())
            {
                // Grab its name. This will be in the form "Blah Blah Parallel Port (LPT1)".
                // We're going to get ugly now...
                string name = (string)port.Properties["Name"].Value;
 
                // Skip ports that are not parallel ports. This is UGLY...I'd much rather
                // do this a cleaner way, but it seems that different parallel ports do
                // things differently.
                if (!name.Contains("(LPT")) { continue; }
 
                // and extract the parallel port name by looking for everything inside
                // the parentheses. I don't know how else to do it...
                int beginLoc = name.IndexOf("(LPT") + 1;
                int endLoc = name.IndexOf(')', beginLoc);
                name = name.Substring(beginLoc, endLoc - beginLoc);
 
                // Grab the device ID so we can find all the associated port resources.
                string deviceID = (string)port.Properties["DeviceID"].Value;
                // Replace single backslashes with double backslashes to be suitable
                // to use in a query. Note that I also had to escape it myself in
                // code...so \\ represents a single backslash.
                // TODO: Any other escaping necessary?
                deviceID = deviceID.Replace("\\", "\\\\");
 
                // Now search for I/O ranges of this port.
                ObjectQuery resourceQuery =
                    new ObjectQuery("ASSOCIATORS OF {Win32_PnPEntity.DeviceID=\"" +
                    deviceID + "\"} WHERE ResultClass = Win32_PortResource");
                ManagementObjectSearcher resourceSearcher =
                    new ManagementObjectSearcher(resourceQuery);
 
                // Find the SPP and ECP base addresses
                ushort SPPBase = 0xFFFF;
                ushort ECPBase = 0xFFFF;

                foreach (ManagementObject resource in resourceSearcher.Get())
                {
                    // Grab starting & ending addresses
                    ushort startAddress =
                        (ushort)(UInt64)resource.Properties["StartingAddress"].Value;

                    ushort endAddress =
                        (ushort)(UInt64)resource.Properties["EndingAddress"].Value;
 
                    ushort rangeLen = (ushort)(endAddress - startAddress + 1);
                    // If we haven't yet found the SPP base address, and this range
                    // is big enough, use it as the SPP base address.
                    if ((SPPBase == 0xFFFF) && (rangeLen >= SPPIOLength))
                    {
                        SPPBase = startAddress;
                    }
                    // If we haven't yet found the ECP base address, and this range
                    // is big enough, use it as the ECP base address
                    else if ((ECPBase == 0xFFFF) && (rangeLen >= ECPIOLength))
                    {
                        ECPBase = startAddress;
                    }
                }
                // Although I didn't include the code here, if I fail to detect an
                // SPP address with the rules above, I grab the first returned
                // address and use it as the SPP base address. Then I set the ECP
                // address to 0xFFFF (which I treat as meaning "unable to find")
 
                // TODO: If SPPBase != 0xFFFF, add it to the list of discovered
                // ports -- the name of the port is stored in the variable "name"

                result = SPPBase;
            }

            return result;
        }
    }

}
