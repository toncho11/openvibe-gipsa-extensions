﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace brain_invaders_launcher
{
    class ComPortHelper
    {
        public static string[] GetUSBCOMDevices()
        {
            List<string> list = new List<string>();

            try
            {
                ManagementObjectSearcher searcher2 = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity");
                foreach (ManagementObject mo2 in searcher2.Get())
                {
                    string name = mo2["Name"].ToString();
                    string service = (mo2["Service"] == null) ? "" : mo2["Service"].ToString();

                    // Name will have a substring like "(COM12)" in it.
                    if (name.Contains("(COM") && service.ToLower().Contains("usbser"))
                    {
                        int start = name.IndexOf("(COM") + 1;
                        if (start >= 0)
                        {
                            int end = name.IndexOf(")", start + 3);
                            String cname = name.Substring(start, end - start);
                            list.Add(cname);
                        }

                    }
                }
                // remove duplicates, sort alphabetically and convert to array
                string[] usbDevices = list.Distinct().OrderBy(s => s).ToArray();
                return usbDevices;
            }
            catch (Exception ex)
            {
                return list.ToArray();
            }
        }

        public static string AutodetectArduinoPort()
        {
            ManagementScope connectionScope = new ManagementScope();
            SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

            try
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    string deviceId = item["DeviceID"].ToString();

                    if (desc.Contains("Arduino"))
                    {
                        return deviceId;
                    }
                }
            }
            catch (ManagementException e)
            {
                /* Do Nothing */
            }

            return null;
        }
    }
}
