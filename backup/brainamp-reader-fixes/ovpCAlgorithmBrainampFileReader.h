#ifndef __OpenViBEPlugins_Algorithm_BrainampFileReader_H__
#define __OpenViBEPlugins_Algorithm_BrainampFileReader_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <fstream>

//Lines added by Anton, should be removed when added to main stream OpenVibe
//These values overwrite the originals
#define OVP_GD_ClassId_Algorithm_BrainampFileReader 	 	OpenViBE::CIdentifier(0x0BA614F5, 0x26541D00)
#define OVP_GD_ClassId_Algorithm_BrainampFileReaderDesc 	OpenViBE::CIdentifier(0x6092674E, 0x462F59F9)
#define OVP_GD_Algorithm_BrainampFileReader_InputParameterId_Filename                                                           OpenViBE::CIdentifier(0x000c486c, 0x477c80b9)
#define OVP_GD_Algorithm_BrainampFileReader_InputParameterId_EpochDuration                                                      OpenViBE::CIdentifier(0x002aee72, 0x288d489e)
#define OVP_GD_Algorithm_BrainampFileReader_InputParameterId_SeekTime                                                           OpenViBE::CIdentifier(0x00627156, 0x55790cae)
#define OVP_GD_Algorithm_BrainampFileReader_OutputParameterId_CurrentStartTime                                                  OpenViBE::CIdentifier(0x003ce019, 0x3dade050)
#define OVP_GD_Algorithm_BrainampFileReader_OutputParameterId_SignalSamples                                                     OpenViBE::CIdentifier(0x0073a91a, 0x6d1d3d26)
#define OVP_GD_Algorithm_BrainampFileReader_OutputParameterId_Stimulations                                                      OpenViBE::CIdentifier(0x008f7c49, 0x6ed710a9)
#define OVP_GD_Algorithm_BrainampFileReader_OutputParameterId_SamplingRate                                                      OpenViBE::CIdentifier(0x00d3cabb, 0x339326c2)
#define OVP_GD_Algorithm_BrainampFileReader_OutputParameterId_CurrentEndTime                                                    OpenViBE::CIdentifier(0x3b87de16, 0xb8efb2fb)
#define OVP_GD_Algorithm_BrainampFileReader_InputTriggerId_Close                                                                OpenViBE::CIdentifier(0x003462ea, 0x031fb8fa)
#define OVP_GD_Algorithm_BrainampFileReader_InputTriggerId_Next                                                                 OpenViBE::CIdentifier(0x003cd062, 0x739f973e)
#define OVP_GD_Algorithm_BrainampFileReader_InputTriggerId_Open                                                                 OpenViBE::CIdentifier(0x00bcf286, 0x6f5fa2f4)
#define OVP_GD_Algorithm_BrainampFileReader_InputTriggerId_Seek                                                                 OpenViBE::CIdentifier(0x00c3acab, 0x4e3de014)
#define OVP_GD_Algorithm_BrainampFileReader_OutputTriggerId_Error                                                               OpenViBE::CIdentifier(0x00b1e3df, 0x3abc6c5a)
#define OVP_GD_Algorithm_BrainampFileReader_OutputTriggerId_DataProduced                                                        OpenViBE::CIdentifier(0x00e7d5f9, 0x1471aff2)


namespace OpenViBEPlugins
{
	namespace FileIO
	{
		class CAlgorithmBrainampFileReader : public OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual OpenViBE::boolean initialize(void);
			virtual OpenViBE::boolean uninitialize(void);
			virtual OpenViBE::boolean process(void);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_BrainampFileReader);

		protected:

			enum EStatus
			{
				Status_Nothing,
				Status_CommonInfos,
				Status_BinrayInfos,
				Status_ChannelInfos,
				Status_MarkerInfos,
				Status_Comment,
			};

			enum EBinaryFormat
			{
				BinaryFormat_Integer16,
				BinaryFormat_UnsignedInteger16,
				BinaryFormat_Float32,
			};

			enum EEndianness
			{
				Endianness_LittleEndian,
				Endianness_BigEndian,
			};

			typedef struct
			{
				OpenViBE::uint64 m_ui64Identifier;
				OpenViBE::uint64 m_ui64StartIndex;
				OpenViBE::uint64 m_ui64Duration;
				std::string m_sName;
			} SStimulation;

			OpenViBE::Kernel::TParameterHandler < OpenViBE::CString* > ip_sFilename;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::float64 > ip_f64EpochDuration;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::uint64 > ip_ui64SeekTime;

			OpenViBE::Kernel::TParameterHandler < OpenViBE::uint64 > op_ui64CurrentStartTime;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::uint64 > op_ui64CurrentEndTime;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::uint64 > op_ui64SamplingRate;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pSignalMatrix;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulations;

			OpenViBE::CString m_sFilename;

			OpenViBE::uint32 m_ui32BinaryFormat;
			OpenViBE::uint32 m_ui32Endianness;
			OpenViBE::uint32 m_ui32ChannelCount;
			OpenViBE::uint64 m_ui64SamplingInterval;
			OpenViBE::uint64 m_ui64StartSampleIndex;
			OpenViBE::uint64 m_ui64EndSampleIndex;
			OpenViBE::uint64 m_ui64SampleCountPerBuffer;

			OpenViBE::uint8* m_pBuffer;
			std::vector < OpenViBE::float64 > m_vChannelScale;
			std::vector < OpenViBEPlugins::FileIO::CAlgorithmBrainampFileReader::SStimulation > m_vStimulation;

			std::ifstream m_oHeaderFile;
			std::ifstream m_oDataFile;
			std::ifstream m_oMarkerFile;
		};

		class CAlgorithmBrainampFileReaderDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Brainamp file reader"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Yann Renard"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("INRIA/IRISA"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Reads input having the BrainAmp file format"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("File reading and writing/Brainamp"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_Algorithm_BrainampFileReader; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::FileIO::CAlgorithmBrainampFileReader; }

			virtual OpenViBE::boolean getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const
			{
				rAlgorithmPrototype.addInputParameter (OVP_Algorithm_BrainampFileReader_InputParameterId_Filename,          "Filename",           OpenViBE::Kernel::ParameterType_String);
				rAlgorithmPrototype.addInputParameter (OVP_Algorithm_BrainampFileReader_InputParameterId_EpochDuration,     "Epoch duration",     OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter (OVP_Algorithm_BrainampFileReader_InputParameterId_SeekTime,          "Seek time",          OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_CurrentStartTime, "Current start time", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_CurrentEndTime,   "Current end time",   OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_SamplingRate,     "Sampling rate",      OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_SignalMatrix,     "Signal samples",     OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_Stimulations,     "Stimulations",       OpenViBE::Kernel::ParameterType_StimulationSet);
				rAlgorithmPrototype.addInputTrigger   (OVP_Algorithm_BrainampFileReader_InputTriggerId_Open,                "Open");
				rAlgorithmPrototype.addInputTrigger   (OVP_Algorithm_BrainampFileReader_InputTriggerId_Seek,                "Seek");
				rAlgorithmPrototype.addInputTrigger   (OVP_Algorithm_BrainampFileReader_InputTriggerId_Next,                "Next");
				rAlgorithmPrototype.addInputTrigger   (OVP_Algorithm_BrainampFileReader_InputTriggerId_Close,               "Close");
				rAlgorithmPrototype.addOutputTrigger  (OVP_Algorithm_BrainampFileReader_OutputTriggerId_Error,              "Error");
				rAlgorithmPrototype.addOutputTrigger  (OVP_Algorithm_BrainampFileReader_OutputTriggerId_DataProduced,       "Data produced");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_BrainampFileReaderDesc);
		};
	};
};

#endif // __OpenViBEPlugins_Algorithm_BrainampFileReader_H__
