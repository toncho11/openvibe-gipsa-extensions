import numpy
import os

class MyOVBox(OVBox):
    # the constructor creates the box and initializes object variables
	def __init__(self):
		OVBox.__init__(self)
		self.iteration = 0
		self.Nrep = None

        	self.pending_stim = []
		self.pending_chunk = []
		self.res = dict()
                self.endOfRep = False
                
	# the initialize method reads settings and outputs the first header
        def initialize(self):
			# all settings
			NrepEnv = os.environ.get('NREP')
			if NrepEnv is None:
					self.Nrep = int(self.setting['Nrep'])
			else:
					self.Nrep = int(NrepEnv)
			self.newtarget()
			print 'initialize descision box with max number of repetition = ' + str(self.Nrep)


	def process(self):

		# process first stimulation input
		for chunk_index in range( len(self.input[1]) ):
			
			if(type(self.input[1][chunk_index]) == OVStimulationHeader):
				self.stimulation_header = self.input[1].pop()
			
			elif(type(self.input[1][chunk_index]) == OVStimulationSet):
				stim = self.input[1].pop()
				while(len(stim)>0):
					st = stim.pop()
					# Row and col stim
					if( (st.identifier>=33024) and (st.identifier<=33055)):
							self.pending_stim.insert(0,st.identifier-33024)
					#end of repetition stim
					elif(st.identifier==33279):
							self.endOfRep = True
							self.Number_of_stim = len(self.pending_stim)
							print "end of repetition : " + str(self.Number_of_stim) + " stimulations received"
						
		# process signal input
		for chunk_index in range( len(self.input[0]) ):

                        
			if(type(self.input[0][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header = self.input[0].pop()
	
			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixBuffer):
					self.pending_chunk.insert(0,self.input[0].pop())

			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixEnd):
					self.input[0].pop()

		# End of repetition
		if(self.endOfRep):
				
				Number_of_chunk = len(self.pending_chunk)
				# Wait for receiving the last results 
				if(Number_of_chunk<self.Number_of_stim):
						pass
				else:
						# Get the right number of results ==> Start Processing
						self.iteration += 1
						for index in range(self.Number_of_stim):
								chunk = self.pending_chunk.pop()
								stim  = self.pending_stim.pop()

								if(stim<16):
										#it's a row !!!
										if(self.rowresults.has_key(stim)):
												self.rowresults[stim].append(chunk[0])
										else:
												self.rowresults[stim] = [chunk[0]]
								if(stim>=16):
										#it's a column !!!
										if(self.colresults.has_key(stim-16)):
												self.colresults[stim-16].append(chunk[0])
										else:
												self.colresults[stim-16] = [chunk[0]]                               


						self.endOfRep = False

						if (self.iteration==self.Nrep):
								#send results
								resultsRow = []
								for r in self.rowresults.itervalues():
										resultsRow.append(numpy.mean(r))

								resultsCol = []
								for c in self.colresults.itervalues():
										resultsCol.append(numpy.mean(c))

				  
								# output row scores
								NRows = len(resultsRow)
								NCols = len(resultsCol)
								#chanLabelsRow = (NRows+NCols+1)*['Row']
								chanLabelsRow = ['value'] + (NRows)*['Row']+(NCols)*['Column'] + ['empty']*(32-(NRows+NCols))
								outHeaderRow = OVStreamedMatrixHeader(self.matrix_header.startTime,self.matrix_header.endTime,[1,32],chanLabelsRow)
								self.output[0].append(outHeaderRow)
								resultsRow.extend(resultsCol)
								resultsRow.extend([0]*(32-(NRows+NCols)))
								outBufferRow = OVStreamedMatrixBuffer(chunk.startTime,chunk.endTime, resultsRow)
								self.output[0].append(outBufferRow)
								print resultsRow

								# output column scores
##                                                NCols = len(resultsCol)
##                                                chanLabelsCol = (NCols+1)*['Col']
##                                                outHeaderCol = OVStreamedMatrixHeader(self.matrix_header.startTime,self.matrix_header.endTime,[1,NCols],chanLabelsCol)
##                                                self.output[1].append(outHeaderCol)
##                                                outBufferCol = OVStreamedMatrixBuffer(chunk.startTime,chunk.endTime, resultsCol)
##                                                self.output[1].append(outBufferCol)

								self.newtarget()
                                                
                                                
	
	def newtarget(self):
		self.iteration = 0
                self.rowresults = dict()
		self.colresults = dict()
	
	def edgingtonComb(self,p1,p2):
		S = p1+p2
		if(S < 1):
			comb = 0.5*(S**2)
		elif(S < 2):
			comb = 0.5*(S**2 - 2.0*((S-1)**2))
		else:
			comb = 1
		return comb
	
if __name__ == '__main__':
	box = MyOVBox()
