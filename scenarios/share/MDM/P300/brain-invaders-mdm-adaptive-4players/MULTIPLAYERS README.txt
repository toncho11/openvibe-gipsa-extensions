STEPS before running Brain Invaders:

1) open online-adaptive-mdm_4p.xlm with OpenVibe designer
2) open Multiplayers Adaptive MDM box
3)Set SETTINGS:

FOR 4players:
C1 file=${__volatile_ScenarioDir}/config/mdm-generic-C1-4p.csv
C0 file=${__volatile_ScenarioDir}/config/mdm-generic-C1-4p.csv


FOR 3players:
C1 file=${__volatile_ScenarioDir}/config/mdm-generic-C1-3p.csv
C0 file=${__volatile_ScenarioDir}/config/mdm-generic-C1-3p.csv


FOR 2players:
C1 file=${__volatile_ScenarioDir}/config/mdm-generic-C1-2p.csv
C0 file=${__volatile_ScenarioDir}/config/mdm-generic-C1-2p.csv

Now you should be able to Start Brain Invaders with online-adaptive-mdm_4p.xlm