import numpy


class CombineMatricesOVBox(OVBox):
    def __init__(self):
        OVBox.__init__(self)

        self.signalHeader = None

        self.is_left_received = False;
        self.is_right_received = False;

        self.is_output_header_sent = False;

    def process(self):

        # process signal input

        if (len(self.input[0]) > 0):
            for chunk_index in range(len(self.input[0])):

                if (type(self.input[0][chunk_index]) == OVStreamedMatrixHeader):
                    self.matrix_header = self.input[0].pop()

                elif (type(self.input[0][chunk_index]) == OVStreamedMatrixBuffer):
                    chunk = self.input[0].pop()

                    if self.is_left_received:
                        print "WARNING: Received another left matrix before output. Replacing."

                    #print chunk
                    self.left_result = numpy.matrix(chunk)  # self.matrix_header.dimensionSizes
                    self.is_left_received = True;

                elif (type(self.input[0][chunk_index]) == OVStreamedMatrixEnd):
                    self.input[0].pop()

        if (len(self.input[1]) > 0):
            for chunk_index in range(len(self.input[1])):

                if (type(self.input[1][chunk_index]) == OVStreamedMatrixHeader):
                    self.matrix_header = self.input[1].pop()

                elif (type(self.input[1][chunk_index]) == OVStreamedMatrixBuffer):
                    chunk = self.input[1].pop()

                    if self.is_right_received:
                        print "WARNING: Received another right matrix before output. Replacing."

                    #print chunk
                    self.right_result = numpy.matrix(chunk)  # self.matrix_header.dimensionSizes
                    self.is_right_received = True;

                elif (type(self.input[1][chunk_index]) == OVStreamedMatrixEnd):
                    self.input[1].pop()

        if (self.is_left_received == True and self.is_right_received == True):  #TODO add here self.is_right_received == true

            #resultsRow = [] #to add extend method
            NRows = 6  #FIXED should be changed to auto
            NCols = 6  #FIXED should be changed to auto

            #print "left result"
            #print self.left_result
            X2 = numpy.concatenate((self.left_result[0, :(NRows + NCols)], self.right_result[0, :(NRows + NCols)]),
                                   axis=1)  #TODO set the second parameter as right matrix

            #print "concatenated X2"
            #print X2
            AllRowsColumns2Players = (NRows + NCols) * 2

            if (self.is_output_header_sent == False):
                print "WARNING: This script only works matrices 1x6."
                chanLabelsRow = ['value'] + (NRows) * ['Row'] + (NCols) * ['Column'] + ['value'] + (NRows) * [
                    'Row'] + (NCols) * ['Column'] + ['empty'] * (32 - AllRowsColumns2Players)
                outHeaderRow = OVStreamedMatrixHeader(self.matrix_header.startTime, self.matrix_header.endTime,
                                                      [1, 32], chanLabelsRow)
                self.output[0].append(outHeaderRow)
                self.is_output_header_sent = True

            resultsRow = X2.tolist()
            resultsRow = resultsRow[0]
            resultsRow.extend([0] * (32 - AllRowsColumns2Players))
            outBufferRow = OVStreamedMatrixBuffer(self._currentTime, self._currentTime + 1.0 / self._clock,
                                                  resultsRow)
            print "resultsRow"
            print resultsRow
            self.output[0].append(outBufferRow)

            self.is_left_received = False
            self.is_right_received = False
            self.left_result = []
            self.right_result = []


box = CombineMatricesOVBox()
