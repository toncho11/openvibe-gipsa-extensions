-- Based on control stimulations on the first input
-- this box outputs on the left or right output.
-- The default output is the right.

-- this function is called when the box is initialized
function initialize(box)
		dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")
        box:log("Trace", "initialize has been called")
 		
		stim_id_left = _G[box:get_setting(2)]
		stim_id_right = _G[box:get_setting(3)]
        is_working = true
end

-- this function is called when the box is uninitialized
function uninitialize(box)
        box:log("Trace", "uninitialize has been called")
		is_working = false
end

-- this function is called once by the box
function process(box)
        box:log("Trace", "process has been called")

		is_left = false;
        -- enters infinite loop
        -- cpu will be released with a call to sleep
        -- at the end of the loop
         while (box:keep_processing()) do

                -- gets current simulated time
                t = box:get_current_time()

				 -- loops over input 2 - the control one
                for input = 2, box:get_input_count() do

                        -- loops on every received stimulation for a given input
                        for stimulation = 1, box:get_stimulation_count(input) do

                                -- gets the received stimulation
                                identifier, date, duration = box:get_stimulation(input, 1)
								
                                -- discards it
                                box:remove_stimulation(input, 1)

								if identifier == stim_id_left then
								   is_left = true
								elseif identifier == stim_id_right then
								   is_left = false
								end
                        end
                end
				
                -- loops on input 1 - the data one
                for input = 1, box:get_input_count() do

                        -- loops on every received stimulation for a given input
                        for stimulation = 1, box:get_stimulation_count(input) do

                                -- gets the received stimulation
                                identifier, date, duration = box:get_stimulation(input, 1)

                                -- discards it
                                box:remove_stimulation(input, 1)

								if is_left == true 
								  then
								     box:send_stimulation(1, identifier, t, duration)
								else
								     box:send_stimulation(2, identifier, t, duration)
								end
                        end
                end

                -- releases cpu
                box:sleep()
         end
end