-- this function is called when the box is initialized
function initialize(box)
		dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")
        box:log("Trace", "initialize has been called")

		output_filename = box:get_setting(2)
		box:log("Info", output_filename)
		-- Opens a file in append mode
		file = io.open(output_filename, "w")

		-- sets the default output file as test.lua
		io.output(file)
        trials = 0;
		matches = 0;
end

-- this function is called when the box is uninitialized
function uninitialize(box)
        box:log("Trace", "uninitialize has been called")
end

-- this function is called once by the box
function process(box)
        box:log("Trace", "process has been called")

        -- enters infinite loop
        -- cpu will be released with a call to sleep
        -- at the end of the loop
         while (box:keep_processing()) do

                -- gets current simulated time
                t = box:get_current_time()

				 -- loops over input 2 - the expected values
                for input = 2, box:get_input_count() do

                        -- loops on every received stimulation for a given input
                        for stimulation = 1, box:get_stimulation_count(input) do

                                -- gets the received stimulation
                                identifier, date, duration = box:get_stimulation(input, 1)
								
								last_expected_identifier = identifier;
						
                                -- discards it
                                box:remove_stimulation(input, 1)
                        end
                end
				
                -- loops on input 1 - the result form the MDM classification
                for input = 1, box:get_input_count() do

                        -- loops on every received stimulation for a given input
                        for stimulation = 1, box:get_stimulation_count(input) do

                                -- gets the received stimulation
                                identifier, date, duration = box:get_stimulation(input, 1)

                                -- discards it
                                box:remove_stimulation(input, 1)

								if identifier < 100000 
									then 
									last_result_identifier = identifier
							
									string_to_write = string.format("At time %f the expected class label is %i, the classification result is: %i\n", date, last_expected_identifier, last_result_identifier)
									io.write(string_to_write)
									trials = trials + 1
									
									if last_expected_identifier == last_result_identifier
									   then
									     matches = matches + 1
									   end
									end
                        end
                end

                -- releases cpu
                box:sleep()
         end
		 
		-- closes the open file
		io.write(string.format("Accuracy: %i/%i",matches,trials))
		io.close(file)
end